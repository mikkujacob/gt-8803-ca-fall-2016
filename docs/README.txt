README
______

RUNNING THE CODE:

1. Java binary class file (ProjectX.class) is available to run in ../bin/cs8803ca/projectX/

VIEWING AND RUNNING THE CODE FROM ECLIPSE:

1. Download Eclipse from https://eclipse.org/downloads/ .
2. Unzip codebase.
3. Import project using File->Import->General->Existing Projects Into Workspace, then click Next.
4. Select codebase root directory and click Finish.
5. Source is inside src/, libraries are inside lib/, documentation is inside docs/
6. Open cs8803ca.ProjectX.ProjectX.java
7. Click Run As Application.
