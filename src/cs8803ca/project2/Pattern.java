package cs8803ca.project2;

import java.util.ArrayList;

import cs8803ca.project2.Project2.pt;
import cs8803ca.project2.Project2.pts;
import cs8803ca.project2.Project2.vec;

public class Pattern
{
	Project2 parent;
	int primitiveIndex, trajectoryIndex;
	int superSampledPointCount;
	pts superSampledTrajectory;
	float geodesicLength;
	float euclideanLength;
	
	float xMin, xMax, yMin, yMax;
	
	ArrayList<pts> primitives, trajectories;
	pts smoothedPrimitive, smoothedTrajectory;
	pts backupPrimitive, backupTrajectory;
	
	public Pattern(Project2 parent)
	{
		this.parent = parent;
		primitives = new ArrayList<pts>();
		primitives.add(parent.new pts());
		trajectories = new ArrayList<pts>();
		trajectories.add(parent.new pts());
		smoothedPrimitive = parent.new pts();
		smoothedTrajectory = parent.new pts();
		primitiveIndex = trajectoryIndex = 0;
		superSampledPointCount = 0;
		geodesicLength = 0;
		euclideanLength = 0;
		xMin = 0;
		xMax = 0;
		yMin = 0;
		yMax = 0;
		superSampledTrajectory = parent.new pts();
	}
	
	public pts computeSamplePoints(float distanceThreshold)
	{
		return computeSamplePoints(distanceThreshold, trajectories.get(trajectoryIndex), true);
	}
	
	public pts computeSamplePoints(float distanceThreshold, pts originalTrajectory, Boolean superSample)
	{
		ArrayList<pt> trajectorySamplePoints = new ArrayList<pt>();
		trajectorySamplePoints.clear();
		pts trajectory = parent.new pts(originalTrajectory);
		float gd = 0;
		trajectorySamplePoints.add(trajectory.G.get(0));
		
//		System.out.println("Entered CSP");
		
		if(superSample)
		{
			trajectory = superSample(originalTrajectory);
		}
		
		for(int index = 1; index < trajectory.G.size(); index++)
		{
			if(gd >= distanceThreshold)
			{
				trajectorySamplePoints.add(trajectory.G.get(index));
				gd = 0;
			}
			else
			{
				gd += parent.d(trajectory.G.get(index), trajectory.G.get(index - 1));
			}
		}
//		System.out.println("Number of sample points along trajectory: " + trajectorySamplePoints.size());
		
		pts result = parent.new pts();
		result.addAllPts(trajectorySamplePoints);
		return result;
	}
	
	public pts superSample()
	{
		return superSample(trajectories.get(trajectoryIndex));
	}
	
	public pts superSample(pts originalTrajectory)
	{
		float minDistance = Integer.MAX_VALUE;
		pts trajectory = parent.new pts(originalTrajectory);
		
		for(int index = 1; index < trajectory.G.size(); index++)
		{
			float localDistance = parent.d(trajectory.G.get(index), trajectory.G.get(index - 1));
			if(localDistance < minDistance && localDistance > 0.1f)
			{
				minDistance = localDistance;
			}
		}
		
//		System.out.println("Compute Min Dist: " + minDistance);
		
		for(int index = 1; index < trajectory.G.size(); index++)
		{
			float localDistance = parent.d(trajectory.G.get(index), trajectory.G.get(index - 1));
//			System.out.println("LocalDistance: " + localDistance);
			if(localDistance > minDistance)
			{
				vec vector = parent.new vec();
				vector = parent.U(trajectory.G.get(index - 1), trajectory.G.get(index));
				pt P = parent.P(trajectory.G.get(index - 1), minDistance, vector);
				trajectory.addPtAt(index, P);
//				System.out.println("Index: " + index);
			}
		}
		
//		System.out.println("Number of supersampled points along trajectory: " + trajectory.G.size());
		
		superSampledPointCount = trajectory.G.size();
		superSampledTrajectory = trajectory;
		
		geodesicLength = computeGeodesicDistance(trajectory, 0, trajectory.G.size() - 1);
		euclideanLength = parent.d(trajectory.G.get(0), trajectory.G.get(trajectory.G.size() - 1));
		
		computeTrajectoryBounds(trajectory);
		
		return trajectory;
	}
	
	public float computeGeodesicDistance(pts trajectory, int startindex, int finishIndex)
	{
		float gd = 0f;
		
		for(int index = startindex; index < finishIndex; index++)
		{
			gd += parent.d(trajectory.G.get(index), trajectory.G.get(index + 1));
		}
		
		return gd;
	}
	
	public void computeTrajectoryBounds(pts trajectory)
	{
		float minX = Float.MAX_VALUE, minY = Float.MAX_VALUE, maxX = Float.MIN_VALUE, maxY = Float.MIN_VALUE;
		for(int index = 0; index < trajectory.G.size(); index++)
		{
			if(trajectory.G.get(index).x < minX)
			{
				minX = trajectory.G.get(index).x;
			}
			if(trajectory.G.get(index).y < minY)
			{
				minY = trajectory.G.get(index).y;
			}
			if(trajectory.G.get(index).x > maxX)
			{
				maxX = trajectory.G.get(index).x;
			}
			if(trajectory.G.get(index).y > maxY)
			{
				maxY = trajectory.G.get(index).y;
			}
		}
		
		xMin = minX;
		yMin = minY;
		xMax = maxX;
		yMax = maxY;
	}
	
	public int getPrimitiveIndex() {
		return primitiveIndex;
	}

	public void setPrimitiveIndex(int primitiveIndex) {
		this.primitiveIndex = primitiveIndex;
	}

	public int getTrajectoryIndex() {
		return trajectoryIndex;
	}

	public void setTrajectoryIndex(int trajectoryIndex) {
		this.trajectoryIndex = trajectoryIndex;
	}

	public ArrayList<pts> getPrimitives() {
		return primitives;
	}

	public void setPrimitives(ArrayList<pts> primitives) {
		this.primitives = primitives;
	}

	public ArrayList<pts> getTrajectories() {
		return trajectories;
	}

	public void setTrajectories(ArrayList<pts> trajectories) {
		this.trajectories = trajectories;
	}

	public int getSuperSampledPointCount()
	{
		return superSampledPointCount;
	}

	public void setSuperSampledPointCount(int superSampledPointCount)
	{
		this.superSampledPointCount = superSampledPointCount;
	}

	public pts getSuperSampledTrajectory()
	{
		return superSampledTrajectory;
	}

	public void setSuperSampledTrajectory(pts superSampledTrajectory)
	{
		this.superSampledTrajectory = superSampledTrajectory;
	}

	public float getGeodesicLength()
	{
		return geodesicLength;
	}

	public void setGeodesicLength(float geodesicLength)
	{
		this.geodesicLength = geodesicLength;
	}

	public float getEuclideanLength()
	{
		return euclideanLength;
	}

	public void setEuclideanLength(float euclideanLength)
	{
		this.euclideanLength = euclideanLength;
	}

	public float getxMin()
	{
		return xMin;
	}

	public void setxMin(float xMin)
	{
		this.xMin = xMin;
	}

	public float getxMax()
	{
		return xMax;
	}

	public void setxMax(float xMax)
	{
		this.xMax = xMax;
	}

	public float getyMin()
	{
		return yMin;
	}

	public void setyMin(float yMin)
	{
		this.yMin = yMin;
	}

	public float getyMax()
	{
		return yMax;
	}

	public void setyMax(float yMax)
	{
		this.yMax = yMax;
	}
	
	public void smoothenPrimitive(int n)
	{
		pts localPrimitive = parent.new pts(primitives.get(primitiveIndex));
		
		for(int repetitions = 0; repetitions < n; repetitions++)
		{
			smoothedPrimitive = parent.new pts();
			for(int index = 1; index < localPrimitive.G.size() - 1; index++)
			{
				pt B = localPrimitive.G.get(index);
				pt A = localPrimitive.G.get(index - 1);
				pt C = localPrimitive.G.get(index + 1);
				
				vec BA = parent.V(B, A);
				vec BC = parent.V(B, C);
				vec BBdash = BA.add(BC).divideBy(4);
				
				pt P = parent.P(B, BBdash);
				
				smoothedPrimitive.addPt(P);
			}
			if(!smoothedPrimitive.G.isEmpty())
			{
				localPrimitive = parent.new pts(smoothedPrimitive);
			}
		}
		
		smoothedPrimitive = parent.new pts(localPrimitive);
	}
	
	public void smoothenTrajectory(int n)
	{
		pts localTrajectory = parent.new pts(trajectories.get(trajectoryIndex));
		
		for(int repetitions = 0; repetitions < n; repetitions++)
		{
			smoothedTrajectory = parent.new pts();
			for(int index = 1; index < localTrajectory.G.size() - 1; index++)
			{
				pt B = localTrajectory.G.get(index);
				pt A = localTrajectory.G.get(index - 1);
				pt C = localTrajectory.G.get(index + 1);
				
				vec BA = parent.V(B, A);
				vec BC = parent.V(B, C);
				vec BBdash = BA.add(BC).divideBy(4);
				
				pt P = parent.P(B, BBdash);
				
				smoothedTrajectory.addPt(P);
			}
			if(!smoothedTrajectory.G.isEmpty())
			{
				localTrajectory = parent.new pts(smoothedTrajectory);
			}
		}
		
		smoothedTrajectory = parent.new pts(localTrajectory);
	}
	
	public pts getSmoothedPrimitive()
	{
		return smoothedPrimitive;
	}
	
	public pts getSmoothedTrajectory()
	{
		return smoothedTrajectory;
	}
	
	public void toggleSmoothedPrimitive()
	{
		if(smoothedPrimitive.G.isEmpty())
		{
			smoothenPrimitive(5);
		}
		
		if(backupPrimitive == null)
		{
			backupPrimitive = parent.new pts(primitives.get(primitiveIndex));
			primitives.set(primitiveIndex, smoothedPrimitive);
		}
		else
		{
			primitives.set(primitiveIndex, backupPrimitive);
			backupPrimitive = null;
		}
	}
	
	public void toggleSmoothedTrajectory()
	{
		if(smoothedTrajectory.G.isEmpty())
		{
			smoothenTrajectory(5);
		}
		
		if(backupTrajectory == null)
		{
			backupTrajectory = parent.new pts(trajectories.get(trajectoryIndex));
			trajectories.set(trajectoryIndex, smoothedTrajectory);
		}
		else
		{
			trajectories.set(trajectoryIndex, backupTrajectory);
			backupTrajectory = null;
		}
	}
}
