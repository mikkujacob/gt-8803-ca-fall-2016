package cs8803ca.project2;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;

import controlP5.ControlP5;
import controlP5.Slider;

import processing.core.PApplet;
import processing.core.PImage;

public class Project2 extends PApplet
{
	private static final Boolean isFullScreen = true;

	public static void main(String args[])
	{
		if(isFullScreen)
		{
			PApplet.main(new String[] { "--present", "cs8803ca.project2.Project2" });
		}
		else
		{
			PApplet.main(new String[] { "cs8803ca.project2.Project2" });
		}
		
	}

	// LecturesInGraphics: vector interpolation
	// Template for sketches
	// Author: Jarek ROSSIGNAC

	// **************************** global variables
	// ****************************
	pts P = new pts();
	Pattern pattern = new Pattern(this);
	float t = 1, t2 = 1, f = 0, tanimation = 1, tanimationindex = 0;
	Boolean hasChanged = false;
	Boolean DRAW_PRIMITIVE_MODE = true, DRAW_TRAJECTORY_MODE = false, EDIT_PATTERN_MODE = false;
	Boolean PRIMITIVE_INPUT_MODE_DRAG = true, PRIMITIVE_CONTROL_POINT_CLOSED = false, PRIMITIVE_SMOOTHING = false;
	Boolean TRAJECTORY_INPUT_MODE_DRAG = true, TRAJECTORY_CONTROL_POINT_CLOSED = false, TRAJECTORY_SMOOTHING = false;
	Boolean TRAJECTORY_VISIBLE = false, PRIMITIVES_VISIBLE = true, MANDALA_VISIBLE = true;
	Boolean PRIMITIVE_FILLED = false, PRIMITIVE_CLOSED = true;
	pts trajectorySamplePoints;
	Random rand;
	ControlP5 cp5e, cp5t, cp5p;
	int sliderColor = color(125, 243, 23);
	Slider slider;//, trajectoryScaleSlider, primitiveScaleSlider, sampleDistanceSlider, mandalaAngleSlider;
	float trajectoryScale = 0.3f, primitiveScale = 0.2f, sampleDistance = 300, mandalaAngle = (float) (Math.PI / 6);
	int redVal = 0, greenVal = 0, blueVal = 0;
	float len = 60; // length of arrows
	Boolean animateForward = true;
	
	public void setup()
	{
//		P.loadPts("data/pts");
		P = new pts();
		rand = new Random();
		trajectorySamplePoints = new pts();
		redVal = rand.nextInt(256);
		greenVal = rand.nextInt(256);
		blueVal = rand.nextInt(256);
		
		cp5e = new ControlP5(this);
		// name, minValue, maxValue, defaultValue, x, y, width, height
		cp5e.addTextlabel("primitiveInstructionLabel", "You can return to edit the primitive or trajectory by pressing the P or T keys", 50, 100).setColorValue(color(124, 124, 124)).setFont(createFont("Georgia",32));
		cp5e.addSlider("trajectoryScaleSlider", 1f, 0.01f, 0.3f, 50, 200, 200, 20).setCaptionLabel("");
		cp5e.addTextlabel("trajectoryScalingLabel", "Trajectory Scaling", 50, 240).setColorValue(color(124, 124, 124)).setFont(createFont("Georgia",20));
		cp5e.addSlider("primitiveScaleSlider", 1f, 0.01f, 0.2f, 50, 300, 200, 20).setCaptionLabel("");
		cp5e.addTextlabel("primitiveScalingLabel", "Primitive Scaling", 50, 340).setColorValue(color(124, 124, 124)).setFont(createFont("Georgia",20));
		cp5e.addSlider("sampleDistanceSlider", 1000f, 10f, 100f, 50, 400, 200, 20).setCaptionLabel("");
		cp5e.addTextlabel("SampleDistanceLabel", "Inter-Primitive Distance", 50, 440).setColorValue(color(124, 124, 124)).setFont(createFont("Georgia",20));
//		cp5.addSlider("mandalaAngleSlider", 0.01f, (float)(Math.PI), (float)(Math.PI / 6), 50, 500, 200, 20).setCaptionLabel("Mandala Angle");
		cp5e.addKnob("mandalaAngleKnob", 0.02f, (float)(Math.PI), 50, 500, 100).setCaptionLabel("");
		cp5e.addTextlabel("mandalaAngleLabel", "Mandala Angle", 50, 640).setColorValue(color(124, 124, 124)).setFont(createFont("Georgia",20));
		cp5e.addButton("beautificationButton").setPosition(50, 800).setSize(100, 40).setCaptionLabel("Beautify");
		
		cp5e.setVisible(false);
		
		cp5p = new ControlP5(this);
		cp5p.addTextlabel("primitiveInstructionLabel", "Draw a primitive shape for the pattern and then press the T key", 50, 100).setColorValue(color(124, 124, 124)).setFont(createFont("Georgia",32));
		
		cp5t = new ControlP5(this);
		cp5t.addTextlabel("trajectoryInstructionLabel", "Draw a trajectory for the primitives to be laid out in the pattern and then press the E key", 50, 100).setColorValue(color(124, 124, 124)).setFont(createFont("Georgia",32));
		cp5t.setVisible(false);
		
//		P.resetOnCircle(3);
	}
	
	public void settings()
	{
		if(isFullScreen)
		{
			size(displayWidth, displayHeight);
		}
		else
		{
			size(600, 600); // window size
		}
		smooth(); // turn on antialiasing
	}
	
	public void draw()
	{
		// executed at each frame
		background(white); // clear screen and paints white background
		if (snapPic)
			beginRecord(PDF, PicturesOutputPath + "/P"
					+ nf(pictureCounter++, 3) + ".pdf");

		if (animating)
		{
			if(animateForward)
			{
				t += 0.01;
				if (t >= 1)
				{
					t = 1;
	//				animating = false;
					animateForward = false;
				}
				
				t2 += 0.01;
				if (t2 >= 1)
				{
					t2 = 1;
	//				animating = false;
					animateForward = false;
				}
			}
			else
			{
				t -= 0.01;
				if (t <= 0)
				{
					t = 0;
//					animating = false;
					animateForward = true;
				}
				
				t2 -= 0.01;
				if (t2 <= 0)
				{
					t2 = 0;
//					animating = false;
					animateForward = true;
				}
			}
			
			beautification();
		}
		
		drawGUI();
		
		if (DRAW_PRIMITIVE_MODE == true)
		{
			pattern.getPrimitives().get(pattern.getPrimitiveIndex()).drawCurve();
		}
		else if (DRAW_TRAJECTORY_MODE == true)
		{
			pattern.getTrajectories().get(pattern.getTrajectoryIndex()).drawCurve();
		}
		else if (EDIT_PATTERN_MODE == true)
		{
			pushMatrix();
			drawMandalaCircular(mandalaAngle, trajectoryScale, sampleDistance, primitiveScale);
			popMatrix();
		}
		
		if (snapPic)
		{
			endRecord();
			snapPic = false;
		} // end saving a .pdf of the screen

		fill(black);
		displayHeader();
		if (scribeText && !filming)
			displayFooter(); // shows title, menu, and my face & name
		if (filming && (animating || change))
		{
			//Take an actual screenshot.
			PImage screenShot = takeScreenShot();
			screenShot.save("FRAMES/F" + nf(frameCounter++, 5) + ".tif");
//			saveFrame("FRAMES/F" + nf(frameCounter++, 4) + ".tif"); // saves a
		// movie
		// frame
		}
		change = false; // to avoid capturing movie frames when nothing happens
		hasChanged = false;
	}
	
	public PImage takeScreenShot()
	{
	    Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
	    
	    try {
	        
	        BufferedImage screenBuffer = new Robot().createScreenCapture(screenRect);
	        
//	        PImage screenShot = new PImage(screenBuffer.getWidth(), screenBuffer.getHeight(), ARGB);
	        PImage screenShot = createImage(screenBuffer.getWidth(), screenBuffer.getHeight(), ARGB);
	        screenBuffer.getRGB(0, 0, screenShot.width, screenShot.height, screenShot.pixels, 0, screenShot.width);
	        screenShot.updatePixels();
	        
	        return screenShot;
	        
	    } catch ( AWTException e ) {
	        e.printStackTrace();
	    }
	    
	    return null;
	} 
	
	public void drawGUI()
	{
		if(EDIT_PATTERN_MODE)
		{
			cp5e.setVisible(true);
			cp5p.setVisible(false);
			cp5t.setVisible(false);
		}
		else if(DRAW_PRIMITIVE_MODE)
		{
			cp5e.setVisible(false);
			cp5p.setVisible(true);
			cp5t.setVisible(false);
		}
		else if(DRAW_TRAJECTORY_MODE)
		{
			cp5e.setVisible(false);
			cp5p.setVisible(false);
			cp5t.setVisible(true);
		}
	}
	
	public void trajectoryScaleSlider(float theValue)
	{
//		System.out.println("TS: " + theValue);
		trajectoryScale = theValue;
		change = true;
	}
	
	public void primitiveScaleSlider(float theValue)
	{
//		System.out.println("PS: " + theValue);
		primitiveScale = theValue;
		change = true;
	}
	
	public void sampleDistanceSlider(float theValue)
	{
//		System.out.println("SD: " + theValue);
		sampleDistance = theValue;
		hasChanged = true;
		change = true;
	}
	
//	public void mandalaAngleSlider(float theValue)
//	{
//		System.out.println("MA: " + theValue);
//		mandalaAngle = theValue;
//		change = true;
//	}
	
	public void mandalaAngleKnob(float theValue)
	{
//		System.out.println("MAKnob: " + theValue);
		mandalaAngle = theValue;
		change = true;
	}
	
	public void beautificationButton(float theValue)
	{
		animating = !animating;
		if(animating == true)
		{
			pattern.smoothenPrimitive(10);
			pattern.smoothenTrajectory(10);
			pattern.toggleSmoothedPrimitive();
			pattern.toggleSmoothedTrajectory();
			t = 0;
			t2 = 0;
		}
		else
		{
			pattern.toggleSmoothedPrimitive();
			pattern.toggleSmoothedTrajectory();
			t = 1;
			t2 = 1;
		}
	}
	
	public void beautification()
	{
		//optimizing trajectoryScaling
		//optimizing primitiveScaling
		//optimizing samplingDistance
		//Animating mandalaAngle

		mandalaAngle = calculateMandalaAngle(t);
//		System.out.println("MA: " + mandalaAngle);
		sampleDistance = calculateSampleDistance(t2);
//		System.out.println("SD: " + sampleDistance);
	}
	
	public float calculateMandalaAngle(float time)
	{
		if(time < 0.01)
		{
			time = 0.01f;
		}
		time = 1 - time;
		float min = cp5e.getController("mandalaAngleKnob").getMin();
		float max = cp5e.getController("mandalaAngleKnob").getMax();
//		System.out.println("Min: " + min + "; Max: " + max);
		
		float angle = time * (max - min) + min;
		cp5e.getController("mandalaAngleKnob").setValue(angle);
		return angle;
	}
	
	public float calculateSampleDistance(float time)
	{
		float min = cp5e.getController("sampleDistanceSlider").getMin();
		float max = cp5e.getController("sampleDistanceSlider").getMax();
//		System.out.println("Min: " + min + "; Max: " + max);
		
		float distance = time * (max - min) + min;
		cp5e.getController("sampleDistanceSlider").setValue(distance);
		return distance;
	}
	
	public void drawMandalaCircular(float angle, float trajectoryScale, float sampleDistance, float primitiveScale)
	{
		if(!MANDALA_VISIBLE)
		{
			return;
		}
		
		pushMatrix();
		
		for(int i = 0; i < 2 * Math.PI / angle; i++)
		{	
			drawPrimitiveOnTrajectory(trajectoryScale, sampleDistance, primitiveScale, angle * i);
		}

		popMatrix();
	}
	
	public void drawPrimitiveOnTrajectory(float trajectoryScale, float sampleDistance, float primitiveScale)
	{
		drawPrimitiveOnTrajectory(trajectoryScale, sampleDistance, primitiveScale, 0f);
	}
	
	public void drawPrimitiveOnTrajectory(float trajectoryScale, float sampleDistance, float primitiveScale, float orientation)
	{
		pts trajectory = new pts(pattern.getTrajectories().get(pattern.getTrajectoryIndex()));
		trajectory = trajectory.scaleAllAroundCentroid(trajectoryScale);
		trajectory = trajectory.rotateAll(orientation, trajectory.G.get(0));
		trajectory = trajectory.moveAll(V(trajectory.G.get(0), new pt(displayWidth / 2, displayHeight / 2)));
		if(TRAJECTORY_VISIBLE)
		{
			trajectory.drawCurve();
		}
		if(hasChanged == true)
		{
			if(trajectorySamplePoints.G.size() > 0)
			{
				trajectorySamplePoints = pattern.computeSamplePoints(sampleDistance, pattern.superSample(), false);
			}
			else
			{
				trajectorySamplePoints = pattern.computeSamplePoints(sampleDistance);
			}
			
			if(pattern.getSuperSampledPointCount() / 2 > 10)
			{
				float distance = pattern.getGeodesicLength() / 2;
				cp5e.getController("sampleDistanceSlider").setMin(distance);
			}

			hasChanged = false;
//			System.out.println("HAS CHANGED IS " + hasChanged);
		}

		pts samplePoints = new pts(trajectorySamplePoints);
		samplePoints = samplePoints.scaleAllAroundCentroid(trajectoryScale);
		samplePoints = samplePoints.rotateAll(orientation, samplePoints.G.get(0));
		samplePoints = samplePoints.moveAll(V(samplePoints.G.get(0), new pt(displayWidth / 2, displayHeight / 2)));

		pts primitive = new pts(pattern.getPrimitives().get(pattern.getPrimitiveIndex()));
		primitive = primitive.scaleAllAroundCentroid(primitiveScale);
		
		for(pt samplePoint: samplePoints.G)
		{	
			pushMatrix();
			pt primitiveCentroid = primitive.Centroid();
			translate(-1*primitiveCentroid.x, -1*primitiveCentroid.y);
			translate(samplePoint.x, samplePoint.y);
			if(PRIMITIVES_VISIBLE)
			{
				drawPrimitive(primitive, redVal, greenVal, blueVal);
				
				if(frameCount % 10 == 0)
				{
					int increment = 256 / samplePoints.G.size();
					redVal = (redVal + increment) % 256;
					greenVal = (greenVal + increment) % 256;
					blueVal = (blueVal + increment) % 256;
				}
			}
			popMatrix();
		}
	}
	
	public void drawPrimitive(pts primitive, int red, int green, int blue)
	{
		if(!PRIMITIVE_FILLED)
		{
			primitive.drawCurve();
		}
		else
		{
			pts filledPrimitive = new pts(primitive);
			int oldStroke = g.strokeColor;
			noStroke();
			if(PRIMITIVE_CLOSED)
			{
				filledPrimitive.drawFilledClosedCurve(red, green, blue);
			}
			else
			{
				filledPrimitive.drawFilledCurve(red, green, blue);
			}
			stroke(oldStroke);
		}
		
	}
	
	// **************************** user actions ****************************
		public void keyPressed()
		{ // executed each time a key is pressed: sets the "keyPressed" and "key"
			// state variables,
			// till it is released or another key is pressed or released
			if (key == '?')
				scribeText = !scribeText; // toggle display of help text and authors
			// picture
			if (key == '!')
				snapPicture(); // make a picture of the canvas and saves as .jpg
			// image
			if (key == '`')
				snapPic = true; // to snap an image of the canvas and save as
			// zoom-able a PDF
			if (key == '~')
			{
				filming = !filming;
			} // filming on/off capture frames into folder FRAMES
			if (key == 'a')
			{
				animating = true;
				t = 0;
				t2 = 0;
				tanimation = 0;
			}
			if (key == 's')
				P.savePts("data/pts");
			if (key == 'l')
				P.loadPts("data/pts");
			
			if(key == 'p' || key == 'P')
			{
				DRAW_PRIMITIVE_MODE = true; DRAW_TRAJECTORY_MODE = false; EDIT_PATTERN_MODE = false;
				animating = false;
				t = 1;
				t2 = 1;
			}
			if(key == 't' || key == 'T')
			{
				DRAW_PRIMITIVE_MODE = false; DRAW_TRAJECTORY_MODE = true; EDIT_PATTERN_MODE = false;
				animating = false;
				t = 1;
				t2 = 1;
			}
			if(key == 'e' || key == 'E')
			{
				if(!pattern.getPrimitives().get(pattern.getPrimitiveIndex()).G.isEmpty() && !pattern.getTrajectories().get(pattern.getTrajectoryIndex()).G.isEmpty())
				{
					DRAW_PRIMITIVE_MODE = false; DRAW_TRAJECTORY_MODE = false; EDIT_PATTERN_MODE = true;
					hasChanged = true;
//					System.out.println("HAS CHANGED IS " + hasChanged);
					t = 0;
					t2 = 0;
				}
				else
				{
					System.out.println("TRIED TO DO EDIT PATTERN MODE WITHOUT INITIALIZING A PRIMITIVE OR A TRAJECTORY");
				}
			}
			if(key == '1')
			{
				PRIMITIVES_VISIBLE = !PRIMITIVES_VISIBLE;
			}
			if(key == '2')
			{
				TRAJECTORY_VISIBLE = !TRAJECTORY_VISIBLE;
			}
			if(key == '3')
			{
				MANDALA_VISIBLE = !MANDALA_VISIBLE;
			}
			if(key == '4')
			{
				PRIMITIVE_FILLED = !PRIMITIVE_FILLED;
			}
			if(key == '5')
			{
				PRIMITIVE_CLOSED = !PRIMITIVE_CLOSED;
			}
			
			if(key == CODED){
				if(keyCode == LEFT){
					if(DRAW_PRIMITIVE_MODE == true){
						if(pattern.getPrimitiveIndex() >= 1){ 
							pattern.setPrimitiveIndex(pattern.getPrimitiveIndex() - 1); 
						}
					}
					
					if(DRAW_TRAJECTORY_MODE == true){
						if(pattern.getTrajectoryIndex() >= 1){
							pattern.setTrajectoryIndex(pattern.getTrajectoryIndex() - 1);
						}
					}
				}
				if(keyCode == RIGHT){
					if(DRAW_PRIMITIVE_MODE == true){
						int lastIndex = pattern.getPrimitives().size() - 1;
						if((pattern.getPrimitiveIndex() == lastIndex) && (pattern.getPrimitives().get(lastIndex).G.size() != 0)){
							pattern.getPrimitives().add(new pts());
							pattern.setPrimitiveIndex(pattern.getPrimitiveIndex() + 1);
						}
						else if(pattern.getPrimitiveIndex() < lastIndex){ 
							pattern.setPrimitiveIndex(pattern.getPrimitiveIndex() + 1); 
							}
					}
					
					if(DRAW_TRAJECTORY_MODE == true){
						int lastIndex = pattern.getTrajectories().size() - 1;
						if((pattern.getTrajectoryIndex() == lastIndex) && (pattern.getTrajectories().get(lastIndex).G.size() != 0)){
							pattern.getTrajectories().add(new pts());
							pattern.setTrajectoryIndex(pattern.getTrajectoryIndex() + 1);
						}
						else if(pattern.getTrajectoryIndex() < lastIndex){ 
							pattern.setTrajectoryIndex(pattern.getTrajectoryIndex() + 1); 
							}
					}
				}
			}
			if (key == 'Q')
				exit(); // quit application
			change = true; // to make sure that we save a movie frame each time
			// something changes
		}

		public void mousePressed()
		{ // executed when the mouse is pressed
			if(DRAW_PRIMITIVE_MODE)
			{
				
			}
			else if (DRAW_TRAJECTORY_MODE)
			{
				
			}
			else if (EDIT_PATTERN_MODE)
			{
//				P.pickClosest(Mouse()); // used to pick the closest vertex of C to the
//				 // mouse
			}
			
			change = true;
		}
		
		public void mouseClicked()
		{
			if(DRAW_PRIMITIVE_MODE)
			{
				if(!PRIMITIVE_INPUT_MODE_DRAG)
				{
					pattern.getPrimitives().get(pattern.getPrimitiveIndex()).addPt(mouseX, mouseY);
				}
			}
			
			if(DRAW_TRAJECTORY_MODE)
			{
				if(!TRAJECTORY_INPUT_MODE_DRAG)
				{
					pattern.getTrajectories().get(pattern.getTrajectoryIndex()).addPt(mouseX, mouseY);
				}
			}
		}

		public void mouseDragged()
		{
			if(DRAW_PRIMITIVE_MODE)
			{
				if(PRIMITIVE_INPUT_MODE_DRAG)
				{
					pattern.getPrimitives().get(pattern.getPrimitiveIndex()).addPt(mouseX, mouseY);
				}
			}
			
			if(DRAW_TRAJECTORY_MODE){
				if(TRAJECTORY_INPUT_MODE_DRAG)
				{	
					pattern.getTrajectories().get(pattern.getTrajectoryIndex()).addPt(mouseX, mouseY);
				}
			}
			
//			if (!keyPressed || (key == 'a'))
//				P.dragPicked(); // drag selected point with mouse
//			if (keyPressed)
//			{
//				if (key == '.')
//					f += 2. * (float) (mouseX - pmouseX) / width; // adjust current
//				// frame
//				if (key == 't')
//					P.dragAll(); // move all vertices
//				if (key == 'r')
//					P.rotateAllAroundCentroid(Mouse(), Pmouse()); // turn all
//				// vertices
//				// around their
//				// center of
//				// mass
//				if (key == 'z')
//					P.scaleAllAroundCentroid(Mouse(), Pmouse()); // scale all
//				// vertices with
//				// respect to
//				// their center
//				// of mass
//			}
			change = true;
		}

		// **************************** text for name, title and help
		// ****************************
		String title = "CA 2015 P2: Pattern Design System",
				name = "Student: Kristian Eberhardson & Mikhail Jacob",
				menu = "?:(show/hide) help, a: animate, `:snap picture, ~:(start/stop) recording movie frames, Q:quit",
				guide = "drag:edit P&V, p/t/e:Draw Primitive/Draw Trajectory/Edit Pattern, 1/2/3/4/5: Toggle Primitives/Toggle Trajectories/Toggle Pattern/Fill Primitives";
		// info

		void drawObject(pt P, vec V)
		{
			beginShape();
			v(P(P(P, 1, V), 0.25f, R(V)));
			v(P(P(P, 1, V), -0.25f, R(V)));
			v(P(P(P, -1, V), -0.25f, R(V)));
			v(P(P(P, -1, V), 0.25f, R(V)));
			endShape(CLOSE);
		}

		float timeWarp(float f)
		{
			return sq(sin(f * PI / 2));
		}

		// *****************************************************************************
		// TITLE: GEOMETRY UTILITIES IN 2D
		// DESCRIPTION: Classes and functions for manipulating points, vectors,
		// edges, triangles, quads, frames, and circular arcs
		// AUTHOR: Prof Jarek Rossignac
		// DATE CREATED: September 2009
		// EDITS: Revised July 2011
		// *****************************************************************************
		// ************************************************************************
		// **** POINT CLASS
		// ************************************************************************
		class pt
		{
			float x = 0, y = 0;

			// CREATE
			pt()
			{
			}

			pt(float px, float py)
			{
				x = px;
				y = py;
			};

			// MODIFY
			pt setTo(float px, float py)
			{
				x = px;
				y = py;
				return this;
			};

			pt setTo(pt P)
			{
				x = P.x;
				y = P.y;
				return this;
			};

			pt setToMouse()
			{
				x = mouseX;
				y = mouseY;
				return this;
			};

			pt add(float u, float v)
			{
				x += u;
				y += v;
				return this;
			} // P.add(u,v): P+=<u,v>

			pt add(pt P)
			{
				x += P.x;
				y += P.y;
				return this;
			}; // incorrect notation, but useful for computing weighted averages

			pt add(float s, pt P)
			{
				x += s * P.x;
				y += s * P.y;
				return this;
			}; // adds s*P

			pt add(vec V)
			{
				x += V.x;
				y += V.y;
				return this;
			} // P.add(V): P+=V

			pt add(float s, vec V)
			{
				x += s * V.x;
				y += s * V.y;
				return this;
			} // P.add(s,V): P+=sV

			pt translateTowards(float s, pt P)
			{
				x += s * (P.x - x);
				y += s * (P.y - y);
				return this;
			}; // transalte by ratio s towards P

			pt scale(float u, float v)
			{
				x *= u;
				y *= v;
				return this;
			};

			pt scale(float s)
			{
				x *= s;
				y *= s;
				return this;
			} // P.scale(s): P*=s

			pt scale(float s, pt C)
			{
				x *= C.x + s * (x - C.x);
				y *= C.y + s * (y - C.y);
				return this;
			} // P.scale(s,C): scales wrt C: P=L(C,P,s);

			pt rotate(float a)
			{
				float dx = x, dy = y, c = cos(a), s = sin(a);
				x = c * dx + s * dy;
				y = -s * dx + c * dy;
				return this;
			}; // P.rotate(a): rotate P around origin by angle a in radians

			pt rotate(float a, pt G)
			{
				float dx = x - G.x, dy = y - G.y, c = cos(a), s = sin(a);
				x = G.x + c * dx + s * dy;
				y = G.y - s * dx + c * dy;
				return this;
			}; // P.rotate(a,G): rotate P around G by angle a in radians

			pt rotate(float s, float t, pt G)
			{
				float dx = x - G.x, dy = y - G.y;
				dx -= dy * t;
				dy += dx * s;
				dx -= dy * t;
				x = G.x + dx;
				y = G.y + dy;
				return this;
			}; // fast rotate s=sin(a); t=tan(a/2);

			pt moveWithMouse()
			{
				x += mouseX - pmouseX;
				y += mouseY - pmouseY;
				return this;
			};

			// DRAW , WRITE
			pt write()
			{
				print("(" + x + "," + y + ")");
				return this;
			}; // writes point coordinates in text window

			pt v()
			{
				vertex(x, y);
				return this;
			}; // used for drawing polygons between beginShape(); and endShape();

			pt show(float r)
			{
				ellipse(x, y, 2 * r, 2 * r);
				return this;
			}; // shows point as disk of radius r

			pt show()
			{
				show(3);
				return this;
			}; // shows point as small dot

			pt label(String s, float u, float v)
			{
				fill(black);
				text(s, x + u, y + v);
				noFill();
				return this;
			};

			pt label(String s, vec V)
			{
				fill(black);
				text(s, x + V.x, y + V.y);
				noFill();
				return this;
			};

			pt label(String s)
			{
				label(s, 5, 4);
				return this;
			};
		} // end of pt class

		// ************************************************************************
		// **** VECTOR CLASS
		// ************************************************************************
		class vec
		{
			float x = 0, y = 0;

			// CREATE
			vec()
			{
			};

			vec(float px, float py)
			{
				x = px;
				y = py;
			};

			// MODIFY
			vec setTo(float px, float py)
			{
				x = px;
				y = py;
				return this;
			};

			vec setTo(vec V)
			{
				x = V.x;
				y = V.y;
				return this;
			};

			vec zero()
			{
				x = 0;
				y = 0;
				return this;
			}

			vec scaleBy(float u, float v)
			{
				x *= u;
				y *= v;
				return this;
			};

			vec scaleBy(float f)
			{
				x *= f;
				y *= f;
				return this;
			};

			vec reverse()
			{
				x = -x;
				y = -y;
				return this;
			};

			vec divideBy(float f)
			{
				x /= f;
				y /= f;
				return this;
			};

			vec normalize()
			{
				float n = sqrt(sq(x) + sq(y));
				if (n > 0.000001)
				{
					x /= n;
					y /= n;
				}
				;
				return this;
			};

			vec add(float u, float v)
			{
				x += u;
				y += v;
				return this;
			};

			vec add(vec V)
			{
				x += V.x;
				y += V.y;
				return this;
			};

			vec add(float s, vec V)
			{
				x += s * V.x;
				y += s * V.y;
				return this;
			};

			vec rotateBy(float a)
			{
				float xx = x, yy = y;
				x = xx * cos(a) - yy * sin(a);
				y = xx * sin(a) + yy * cos(a);
				return this;
			};

			vec left()
			{
				float m = x;
				x = -y;
				y = m;
				return this;
			};

			// OUTPUT VEC
			@Override
			public vec clone()
			{
				return (new vec(x, y));
			};

			// OUTPUT TEST MEASURE
			float norm()
			{
				return (sqrt(sq(x) + sq(y)));
			}

			boolean isNull()
			{
				return ((abs(x) + abs(y) < 0.000001));
			}

			float angle()
			{
				return (atan2(y, x));
			}

			// DRAW, PRINT
			void write()
			{
				println("<" + x + "," + y + ">");
			};

			void showAt(pt P)
			{
				line(P.x, P.y, P.x + x, P.y + y);
			};

			void showArrowAt(pt P)
			{
				line(P.x, P.y, P.x + x, P.y + y);
				float n = min(this.norm() / 10.0f, height / 50.0f);
				pt Q = P(P, this);
				vec U = S(-n, U(this));
				vec W = S(.3f, R(U));
				beginShape();
				Q.add(U).add(W).v();
				Q.v();
				Q.add(U).add(M(W)).v();
				endShape(CLOSE);
			};

			void label(String s, pt P)
			{
				P(P).add(0.5f, this).add(3, R(U(this))).label(s);
			};
		} // end vec class

		// ************************************************************************
		// **** POINTS FUNCTIONS
		// ************************************************************************
		// create
		pt P()
		{
			return P(0, 0);
		}; // make point (0,0)

		pt P(float x, float y)
		{
			return new pt(x, y);
		}; // make point (x,y)

		pt P(pt P)
		{
			return P(P.x, P.y);
		}; // make copy of point A

		pt Mouse()
		{
			return P(mouseX, mouseY);
		}; // returns point at current mouse location

		pt Pmouse()
		{
			return P(pmouseX, pmouseY);
		}; // returns point at previous mouse location

		pt ScreenCenter()
		{
			return P(width / 2, height / 2);
		} // point in center of canvas

		// transform
		pt R(pt Q, float a)
		{
			float dx = Q.x, dy = Q.y, c = cos(a), s = sin(a);
			return new pt(c * dx + s * dy, -s * dx + c * dy);
		}; // Q rotated by angle a around the origin

		pt R(pt Q, float a, pt C)
		{
			float dx = Q.x - C.x, dy = Q.y - C.y, c = cos(a), s = sin(a);
			return P(C.x + c * dx - s * dy, C.y + s * dx + c * dy);
		}; // Q rotated by angle a around point P

		pt P(pt P, vec V)
		{
			return P(P.x + V.x, P.y + V.y);
		} // P+V (P transalted by vector V)

		pt P(pt P, float s, vec V)
		{
			return P(P, W(s, V));
		} // P+sV (P transalted by sV)

		pt MoveByDistanceTowards(pt P, float d, pt Q)
		{
			return P(P, d, U(V(P, Q)));
		}; // P+dU(PQ) (transLAted P by *distance* s towards Q)!!!

		// average
		pt P(pt A, pt B)
		{
			return P((float) (A.x + B.x) / 2.0f, (float) (A.y + B.y) / 2.0f);
		}; // (A+B)/2 (average)

		pt P(pt A, pt B, pt C)
		{
			return P((A.x + B.x + C.x) / 3.0f, (A.y + B.y + C.y) / 3.0f);
		}; // (A+B+C)/3 (average)

		pt P(pt A, pt B, pt C, pt D)
		{
			return P(P(A, B), P(C, D));
		}; // (A+B+C+D)/4 (average)

		// weighted average
		pt P(float a, pt A)
		{
			return P(a * A.x, a * A.y);
		} // aA

		pt P(float a, pt A, float b, pt B)
		{
			return P(a * A.x + b * B.x, a * A.y + b * B.y);
		} // aA+bB, (a+b=1)

		pt P(float a, pt A, float b, pt B, float c, pt C)
		{
			return P(a * A.x + b * B.x + c * C.x, a * A.y + b * B.y + c * C.y);
		} // aA+bB+cC

		pt P(float a, pt A, float b, pt B, float c, pt C, float d, pt D)
		{
			return P(a * A.x + b * B.x + c * C.x + d * D.x, a * A.y + b * B.y + c
					* C.y + d * D.y);
		} // aA+bB+cC+dD

		// LERP
		pt L(pt A, pt B, float t)
		{
			return P(A.x + t * (B.x - A.x), A.y + t * (B.y - A.y));
		}
		
		//Generalized LERP
		pt GL(float a, pt A, float b, pt B, float t)
		{
			//GL = (b-t) / (b-a) A + (t-a) / (b-a) B
			return P(((b-t) / (b-a) * A.x + (t-a) / (b-a) * B.x), ((b-t) / (b-a) * A.y + (t-a) / (b-a) * B.y));
		}

		// measure
		boolean isSame(pt A, pt B)
		{
			return (A.x == B.x) && (A.y == B.y);
		} // A==B

		boolean isSame(pt A, pt B, float e)
		{
			return ((abs(A.x - B.x) < e) && (abs(A.y - B.y) < e));
		} // ||A-B||<e

		float d(pt P, pt Q)
		{
			return sqrt(d2(P, Q));
		}; // ||AB|| (Distance)

		float d2(pt P, pt Q)
		{
			return sq(Q.x - P.x) + sq(Q.y - P.y);
		}; // AB*AB (Distance squared)

		// ************************************************************************
		// **** VECTOR FUNCTIONS
		// ************************************************************************
		// create
		vec V(vec V)
		{
			return new vec(V.x, V.y);
		}; // make copy of vector V

		vec V(pt P)
		{
			return new vec(P.x, P.y);
		}; // make vector from origin to P

		vec V(float x, float y)
		{
			return new vec(x, y);
		}; // make vector (x,y)

		vec V(pt P, pt Q)
		{
			return new vec(Q.x - P.x, Q.y - P.y);
		}; // PQ (make vector Q-P from P to Q

		vec U(vec V)
		{
			float n = n(V);
			if (n == 0)
				return new vec(0, 0);
			else
				return new vec(V.x / n, V.y / n);
		}; // V/||V|| (Unit vector : normalized version of V)

		vec U(pt P, pt Q)
		{
			return U(V(P, Q));
		}; // PQ/||PQ| (Unit vector : from P towards Q)

		vec MouseDrag()
		{
			return new vec(mouseX - pmouseX, mouseY - pmouseY);
		}; // vector representing recent mouse displacement

		// weighted sum
		vec W(float s, vec V)
		{
			return V(s * V.x, s * V.y);
		} // sV

		vec W(vec U, vec V)
		{
			return V(U.x + V.x, U.y + V.y);
		} // U+V

		vec W(vec U, float s, vec V)
		{
			return W(U, S(s, V));
		} // U+sV

		vec W(float u, vec U, float v, vec V)
		{
			return W(S(u, U), S(v, V));
		} // uU+vV ( Linear combination)

		// transformed
		vec R(vec V)
		{
			return new vec(-V.y, V.x);
		}; // V turned right 90 degrees (as seen on screen)

		vec R(vec V, float a)
		{
			float c = cos(a), s = sin(a);
			return (new vec(V.x * c - V.y * s, V.x * s + V.y * c));
		}; // V rotated by a radians

		vec S(float s, vec V)
		{
			return new vec(s * V.x, s * V.y);
		}; // sV

		vec Reflection(vec V, vec N)
		{
			return W(V, -2.0f * dot(V, N), N);
		}; // reflection

		vec M(vec V)
		{
			return V(-V.x, -V.y);
		} // -V

		// Interpolation
		vec L(vec U, vec V, float s)
		{
			return new vec(U.x + s * (V.x - U.x), U.y + s * (V.y - U.y));
		}; // (1-s)U+sV (Linear interpolation between vectors)

		vec S(vec U, vec V, float s)
		{
			float a = angle(U, V);
			vec W = R(U, s * a);
			float u = n(U), v = n(V);
			return W(pow(v / u, s), W);
		} // steady interpolation from U to V

		// measure
		float dot(vec U, vec V)
		{
			return U.x * V.x + U.y * V.y;
		} // dot(U,V): U*V (dot product U*V)

		float det(vec U, vec V)
		{
			return dot(R(U), V);
		} // det | U V | = scalar cross UxV

		float n(vec V)
		{
			return sqrt(dot(V, V));
		}; // n(V): ||V|| (norm: length of V)

		float n2(vec V)
		{
			return sq(V.x) + sq(V.y);
		}; // n2(V): V*V (norm squared)

		boolean parallel(vec U, vec V)
		{
			return dot(U, R(V)) == 0;
		};

		float angle(vec U, vec V)
		{
			return atan2(det(U, V), dot(U, V));
		}; // angle <U,V> (between -PI and PI)

		float angle(vec V)
		{
			return (atan2(V.y, V.x));
		}; // angle between <1,0> and V (between -PI and PI)

		float angle(pt A, pt B, pt C)
		{
			return angle(V(B, A), V(B, C));
		} // angle <BA,BC>

		float turnAngle(pt A, pt B, pt C)
		{
			return angle(V(A, B), V(B, C));
		} // angle <AB,BC> (positive when right turn as seen on screen)

		int toDeg(float a)
		{
			return (int) (a * 180 / PI);
		} // convert radians to degrees

		float toRad(float a)
		{
			return (a * PI / 180);
		} // convert degrees to radians

		float positive(float a)
		{
			if (a < 0)
				return a + TWO_PI;
			else
				return a;
		} // adds 2PI to make angle positive

		// SLERP
		vec slerp(vec U, float t, vec V)
		{
			float a = angle(U, V);
			float b = sin((1.0f - t) * a), c = sin(t * a), d = sin(a);
			return W(b / d, U, c / d, V);
		} // UNIT vectors ONLY!

		// ************************************************************************
		// **** DISPLAY
		// ************************************************************************
		// point / polygon
		void show(pt P, float r)
		{
			ellipse(P.x, P.y, 2 * r, 2 * r);
		}; // draws circle of center r around P

		void show(pt P)
		{
			ellipse(P.x, P.y, 6, 6);
		}; // draws small circle around point

		// edge / arrow
		void edge(pt P, pt Q)
		{
			line(P.x, P.y, Q.x, Q.y);
		}; // draws edge (P,Q)

		void arrow(pt P, pt Q)
		{
			arrow(P, V(P, Q));
		} // draws arrow from P to Q

		void show(pt P, vec V)
		{
			line(P.x, P.y, P.x + V.x, P.y + V.y);
		} // show V as line-segment from P

		void show(pt P, float s, vec V)
		{
			show(P, S(s, V));
		} // show sV as line-segment from P

		void arrow(pt P, float s, vec V)
		{
			arrow(P, S(s, V));
		} // show sV as arrow from P

		void arrow(pt P, vec V, String S)
		{
			arrow(P, V);
			P(P(P, 0.70f, V), 15, R(U(V))).label(S, V(-5, 4));
		} // show V as arrow from P and print string S on its side

		void arrow(pt P, vec V)
		{
			show(P, V);
			float n = n(V);
			if (n < 0.01)
				return;
			float s = max(min(0.2f, 20.0f / n), 6.0f / n); // show V as arrow from P
			pt Q = P(P, V);
			vec U = S(-s, V);
			vec W = R(S(.3f, U));
			beginShape();
			v(P(P(Q, U), W));
			v(Q);
			v(P(P(Q, U), -1, W));
			endShape(CLOSE);
		};

		// triangle, polygon
		void v(pt P)
		{
			vertex(P.x, P.y);
		}; // vertex for drawing polygons between beginShape() and endShape()

		void show(pt A, pt B, pt C)
		{
			beginShape();
			A.v();
			B.v();
			C.v();
			endShape(CLOSE);
		} // render triangle A, B, C

		void show(pt A, pt B, pt C, pt D)
		{
			beginShape();
			A.v();
			B.v();
			C.v();
			D.v();
			endShape(CLOSE);
		} // render quad A, B, C, D

		// text
		void label(pt P, String S)
		{
			text(S, P.x - 4, P.y + 6.5f);
		} // writes string S next to P on the screen ( for example
		// label(P[i],str(i));)

		void label(pt P, vec V, String S)
		{
			text(S, P.x - 3.5f + V.x, P.y + 7 + V.y);
		} // writes string S at P+V

		// *****************************************************************************
		// TITLE: Point sequence for polylines and polyloops
		// AUTHOR: Prof Jarek Rossignac
		// DATE CREATED: September 2012
		// EDITS: Last revised Sept 10, 2012
		// *****************************************************************************
		class pts
		{
			int nv = 0;
			int pv = 0; // picked vertex
			int maxnv = 1000; // max number of vertices
//			pt[] G = new pt[maxnv]; // geometry table (vertices)
			ArrayList<pt> G = new ArrayList<pt>();

			public pts()
			{
			}
			
			public pts(pts pts){
				maxnv = pts.maxnv;
				addAllPts(pts.G);
			}

			pts empty()
			{
				nv = 0;
				return this;
			}

			pts addPt(pt P)
			{
				G.add(P);
				pv = nv;
				nv++;
				return this;
			}

			pts addPt(float x, float y)
			{
				pt P = new pt(x, y);
				addPt(P);
				return this;
			}
			
			pts addAllPts(ArrayList<pt> points)
			{
				for(pt P: points)
				{
					addPt(P.x, P.y);
				}
				
				return this;
			}
			
			pts addPtAt(int index, pt P)
			{
				G.add(index, P);
				pv = index;
				nv++;
				return this;
			}

			pts addPtAt(int index, float x, float y)
			{
				pt P = new pt(x, y);
				addPtAt(index, P);
				return this;
			}

			pts resetOnCircle(int k)
			{ // init the points to be on a circle
				pt C = ScreenCenter();
				for (int i = 0; i < k; i++)
					addPt(R(P(C, V(0, -width / 3f)), 2.0f * PI * i / k, C));
				return this;
			}

			pts makeGrid(int w)
			{ // make a 2D grid of w x w vertices
				for (int i = 0; i < w; i++)
					for (int j = 0; j < w; j++)
						addPt(P(.7f * height * j / (w - 1) + .1f * height, .7f
								* height * i / (w - 1) + .1f * height));
				return this;
			}

			pts deletePickedPt()
			{
				G.remove(pv);
				pv = max(0, pv - 1);
				nv--;
				return this;
			}

			pts setPt(pt P, int i)
			{
				G.get(i).setTo(P);
				return this;
			}

			pts IDs()
			{
				for (int v = 0; v < nv; v++)
				{
					fill(white);
					show(G.get(v), 13);
					fill(black);
					if (v < 10)
						label(G.get(v), str(v));
					else
						label(G.get(v), V(-5, 0), str(v));
				}
				noFill();
				return this;
			}

			pts showPicked()
			{
				show(G.get(pv), 13);
				return this;
			}

			pts draw(int c)
			{
				fill(c);
				for (int v = 0; v < nv; v++)
					show(G.get(v), 13);
				return this;
			}

			pts draw()
			{
				for (int v = 0; v < nv; v++)
					show(G.get(v), 13);
				return this;
			}

			pts drawCurve()
			{
				beginShape();
				for (int v = 0; v < nv; v++)
					G.get(v).v();
				endShape();
				return this;
			}
			
			pts drawClosedCurve()
			{
				beginShape();
				
				for(pt P : G)
				{
					P.v();
				}
				
				G.get(0);
					
				endShape();
				return this;
			}
			
			pts drawFilledCurve(int r, int g, int b)
			{
				drawFilledCurve(r, g, b, 255);
				return this;
			}
			
			pts drawFilledCurve(int r, int g, int b, int alpha)
			{
				fill(r, g, b, alpha);
				drawCurve();
				noFill();
				return this;
			}
			
			pts drawFilledCurve(int val)
			{
				fill(val);
				drawCurve();
				noFill();
				return this;
			}
			
			pts drawFilledClosedCurve(int r, int g, int b)
			{
				drawFilledClosedCurve(r, g, b, 255);
				return this;
			}
			
			pts drawFilledClosedCurve(int r, int g, int b, int alpha)
			{
				fill(r, g, b, alpha);
				drawClosedCurve();
				noFill();
				return this;
			}
			
			pts drawFilledClosedCurve(int val)
			{
				fill(val);
				drawClosedCurve();
				noFill();
				return this;
			}

			void pickClosest(pt M)
			{
				pv = 0;
				for (int i = 1; i < nv; i++)
					if (d(M, G.get(i)) < d(M, G.get(pv)))
						pv = i;
			}

			pt Centroid()
			{
				pt C = P();
				for (int i = 0; i < nv; i++)
					C.add(G.get(i));
				return P(1.0f / nv, C);
			}

			pts dragPicked()
			{
				G.get(pv).moveWithMouse();
				return this;
			} // moves selected point (index p) by amount mouse moved recently

			pts dragAll()
			{
				for (int i = 0; i < nv; i++)
					G.get(i).moveWithMouse();
				return this;
			} // moves selected point (index p) by amount mouse moved recently

			pts moveAll(vec V)
			{
				for (int i = 0; i < nv; i++)
					G.get(i).add(V);
				return this;
			};

			pts rotateAll(float a, pt C)
			{
				for (int i = 0; i < nv; i++)
					G.get(i).rotate(a, C);
				return this;
			}; // rotates points around pt G by angle a

			pts rotateAllAroundCentroid(float a)
			{
				rotateAll(a, Centroid());
				return this;
			}; // rotates points around their center of mass by angle a

			pts rotateAll(pt G, pt P, pt Q)
			{
				rotateAll(angle(V(G, P), V(G, Q)), Centroid());
				return this;
			}; // rotates points around G by angle <GP,GQ>

			pts rotateAllAroundCentroid(pt P, pt Q)
			{
				rotateAll(Centroid(), P, Q);
				return this;
			}; // rotates points around their center of mass G by angle <GP,GQ>

			pts scaleAll(float s, pt C)
			{
				for (int i = 0; i < nv; i++)
					G.get(i).translateTowards(s, C);
				return this;
			};

			pts scaleAllAroundCentroid(float s)
			{
				scaleAll(s, Centroid());
				return this;
			};

			pts scaleAllAroundCentroid(pt M, pt P)
			{
				pt C = Centroid();
				float m = d(C, M), p = d(C, P);
				scaleAll((p - m) / p, C);
				return this;
			};

			pts fitToCanvas()
			{ // translates and scales mesh to fit canvas
				float sx = 100000;
				float sy = 10000;
				float bx = 0.0f;
				float by = 0.0f;
				for (int i = 0; i < nv; i++)
				{
					if (G.get(i).x > bx)
					{
						bx = G.get(i).x;
					}
					;
					if (G.get(i).x < sx)
					{
						sx = G.get(i).x;
					}
					;
					if (G.get(i).y > by)
					{
						by = G.get(i).y;
					}
					;
					if (G.get(i).y < sy)
					{
						sy = G.get(i).y;
					}
					;
				}
				for (int i = 0; i < nv; i++)
				{
					G.get(i).x = 0.93f * (G.get(i).x - sx) * (width) / (bx - sx) + 23;
					G.get(i).y = 0.90f * (G.get(i).y - sy) * (height - 100) / (by - sy)
							+ 100;
				}
				return this;
			}

			// void savePts() {
			// String savePath =
			// selectOutput("Select or specify file name where the points will be saved");
			// // Opens file chooser
			// if (savePath == null) {println("No output file was selected...");
			// return;}
			// else println("writing to "+savePath);
			// savePts(savePath);
			// }

			void savePts(String fn)
			{
				String[] inppts = new String[nv + 1];
				int s = 0;
				inppts[s++] = str(nv);
				for (int i = 0; i < nv; i++)
				{
					inppts[s++] = str(G.get(i).x) + "," + str(G.get(i).y);
				}
				saveStrings(fn, inppts);
			};

			// void loadPts() {
			// String loadPath = selectInput("Select file to load"); // Opens file
			// chooser
			// if (loadPath == null) {println("No input file was selected...");
			// return;}
			// else println("reading from "+loadPath);
			// loadPts(loadPath);
			// }

			void loadPts(String fn)
			{
				println("loading: " + fn);
				String[] ss = loadStrings(fn);
				String subpts;
				int s = 0;
				int comma, comma1, comma2;
				float x, y;
				int a, b, c;
				nv = (int) Integer.valueOf(ss[s++]);
				print("nv=" + nv);
				for (int k = 0; k < nv; k++)
				{
					int i = k + s;
					comma = ss[i].indexOf(',');
					x = (float) Float.valueOf(ss[i].substring(0, comma));
					y = (float) Float.valueOf(ss[i].substring(comma + 1,
							ss[i].length()));
					G.get(k).setTo(x, y);
				}
				;
				pv = 0;
			};

		} // end class pts

		// LecturesInGraphics: utilities
		// Author: Jarek ROSSIGNAC, last edited on July 21, 2015
		PImage myFace; // picture of author's face, should be: data/pic.jpg in
		// sketch folder

		// ************************************************************************
		// COLORS
		int black = color(0,0,0),
				white = color(255,255,255),
				red = color(255,0,0), green = color(0,255,0), blue = color(0,0,255),
				yellow = color(255,255,0), cyan = color(0,255,255), magenta = color(255,0,255),
				grey = color(127,127,127), brown = color(175,100,0), sand = color(252,186,105),
				pink = color(255,142,231), light_blue = color(0, 0, 127), violet = color(140, 110, 148),
				dark_green = color(124, 148, 110), dark_red = color(145, 65, 65);

		// ************************************************************************
		// GRAPHICS
		void pen(int c, float w)
		{
			stroke(c);
			strokeWeight(w);
		}

		void showDisk(float x, float y, float r)
		{
			ellipse(x, y, r * 2, r * 2);
		}

		// ************************************************************************
		// SAVING INDIVIDUAL IMAGES OF CANVAS
		boolean snapPic = false;
		String PicturesOutputPath = "data/PDFimages";
		int pictureCounter = 0;

		void snapPicture()
		{
			saveFrame("PICTURES/P" + nf(pictureCounter++, 3) + ".jpg");
		}

		// ************************ SAVING IMAGES for a MOVIE
		boolean filming = false; // when true frames are captured in FRAMES for a
		// movie
		int frameCounter = 0; // count of frames captured (used for naming the image
		// files)
		boolean change = false; // true when the user has presed a key or moved the
		// mouse
		boolean animating = false; // must be set by application during animations
		// to force frame capture
		/*
		 * To make a movie : Press '~' to start filming, act the movie or start an
		 * animation, press '~' to pause/stop (you can restart to add frames) Then,
		 * from within your Processing sketch, from the processing menu, select
		 * Tools > Movie Maker. Click on Choose� Navigate to your Sketch Folder.
		 * Select, but do not open, the FRAMES folder. Press Create Movie, Select
		 * the parameters you want.
		 * 
		 * May not work for a large canvas!
		 */

		// ************************************************************************
		// TEXT
		Boolean scribeText = true; // toggle for displaying of help text

		void scribe(String S, float x, float y)
		{
			fill(0);
			text(S, x, y);
			noFill();
		} // writes on screen at (x,y) with current fill color

		void scribeHeader(String S, int i)
		{
			text(S, 10, 20 + i * 20);
			noFill();
		} // writes black at line i

		void scribeHeaderRight(String S)
		{
			fill(0);
			text(S, width - 7.5f * S.length(), 20);
			noFill();
		} // writes black on screen top, right-aligned

		void scribeFooter(String S, int i)
		{
			fill(0);
			text(S, 10, height - 10 - i * 20);
			noFill();
		} // writes black on screen at line i from bottom

		void scribeAtMouse(String S)
		{
			fill(0);
			text(S, mouseX, mouseY);
			noFill();
		} // writes on screen near mouse

		void scribeMouseCoordinates()
		{
			fill(black);
			text("(" + mouseX + "," + mouseY + ")", mouseX + 7, mouseY + 25);
			noFill();
		}

		void displayHeader()
		{ // Displays title and authors face on screen
			scribeHeader(title, 0);
			scribeHeaderRight(name);
		}

		void displayFooter()
		{ // Displays help text at the bottom
			scribeFooter(guide, 1);
			scribeFooter(menu, 0);
		}

		// ************************************************************************
		// **** SPIRAL
		// ************************************************************************
		pt PtOnSpiral(pt A, pt B, pt C, float t)
		{
			float a = spiralAngle(A, B, B, C);
			float s = spiralScale(A, B, B, C);
			pt G = spiralCenter(a, s, A, B);
			return L(G, R(B, t * a, G), pow(s, t));
		}

		pt spiralPt(pt A, pt G, float s, float a)
		{
			return L(G, R(A, a, G), s);
		}

		pt spiralPt(pt A, pt G, float s, float a, float t)
		{
			return L(G, R(A, t * a, G), pow(s, t));
		}

		pt spiralCenter(pt A, pt B, pt C, pt D)
		{ // computes center of spiral that takes A to C and B to D
			float a = spiralAngle(A, B, C, D);
			float z = spiralScale(A, B, C, D);
			return spiralCenter(a, z, A, C);
		}

		float spiralAngle(pt A, pt B, pt C, pt D)
		{
			return angle(V(A, B), V(C, D));
		}

		float spiralScale(pt A, pt B, pt C, pt D)
		{
			return d(C, D) / d(A, B);
		}

		pt spiralCenter(float a, float z, pt A, pt C)
		{
			float c = cos(a), s = sin(a);
			float D = sq(c * z - 1) + sq(s * z);
			float ex = c * z * A.x - C.x - s * z * A.y;
			float ey = c * z * A.y - C.y + s * z * A.x;
			float x = (ex * (c * z - 1) + ey * s * z) / D;
			float y = (ey * (c * z - 1) - ex * s * z) / D;
			return P(x, y);
		}
		
		//************************************************************************
		//**** CIRCLES
		//************************************************************************
		// create 
		float circumRadius (pt A, pt B, pt C) {float a=d(B,C), b=d(C,A), c=d(A,B), s=(a+b+c)/2, d=sqrt(s*(s-a)*(s-b)*(s-c)); return a*b*c/4/d;} // radiusCircum(A,B,C): radius of circumcenter
		pt CircumCenter (pt A, pt B, pt C) {vec AB = V(A,B); vec AC = R(V(A,C)); 
		   return P(A,1f/2/dot(AB,AC),W(-n2(AC),R(AB),n2(AB),AC)); }; // CircumCenter(A,B,C): center of circumscribing circle, where medians meet)

		// display 
		void drawCircle(int n) {  
		  float x=1, y=0; float a=TWO_PI/n, t=tan(a/2), s=sin(a); 
		  beginShape(); for (int i=0; i<n; i++) {x-=y*t; y+=x*s; x-=y*t; vertex(x,y);} endShape(CLOSE);}


		void showArcThrough (pt A, pt B, pt C) {
		  if (abs(dot(V(A,B),R(V(A,C))))<0.01*d2(A,C)) {edge(A,C); return;}
		   pt O = CircumCenter ( A,  B,  C); 
		   float r=d(O,A);
		   vec OA=V(O,A), OB=V(O,B), OC=V(O,C);
		   float b = angle(OA,OB), c = angle(OA,OC); 
		   if(0<c && c<b || b<0 && 0<c)  c-=TWO_PI; 
		   else if(b<c && c<0 || c<0 && 0<b)  c+=TWO_PI; 
		   beginShape(); v(A); for (float t=0; t<1; t+=0.01) v(R(A,t*c,O)); v(C); endShape();
		   }

		pt pointOnArcThrough (pt A, pt B, pt C, float t) { // July 2011
		  if (abs(dot(V(A,B),R(V(A,C))))<0.001*d2(A,C)) {edge(A,C); return L(A,C,t);}
		   pt O = CircumCenter ( A,  B,  C); 
		   float r=(d(O,A) + d(O,B)+ d(O,C))/3;
		   vec OA=V(O,A), OB=V(O,B), OC=V(O,C);
		   float b = angle(OA,OB), c = angle(OA,OC); 
		   if(0<b && b<c) {}
		   else if(0<c && c<b) {b=b-TWO_PI; c=c-TWO_PI;}
		   else if(b<0 && 0<c) {c=c-TWO_PI;}
		   else if(b<c && c<0) {b=TWO_PI+b; c=TWO_PI+c;}
		   else if(c<0 && 0<b) {c=TWO_PI+c;}
		   else if(c<b && b<0) {}
		   return R(A,t*c,O);
		   }
}
