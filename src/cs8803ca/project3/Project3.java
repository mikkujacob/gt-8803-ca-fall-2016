
package cs8803ca.project3;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import controlP5.ControlP5;
import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;

/**
 * Project 3 that has a color wheel on the LHS of screen. A user sketch canvas is on 
 * RHS of screen. User can add points to a color wheel based on a single picked 
 * starting color. The added points are shades that can be used to automatically 
 * paint the user sketch.
 * @author mikhail.jacob
 *
 */
public class Project3 extends PApplet
{
	private static final Boolean isFullScreen = true;
	PGraphics canvas;
	int pixelsTouched;
	int screenX, screenY, windowW = 600, windowH = 600;
	Boolean lightnessChange = true, chromaChange = true, hueChange = true, 
			shuffleColor = false, paintAll = true;
	boolean filming = false; // when true frames are captured in FRAMES for a
	// movie
	int frameCounter = 0; // count of frames captured (used for naming the image
	// files)
	ColorWheel wheel;
	ControlP5 cp5;
	Point userColorPoint;
	ArrayList<Point> colorPoints;
	int maxColorPoints;
	int numColorPoints;
	ArrayList<Integer> colors;
	ArrayList<Integer> backupColors;
	int colorIndex;
	ArrayList<ArrayList<Point>> sketchPoints;
	int sketchStrokeIndex;
	Random rand;
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		if(isFullScreen)
		{
			PApplet.main(new String[] { "--present", "cs8803ca.project3.Project3" });
		}
		else
		{
			PApplet.main(new String[] { "cs8803ca.project3.Project3" });
		}
	}
	
	/**
	 * 
	 */
	public void setup()
	{
		if(isFullScreen)
		{
//			canvas = createGraphics((int)(displayWidth * 2f / 3f), displayHeight);
			canvas = createGraphics(displayWidth, displayHeight);
			
			wheel = new ColorWheel(this, 50); // L has range 0 - 100
		}
		else
		{
//			canvas = createGraphics((int)(windowW * 2f / 3f), windowH);
			canvas = createGraphics(windowW, windowH);
			
			wheel = new ColorWheel(this, 50); // L has range 0 - 100
		}
		
		cp5 = new ControlP5(this);
		// name, minValue, maxValue, defaultValue, x, y, width, height
		cp5.addTextlabel("instructionLabel", "Choose colors for your sketch using the color wheel and the points you add to it. \nKeys: G: Grid/L: L Gradient/C: C Gradient/H: H Gradient/E: Erase All/R: Erase Paint/P: Auto or Click Paint", 40, 100).setColorValue(color(124, 124, 124)).setFont(createFont("Georgia",28));
		
		cp5.addSlider("lightnessSlider", 0f, 100f, 50f, 40, 200, 400, 40).setLabelVisible(false);
		cp5.addTextlabel("lightnessLabel", "Adjust Lightness Value", 40, 250).setColorValue(color(124, 124, 124)).setFont(createFont("Georgia",20));
		
		cp5.addSlider("colorPointSlider", 3, 24, 5, 40, 800, 400, 40).setLabelVisible(false);
		cp5.addTextlabel("colorPointLabel", "Adjust Shape of Color Points", 40, 850).setColorValue(color(124, 124, 124)).setFont(createFont("Georgia",20));
		maxColorPoints = 5;
		
		cp5.addButton("addColorPointButton").setPosition(40, 730).setSize(100, 40).setCaptionLabel("Add Points");
		cp5.addButton("removeColorPointButton").setPosition(190, 730).setSize(100, 40).setCaptionLabel("Remove Points");
		cp5.addButton("paintButton").setPosition(340, 730).setSize(100, 40).setCaptionLabel("Paint Sketch");
		
		userColorPoint = wheel.getCenter();
		colorPoints = new ArrayList<>();
		numColorPoints = 1;
		
		colors = new ArrayList<Integer>();
		backupColors = new ArrayList<Integer>();
		colorIndex = 0;
		
		sketchPoints = new ArrayList<ArrayList<Point>>();
		sketchPoints.add(new ArrayList<Point>());
		sketchStrokeIndex = 0;
		
		rand = new Random();
	}
	
	/**
	 * 
	 */
	public void settings()
	{
		if(isFullScreen)
		{
			fullScreen();
		}
		else
		{
			size(windowW, windowH); // window size
		}
		screenX = width;
		screenY = height;
		smooth(); // turn on antialiasing
	}
	
	/**
	 * 
	 */
	public void draw()
	{
		if(screenX != width && screenY != height)
		{
			//Window size changed
//			canvas = createGraphics((int)(width * 2f / 3f), height);
			canvas = createGraphics(width, height);
			
			screenX = width;
			screenY = height;
		}
		
		noStroke();
		noFill();
		background(color(255,255,255));
		
		drawCanvas(canvas, color(0,0,0));
		
		image(canvas, 0, 0);
		
		wheel.draw();
		
		drawColorPoints();
		
		if (filming)
		{
			//Take an actual screenshot.
			PImage screenShot = takeScreenShot();
			screenShot.save("FRAMES/F" + nf(frameCounter++, 5) + ".tif");
//			saveFrame("FRAMES/F" + nf(frameCounter++, 4) + ".tif"); // saves a
		// movie
		// frame
		}
	}
	
	public PImage takeScreenShot()
	{
	    Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
	    
	    try {
	        
	        BufferedImage screenBuffer = new Robot().createScreenCapture(screenRect);
	        
//	        PImage screenShot = new PImage(screenBuffer.getWidth(), screenBuffer.getHeight(), ARGB);
	        PImage screenShot = createImage(screenBuffer.getWidth(), screenBuffer.getHeight(), ARGB);
	        screenBuffer.getRGB(0, 0, screenShot.width, screenShot.height, screenShot.pixels, 0, screenShot.width);
	        screenShot.updatePixels();
	        
	        return screenShot;
	        
	    } catch ( AWTException e ) {
	        e.printStackTrace();
	    }
	    
	    return null;
	}
	
	public void lightnessSlider(float lightness)
	{
		wheel.setLightness(Math.round(lightness));
		
		userColorPoint.z = lightness;
		
		for(Point p : colorPoints)
		{
			p.z = lightness;
		}
	}
	
	public void colorPointSlider(int points)
	{
		maxColorPoints = points;
	}
	
	public void addColorPointButton()
	{
		numColorPoints++;
		
		wheel.computeColorPoints(colorPoints, userColorPoint, numColorPoints, maxColorPoints, lightnessChange, chromaChange, hueChange);
		
		if(!paintAll)
		{
			reinitColors();
		}
	}
	
	public void removeColorPointButton()
	{
		if(numColorPoints > 1)
		{
			numColorPoints--;
			
			wheel.computeColorPoints(colorPoints, userColorPoint, numColorPoints, maxColorPoints, lightnessChange, chromaChange, hueChange);
			
			if(!paintAll)
			{
				reinitColors();
			}
		}
	}
	
	public void paintButton()
	{
		if(paintAll)
		{
			paintAllSketch();
		}
		else
		{
			int randX = rand.nextInt((int)(width * 2f / 3f - 40)), randY = rand.nextInt((height - 240));
			randX += (int)(width * 1f / 3f);
			randY += 200;
			paintFill(randX, randY);
		}
	}
	
	public void paintFill(int x, int y)
	{
		canvas.beginDraw();
		canvas.loadPixels();
		
		if(canvas.pixels[y * canvas.width + x] != color(255,255,255) && canvas.pixels[y * canvas.width + x] != 0)
		{
//			System.out.println("RETURNED: " + canvas.pixels[y * canvas.width + x]);
			canvas.updatePixels();
			canvas.endDraw();
			return;
		}
		
		Random rand = new Random();
		colorIndex = (shuffleColor) ? rand.nextInt(colors.size()) : colorIndex;
		
		eightFloodFill(x, y, colors.get(colorIndex), color(0, 0, 0), canvas);
		
		if(shuffleColor)
		{

			backupColors.add(colors.get(colorIndex));
			colors.remove(colorIndex);
			
			if(colors.isEmpty())
			{
				colors.addAll(backupColors);
				backupColors.clear();
			}
			
			colorIndex = rand.nextInt(colors.size());
		}
		else
		{
			colorIndex = (colorIndex + 1) % colors.size();
		}
		
		canvas.updatePixels();
		canvas.endDraw();
	}
	
	public void paintUnfill(int x, int y)
	{
		canvas.beginDraw();
		canvas.loadPixels();
		
		if(canvas.pixels[y * canvas.width + x] == color(255,255,255) && canvas.pixels[y * canvas.width + x] != 0)
		{
//			System.out.println("RETURNED: " + canvas.pixels[y * canvas.width + x]);
			canvas.updatePixels();
			canvas.endDraw();
			return;
		}
		
		eightFloodUnfill(x, y, color(255,255,255), color(0, 0, 0), canvas);
		
		canvas.updatePixels();
		canvas.endDraw();
	}
	
	public void paintAllSketch()
	{
		int blackPixels = 0;
		int colorPixels = 0;
		this.pixelsTouched = 0;
		int dimension = canvas.width * canvas.height;
		canvas.beginDraw();
		canvas.loadPixels();
		
		ArrayList<Integer> colors = new ArrayList<Integer>();
		ArrayList<Integer> backupColors = new ArrayList<Integer>();
		colors.add(wheel.pointToColor(userColorPoint));
		for(Point p : colorPoints)
		{
			colors.add(wheel.pointToColor(p));
		}
		
		System.out.println("Colors: " + colors.size());
		
		for(int i = 0; i < dimension; i++)
		{
			if(canvas.pixels[i] != color(0,0,0))
			{
				canvas.pixels[i] = color(255,255,255);
			}
		}
		
		int colorIndex = (shuffleColor) ? rand.nextInt(colors.size()) : 0;
		
		//Boundary is mouseX > (int)(width * 1f / 3f) && mouseY > 200 && mouseX < (width - 40) && mouseY < (height - 40)
		for(int index = 0, x = 0, y = 0; index < dimension; index++, x++)
		{
			if(x >= canvas.width)
			{
				x = 0;
				y++;
			}
			if(y >= canvas.height)
			{
				y = 0;
			}
			
			if(x > (int)(width * 1f / 3f) && y > 200 && x < (width - 40) && y < (height - 40))
			{
				if(canvas.pixels[y * canvas.width + x] == color(255, 255, 255))
				{
					eightFloodFill(x, y, colors.get(colorIndex), color(0, 0, 0), canvas);
					
					if(shuffleColor)
					{

						backupColors.add(colors.get(colorIndex));
						colors.remove(colorIndex);
						
						if(colors.isEmpty())
						{
							colors.addAll(backupColors);
							backupColors.clear();
						}
						
						colorIndex = rand.nextInt(colors.size());
					}
					else
					{
						colorIndex = (colorIndex + 1) % colors.size();
					}
				}
				else if(canvas.pixels[y * canvas.width + x] == color(0,0,0))
				{
					blackPixels++;
				}
				else
				{
					colorPixels++;
				}
			}
			
		}
		
		canvas.updatePixels();
		canvas.endDraw();
		System.out.println("Pixels Touched: " + pixelsTouched + ", Color Pixels: " + colorPixels + ", Black Pixels: " + blackPixels + ", Paint Dimension: " + (int)(width * 2f / 3f - 40) * (height - 240));
	}
	
	public void eightFloodFill(int x, int y, int fillColor, int boundaryColor, PGraphics canvas)
	{
		if(canvas.pixels == null)
		{
			System.out.println("ERROR! Pixels NULL!");
			return;
		}
		
		int pixel = canvas.pixels[y * canvas.width + x];
		if(pixel == fillColor || pixel == boundaryColor || pixel != color(255, 255, 255))
		{
//			System.out.println("returned");
			return;
		}
		
		ArrayList<Point> nodes = new ArrayList<Point>();
		HashMap<String, Boolean> nodesSeen = new HashMap<String, Boolean>();
		
		nodes.add(new Point(x, y));
		nodesSeen.put("" + x + "," + y, true);
		
//		int loopCount = 0;
		while (!nodes.isEmpty())
		{
			Point p = nodes.remove(0);
			if (canvas.pixels[(int) p.y * canvas.width + (int) p.x] == color(
					255, 255, 255))
			{
				canvas.pixels[(int) p.y * canvas.width + (int) p.x] = fillColor;
				pixelsTouched++;
			}

			if (!nodesSeen.containsKey("" + ((int) p.x - 1) + "," + ((int) p.y - 1))
					&& (int) p.x - 1 > (int) (width * 1f / 3f)
					&& (int) p.x - 1 < (canvas.width - 40)
					&& (int) p.y - 1 > 200
					&& (int) p.y - 1 < (canvas.height - 40)
					&& canvas.pixels[((int) p.y - 1) * canvas.width + ((int) p.x - 1)] == color(
							255, 255, 255))
			{
				nodes.add(new Point((int) p.x - 1, (int) p.y - 1));
				nodesSeen.put("" + ((int) p.x - 1) + "," + ((int) p.y - 1), true);
			}
			if (!nodesSeen.containsKey("" + (int) p.x + "," + ((int) p.y - 1))
					&& (int) p.x > (int) (width * 1f / 3f)
					&& (int) p.x < (canvas.width - 40)
					&& (int) p.y - 1 > 200
					&& (int) p.y - 1 < (canvas.height - 40)
					&& canvas.pixels[((int) p.y - 1) * canvas.width + (int) p.x] == color(255,
							255, 255))
			{
				nodes.add(new Point((int) p.x, (int) p.y - 1));
				nodesSeen.put("" + (int) p.x + "," + ((int) p.y - 1), true);
			}
			if (!nodesSeen.containsKey("" + ((int) p.x + 1) + "," + ((int) p.y - 1))
					&& (int) p.x + 1 > (int) (width * 1f / 3f)
					&& (int) p.x + 1 < (canvas.width - 40)
					&& (int) p.y - 1 > 200
					&& (int) p.y - 1 < (canvas.height - 40)
					&& canvas.pixels[((int) p.y - 1) * canvas.width + ((int) p.x + 1)] == color(
							255, 255, 255))
			{
				nodes.add(new Point((int) p.x + 1, (int) p.y - 1));
				nodesSeen.put("" + ((int) p.x + 1) + "," + ((int) p.y - 1), true);
			}
			if (!nodesSeen.containsKey("" + ((int) p.x - 1) + "," + (int) p.y)
					&& (int) p.x - 1 > (int) (width * 1f / 3f)
					&& (int) p.x - 1 < (canvas.width - 40)
					&& (int) p.y > 200
					&& (int) p.y < (canvas.height - 40)
					&& canvas.pixels[(int) p.y * canvas.width + ((int) p.x - 1)] == color(255,
							255, 255))
			{
				nodes.add(new Point((int) p.x - 1, (int) p.y));
				nodesSeen.put("" + ((int) p.x - 1) + "," + (int) p.y, true);
			}
			if (!nodesSeen.containsKey("" + ((int) p.x + 1) + "," + (int) p.y)
					&& (int) p.x + 1 > (int) (width * 1f / 3f)
					&& (int) p.x + 1 < (canvas.width - 40)
					&& (int) p.y > 200
					&& (int) p.y < (canvas.height - 40)
					&& canvas.pixels[(int) p.y * canvas.width + ((int) p.x + 1)] == color(255,
							255, 255))
			{
				nodes.add(new Point((int) p.x + 1, (int) p.y));
				nodesSeen.put("" + ((int) p.x + 1) + "," + (int) p.y, true);
			}
			if (!nodesSeen.containsKey("" + ((int) p.x - 1) + "," + ((int) p.y + 1))
					&& (int) p.x - 1 > (int) (width * 1f / 3f)
					&& (int) p.x - 1 < (canvas.width - 40)
					&& (int) p.y + 1 > 200
					&& (int) p.y + 1 < (canvas.height - 40)
					&& canvas.pixels[((int) p.y + 1) * canvas.width + ((int) p.x - 1)] == color(
							255, 255, 255))
			{
				nodes.add(new Point((int) p.x - 1, (int) p.y + 1));
				nodesSeen.put("" + ((int) p.x - 1) + "," + ((int) p.y + 1), true);
			}
			if (!nodesSeen.containsKey("" + (int) p.x + "," + ((int) p.y + 1))
					&& (int) p.x > (int) (width * 1f / 3f)
					&& (int) p.x < (canvas.width - 40)
					&& (int) p.y + 1 > 200
					&& (int) p.y + 1 < (canvas.height - 40)
					&& canvas.pixels[((int) p.y + 1) * canvas.width + (int) p.x] == color(255,
							255, 255))
			{
				nodes.add(new Point((int) p.x, (int) p.y + 1));
				nodesSeen.put("" + (int) p.x + "," + ((int) p.y + 1), true);
			}
			if (!nodesSeen.containsKey("" + ((int) p.x + 1) + "," + ((int) p.y + 1))
					&& (int) p.x + 1 > (int) (width * 1f / 3f)
					&& (int) p.x + 1 < (canvas.width - 40)
					&& (int) p.y + 1 > 200
					&& (int) p.y + 1 < (canvas.height - 40)
					&& canvas.pixels[((int) p.y + 1) * canvas.width + ((int) p.x + 1)] == color(
							255, 255, 255))
			{
				nodes.add(new Point((int) p.x + 1, (int) p.y + 1));
				nodesSeen.put("" + ((int) p.x + 1) + "," + ((int) p.y + 1), true);
			}

//			loopCount++;
		}
		
//		System.out.println("Looped " + loopCount + " times");
	}
	
	public void eightFloodUnfill(int x, int y, int fillColor, int boundaryColor, PGraphics canvas)
	{
		if(canvas.pixels == null)
		{
			System.out.println("ERROR! Pixels NULL!");
			return;
		}
		
		int pixel = canvas.pixels[y * canvas.width + x];
		if(pixel == fillColor || pixel == boundaryColor || pixel == color(255, 255, 255))
		{
//			System.out.println("returned");
			return;
		}
		
		ArrayList<Point> nodes = new ArrayList<Point>();
		HashMap<String, Boolean> nodesSeen = new HashMap<String, Boolean>();
		
		nodes.add(new Point(x, y));
		nodesSeen.put("" + x + "," + y, true);
		
//		int loopCount = 0;
		while (!nodes.isEmpty())
		{
			Point p = nodes.remove(0);
			if (canvas.pixels[(int) p.y * canvas.width + (int) p.x] != color(
					255, 255, 255))
			{
				canvas.pixels[(int) p.y * canvas.width + (int) p.x] = fillColor;
				pixelsTouched++;
			}

			if (!nodesSeen.containsKey("" + ((int) p.x - 1) + "," + ((int) p.y - 1))
					&& (int) p.x - 1 > (int) (width * 1f / 3f)
					&& (int) p.x - 1 < (canvas.width - 40)
					&& (int) p.y - 1 > 200
					&& (int) p.y - 1 < (canvas.height - 40)
					&& canvas.pixels[((int) p.y - 1) * canvas.width + ((int) p.x - 1)] != color(
							0,0,0))
			{
				nodes.add(new Point((int) p.x - 1, (int) p.y - 1));
				nodesSeen.put("" + ((int) p.x - 1) + "," + ((int) p.y - 1), true);
			}
			if (!nodesSeen.containsKey("" + (int) p.x + "," + ((int) p.y - 1))
					&& (int) p.x > (int) (width * 1f / 3f)
					&& (int) p.x < (canvas.width - 40)
					&& (int) p.y - 1 > 200
					&& (int) p.y - 1 < (canvas.height - 40)
					&& canvas.pixels[((int) p.y - 1) * canvas.width + (int) p.x] != color(0,0,0))
			{
				nodes.add(new Point((int) p.x, (int) p.y - 1));
				nodesSeen.put("" + (int) p.x + "," + ((int) p.y - 1), true);
			}
			if (!nodesSeen.containsKey("" + ((int) p.x + 1) + "," + ((int) p.y - 1))
					&& (int) p.x + 1 > (int) (width * 1f / 3f)
					&& (int) p.x + 1 < (canvas.width - 40)
					&& (int) p.y - 1 > 200
					&& (int) p.y - 1 < (canvas.height - 40)
					&& canvas.pixels[((int) p.y - 1) * canvas.width + ((int) p.x + 1)] != color(
							0,0,0))
			{
				nodes.add(new Point((int) p.x + 1, (int) p.y - 1));
				nodesSeen.put("" + ((int) p.x + 1) + "," + ((int) p.y - 1), true);
			}
			if (!nodesSeen.containsKey("" + ((int) p.x - 1) + "," + (int) p.y)
					&& (int) p.x - 1 > (int) (width * 1f / 3f)
					&& (int) p.x - 1 < (canvas.width - 40)
					&& (int) p.y > 200
					&& (int) p.y < (canvas.height - 40)
					&& canvas.pixels[(int) p.y * canvas.width + ((int) p.x - 1)] != color(0,0,0))
			{
				nodes.add(new Point((int) p.x - 1, (int) p.y));
				nodesSeen.put("" + ((int) p.x - 1) + "," + (int) p.y, true);
			}
			if (!nodesSeen.containsKey("" + ((int) p.x + 1) + "," + (int) p.y)
					&& (int) p.x + 1 > (int) (width * 1f / 3f)
					&& (int) p.x + 1 < (canvas.width - 40)
					&& (int) p.y > 200
					&& (int) p.y < (canvas.height - 40)
					&& canvas.pixels[(int) p.y * canvas.width + ((int) p.x + 1)] != color(0,0,0))
			{
				nodes.add(new Point((int) p.x + 1, (int) p.y));
				nodesSeen.put("" + ((int) p.x + 1) + "," + (int) p.y, true);
			}
			if (!nodesSeen.containsKey("" + ((int) p.x - 1) + "," + ((int) p.y + 1))
					&& (int) p.x - 1 > (int) (width * 1f / 3f)
					&& (int) p.x - 1 < (canvas.width - 40)
					&& (int) p.y + 1 > 200
					&& (int) p.y + 1 < (canvas.height - 40)
					&& canvas.pixels[((int) p.y + 1) * canvas.width + ((int) p.x - 1)] != color(
							0,0,0))
			{
				nodes.add(new Point((int) p.x - 1, (int) p.y + 1));
				nodesSeen.put("" + ((int) p.x - 1) + "," + ((int) p.y + 1), true);
			}
			if (!nodesSeen.containsKey("" + (int) p.x + "," + ((int) p.y + 1))
					&& (int) p.x > (int) (width * 1f / 3f)
					&& (int) p.x < (canvas.width - 40)
					&& (int) p.y + 1 > 200
					&& (int) p.y + 1 < (canvas.height - 40)
					&& canvas.pixels[((int) p.y + 1) * canvas.width + (int) p.x] != color(0,0,0))
			{
				nodes.add(new Point((int) p.x, (int) p.y + 1));
				nodesSeen.put("" + (int) p.x + "," + ((int) p.y + 1), true);
			}
			if (!nodesSeen.containsKey("" + ((int) p.x + 1) + "," + ((int) p.y + 1))
					&& (int) p.x + 1 > (int) (width * 1f / 3f)
					&& (int) p.x + 1 < (canvas.width - 40)
					&& (int) p.y + 1 > 200
					&& (int) p.y + 1 < (canvas.height - 40)
					&& canvas.pixels[((int) p.y + 1) * canvas.width + ((int) p.x + 1)] != color(
							0,0,0))
			{
				nodes.add(new Point((int) p.x + 1, (int) p.y + 1));
				nodesSeen.put("" + ((int) p.x + 1) + "," + ((int) p.y + 1), true);
			}

//			loopCount++;
		}
		
//		System.out.println("Looped " + loopCount + " times");
	}
	
	public void drawColorPoints()
	{
		if(!colorPoints.isEmpty())
		{
			int n = colorPoints.size();
			
			line(userColorPoint.x, userColorPoint.y, 
					colorPoints.get(0).x, colorPoints.get(0).y);
			line(colorPoints.get(colorPoints.size() - 1).x, colorPoints.get(colorPoints.size() - 1).y,
					wheel.getCenter().x, wheel.getCenter().y);
			for(int index = 0; index < n - 1; index++)
			{
				line(colorPoints.get(index).x, colorPoints.get(index).y, 
						colorPoints.get(index + 1).x, colorPoints.get(index + 1).y);
				drawPoint(colorPoints.get(index), 50, wheel.pointToColor(colorPoints.get(index)));
			}
			drawPoint(colorPoints.get(colorPoints.size() - 1), 50, wheel.pointToColor(colorPoints.get(colorPoints.size() - 1)));
		}
		
		drawPoint(userColorPoint, 50, wheel.pointToColor(userColorPoint));
	}
	
	public void drawCanvas(PGraphics canvas, int strokeColor)
	{
		canvas.beginDraw();
		int oldStroke = canvas.strokeColor;
		float oldStrokeWeight = canvas.strokeWeight;
		canvas.stroke(strokeColor);
		canvas.strokeWeight(5);
		
		for(ArrayList<Point> stroke : sketchPoints)
		{
			for(int index = 0; index < stroke.size() - 1; index++)
			{
				canvas.line(stroke.get(index).x, stroke.get(index).y, stroke.get(index + 1).x, stroke.get(index + 1).y);
			}
		}
		
		canvas.strokeWeight(oldStrokeWeight);
		canvas.stroke(oldStroke);
		canvas.endDraw();
	}
	
	public void colorPixels(PGraphics image, int color)
	{
		image.beginDraw();
		image.loadPixels();
		for(int index = 0, dimension = image.width * image. height; index < dimension; index++)
		{
			image.pixels[index] = color;
		}
		image.updatePixels();
		image.endDraw();
	}
	
	public void resetPixels()
	{
		colorPixels(canvas, color(255,255,255));
	}
	
	public void drawPoint(Point p, float radius, int fillColor, int strokeColor)
	{
		int oldStroke = g.strokeColor;
		int oldFill = g.fillColor;
		stroke(strokeColor);
		fill(fillColor);
		ellipse(p.x, p.y, radius, radius);
		fill(oldFill);
		stroke(oldStroke);
	}
	
	public void drawPoint(Point p, float radius, int fillColor)
	{
		drawPoint(p, radius, fillColor, color(0,0,0));
	}
	
	public void drawPoint(Point p, float radius)
	{
		drawPoint(p, radius, color(0,0,0));
	}
	
	public void drawPoint(Point p, int color)
	{
		drawPoint(p, 5, color);
	}
	
	public void drawPoint(Point p)
	{
		drawPoint(p, 5, color(0,0,0));
	}
	
	public float distance(float x1, float y1, float x2, float y2)
	{
		return (float) Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
	}
	
	public float distance2(float x1, float y1, float x2, float y2)
	{
		return (float) ((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
	}
	
	public void reinitColors()
	{
		colors.clear();
		backupColors.clear();
		colors.add(wheel.pointToColor(userColorPoint));
		for(Point p : colorPoints)
		{
			colors.add(wheel.pointToColor(p));
		}
	}
	
	public void keyPressed()
	{
		if(key == '~')
		{
			filming = !filming;
		}
		if(key == 'l' || key == 'L')
		{
			lightnessChange = !lightnessChange;
			wheel.computeColorPoints(colorPoints, userColorPoint, numColorPoints, maxColorPoints, lightnessChange, chromaChange, hueChange);
		}
		if(key == 'c' || key == 'C')
		{
			chromaChange = !chromaChange;
			wheel.computeColorPoints(colorPoints, userColorPoint, numColorPoints, maxColorPoints, lightnessChange, chromaChange, hueChange);
		}
		if(key == 'h' || key == 'H')
		{
			hueChange = !hueChange;
			wheel.computeColorPoints(colorPoints, userColorPoint, numColorPoints, maxColorPoints, lightnessChange, chromaChange, hueChange);
		}
		if(key == 'e' || key == 'E')
		{
			sketchPoints.clear();
			sketchPoints.add(new ArrayList<Point>());
			sketchStrokeIndex = 0;
			resetPixels();
		}
		if(key == 'r' || key == 'R')
		{
			resetPixels();
		}
		if(key == 's' || key == 'S')
		{
			shuffleColor = !shuffleColor;
		}
		if(key == 'p' || key =='P')
		{
			paintAll = !paintAll;
			System.out.println("PaintAll: " + paintAll);
			if(!paintAll)
			{
				reinitColors();
				System.out.println("Colors: " + colors.size());
			}
		}
		if(key == 'g' || key == 'G')
		{
			sketchPoints.clear();
			sketchPoints.add(new ArrayList<Point>());
			sketchStrokeIndex = 0;
			resetPixels();
			
			//1		2		3		4		5		6		7		8		9		10		11		12		13		14		15		16
			//1x1	2x1		3x1/2x2	2x2		3x2		3x2		4x2/3x3	4x2/3x3	3x3		4x3		4x3		4x3		5x3/4x4	4x4		4x4		4x4
			int rows = 0, columns = 0;
			int priorRoot = 1, postRoot = 2;
			
			for(int i = 1; i <= numColorPoints; i++)
			{
				if(Math.sqrt(i) == Math.floor(Math.sqrt(i)))
				{
					priorRoot = (int)Math.round(Math.sqrt(i));
					postRoot = priorRoot + 1;
				}
				if(rows * columns < i)
				{
					rows++;
					columns = priorRoot;
				}
				if(rows > postRoot)
				{
					rows = postRoot;
					columns = postRoot;
				}
			}
			
			//Boundaries are: (width * 1f / 3f) < x < (width - 40) && 200 < x < (height - 40)
			float totalWidth = (int)(width * 2f / 3f - 40), totalHeight = (height - 240);
			float columnWidth = totalWidth / rows;
			float rowHeight = totalHeight / columns;
			float xMin = (width * 1f / 3f), yMin = 200, xMax = (width - 40), yMax = (height - 40);
			
			sketchPoints.get(sketchStrokeIndex).add(new Point(xMin, yMin));
			sketchPoints.get(sketchStrokeIndex).add(new Point(xMax, yMin));
			sketchPoints.get(sketchStrokeIndex).add(new Point(xMax, yMax));
			sketchPoints.get(sketchStrokeIndex).add(new Point(xMin, yMax));
			sketchPoints.get(sketchStrokeIndex).add(new Point(xMin, yMin));
			sketchPoints.add(new ArrayList<Point>());
			sketchStrokeIndex++;
			float yNew = yMin + rowHeight;
			for(int i = 0; i < rows; i++)
			{
				
				sketchPoints.get(sketchStrokeIndex).add(new Point(xMin, yNew));
				sketchPoints.get(sketchStrokeIndex).add(new Point(xMax, yNew));
				yNew += rowHeight;
				sketchPoints.add(new ArrayList<Point>());
				sketchStrokeIndex++;
			}
			float xNew = xMin + columnWidth;
			for(int i = 0; i < columns; i++)
			{
				sketchPoints.get(sketchStrokeIndex).add(new Point(xNew, yMin));
				sketchPoints.get(sketchStrokeIndex).add(new Point(xNew, yMax));
				xNew += columnWidth;
				sketchPoints.add(new ArrayList<Point>());
				sketchStrokeIndex++;
			}
			
			sketchPoints.add(new ArrayList<Point>());
			sketchStrokeIndex = sketchPoints.size() - 1;
		}
	}
	
	public void mouseReleased()
	{
		if(!sketchPoints.get(sketchStrokeIndex).isEmpty())
		{
			sketchPoints.add(new ArrayList<Point>());
			sketchStrokeIndex = sketchPoints.size() - 1;
		}
	}
	
	public void mouseDragged()
	{
		if(distance(mouseX, mouseY, Math.round(this.wheel.getCenter().x), Math.round(this.wheel.getCenter().y)) <= Math.round(this.wheel.getWidth() / 2f))
		{
			userColorPoint = new Point(mouseX, mouseY, wheel.getLightness());
			
			wheel.computeColorPoints(colorPoints, userColorPoint, numColorPoints, maxColorPoints, lightnessChange, chromaChange, hueChange);

			if(!paintAll)
			{
				reinitColors();
			}
		}
		
		if(mouseX > (int)(width * 1f / 3f) && mouseY > 200 && mouseX < (width - 40) && mouseY < (height - 40))
		{
			if(!sketchPoints.get(sketchStrokeIndex).isEmpty() && !sketchPoints.get(sketchStrokeIndex).get(sketchPoints.get(sketchStrokeIndex).size() - 1).equalsEpsilon2D(mouseX, mouseY))
			{
				sketchPoints.get(sketchStrokeIndex).add(new Point(mouseX, mouseY));
			}
			else if(sketchPoints.get(sketchStrokeIndex).isEmpty())
			{
				sketchPoints.get(sketchStrokeIndex).add(new Point(mouseX, mouseY));
			}
		}
	}
	
	public void mouseClicked()
	{
		if(distance(mouseX, mouseY, Math.round(this.wheel.getCenter().x), Math.round(this.wheel.getCenter().y)) <= Math.round(this.wheel.getWidth() / 2f))
		{
			userColorPoint = new Point(mouseX, mouseY, wheel.getLightness());
			
			wheel.computeColorPoints(colorPoints, userColorPoint, numColorPoints, maxColorPoints, lightnessChange, chromaChange, hueChange);

			if(!paintAll)
			{
				reinitColors();
			}
		}
		
		if(mouseX > (int)(width * 1f / 3f) && mouseY > 200 && mouseX < (width - 40) && mouseY < (height - 40))
		{
			if(!paintAll)
			{
				if(mouseButton == LEFT)
				{
					paintFill(mouseX, mouseY);
				}
				else if(mouseButton == RIGHT)
				{
					paintUnfill(mouseX, mouseY);
				}
			}
		}
	}
}
