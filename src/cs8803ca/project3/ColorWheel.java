package cs8803ca.project3;

import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PImage;

/**
 * Class that displays a color wheel of a certain Lightness value.
 * The wheel displays Chroma and Hue angle values for that lightness value in the LCH color space.
 * L has range 0 - 100. Height of LCH cylinder.
 * C has range 0 - 100. Radius of LCH cylinder.
 * H has range 0 - 360 or 0 since it is an angle. Radial angle of LCH cylinder. 
 * @author mikhail.jacob
 *
 */
public class ColorWheel
{
	PApplet parent;
	int x, y, w, h;
	float lightness; //Lightness has range 0 - 100.
	PImage wheel;
	ColorSpaces colorSpace;
	
	public ColorWheel(PApplet parent)
	{
		this.parent = parent;
		
		int totalX = (int)(parent.width / 3f);
		int totalY = parent.height;
		int xMin = totalX / 12;
		int yMin = totalY * 3 / 9;
		int width = Math.round(totalX * 5f / 6f);
		int height = width;
		
		this.x = xMin;
		this.y = yMin;
		this.w = width;
		this.h = height;
		lightness = 50;
		
		wheel = new PImage(width, height);
		wheel.loadPixels();
		
		colorSpace = new ColorSpaces(parent);
		
		createColorWheel(wheel, lightness);
	}
	
	public ColorWheel(PApplet parent, int x, int y, int w, int h, float lightness)
	{
		this.parent = parent;
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.lightness = lightness;
		
		wheel = new PImage(w, h);
		wheel.loadPixels();
		
		colorSpace = new ColorSpaces(parent);
		
		createColorWheel(wheel, lightness);
	}
	
	public ColorWheel(PApplet parent, float lightness)
	{
		this.parent = parent;
		
		int totalX = (int)(parent.width / 3f);
		int totalY = parent.height;
		int xMin = totalX / 12;
		int yMin = totalY * 3 / 9;
		int width = Math.round(totalX * 5f / 6f);
		int height = width;
		
		this.x = xMin;
		this.y = yMin;
		this.w = width;
		this.h = height;
		
		this.lightness = lightness;
		
		wheel = new PImage(width, height);
		wheel.loadPixels();
		
		colorSpace = new ColorSpaces(parent);
		
		createColorWheel(wheel, lightness);
	}
	
	public void draw()
	{
		parent.image(wheel, x, y);
		drawPoint(getCenter());
	}
	
	public void drawPoint(Point p, float radius, int fillColor, int strokeColor)
	{
		int oldStroke = parent.g.strokeColor;
		int oldFill = parent.g.fillColor;
		parent.stroke(strokeColor);
		parent.fill(fillColor);
		parent.ellipse(p.x, p.y, radius, radius);
		parent.fill(oldFill);
		parent.stroke(oldStroke);
	}
	
	public void drawPoint(Point p)
	{
		drawPoint(p, 5, parent.color(0,0,0), parent.color(0,0,0));
	}
	
	public void createColorWheel(PImage wheel, double lightness)
	{
		wheel.loadPixels();
		int cx = Math.round(this.w / 2f), cy = Math.round(this.h / 2f);
		float distance = 0;
		
		for(int index = 0, dimension = this.w * this.h, x = 0, y = 0;
				index < dimension;
				index++, x++)
		{
			if(x >= this.w)
			{
				x = 0;
				y++;
			}
			if(y >= this.h)
			{
				y = 0;
			}
			
			distance = distance(x, y, cx, cy);
			
			wheel.pixels[y * this.w + x] = colorWheelPointToColor(x, y, cx, cy, lightness, distance);
		}
		
		wheel.updatePixels();
	}
	
	public int colorWheelPointToColor(int x, int y, int cx, int cy, double lightness, float distance)
	{
		if(distance > Math.round(this.w / 2f) || distance > Math.round(this.h / 2f))
		{
			return parent.color(255,255,255);
		}
		else
		{
			double L = lightness, C = 0.0, H = 0.0;
			
			//scale d (range: 0 - this.w OR this.h) to C (range: 0 - 100)
			C = scaleRange(distance, 0, Math.round(this.w / 2f), 0, 100);
			
			H = PApplet.degrees((float)(Math.atan2((y - cy), (x - cx))));
			
			return colorSpace.LCHtoColor(L, C, H);
		}
	}
	
	public int pointToColor(Point p)
	{
		int cx = this.x + Math.round(this.w / 2), cy = this.y + Math.round(this.h / 2);
		int x = Math.round(p.x), y = Math.round(p.y);
		float distance = Math.round(distance(x, y, cx, cy));
		
		return colorWheelPointToColor(x, y, cx, cy, p.z, distance);
	}
	
	public ArrayList<Point> computeColorPoints(ArrayList<Point> colorPoints, Point userColorPoint, int numColorPoints, int maxColorPoints, Boolean lightnessChange, Boolean chromaChange, Boolean hueChange)
	{
		colorPoints.clear();
		int n = (numColorPoints < maxColorPoints) ? numColorPoints : maxColorPoints;
		float lWidth = Math.round(100f / numColorPoints);
		float lIncrement = 0;
		double angleIncrement = 2 * Math.PI / n;
		Point newPoint = new Point(userColorPoint), center = getCenter();
		double distance = distance(newPoint.x, newPoint.y, center.x, center.y);
		double theta = (Math.atan2((newPoint.y - center.y), (newPoint.x - center.x)));
		double distanceDecrement = distance / numColorPoints;
		double thetaIncrement = 0.0;
		
		for(int index = 1; index < numColorPoints; index++)
		{
			if(hueChange)
			{
				thetaIncrement += angleIncrement;
			}
			if(chromaChange)
			{
				distance -= distanceDecrement; 
			}
			if(lightnessChange)
			{
//				lIncrement = (newPoint.z + lIncrement + lWidth > 100) ? lIncrement + lWidth - 100 : lIncrement + lWidth;
				lIncrement += (index * lWidth);
				lWidth *= -1;
				if(newPoint.z + lIncrement < 0)
				{
					lIncrement += 100;
				}
				else if(newPoint.z + lIncrement > 100)
				{
					lIncrement -= 100;
				}
			}
			float x = center.x + (float) (distance * Math.cos(theta + thetaIncrement));
			float y = center.y + (float) (distance * Math.sin(theta + thetaIncrement));
			float z = newPoint.z + lIncrement;
			colorPoints.add(new Point(x, y, z));
		}
		
		return colorPoints;
	}
	
	public ArrayList<Point> computeColorPoints(ArrayList<Point> colorPoints, Point userColorPoint, int numColorPoints, int maxColorPoints)
	{
		return computeColorPoints(colorPoints, userColorPoint, numColorPoints, maxColorPoints, true, true, true);
	}
	
	public double scaleRange(double oldValue, double oldMin, double oldMax, double newMin, double newMax)
	{
		return (((oldValue - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
	}
	
	public float distance(float x1, float y1, float x2, float y2)
	{
		return (float) Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
	}
	
	public float distance(int x1, int y1, int x2, int y2)
	{
		return (float) Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
	}
	
	public float distance2(int x1, int y1, int x2, int y2)
	{
		return (float) ((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
	}
	
	public void setLightness(float l)
	{
		if(l > 100 || l < 0)
		{
			l = 50;
			System.out.println("ERROR! L should be in range 0 - 100.");
		}
		
		this.lightness = l;
		
		createColorWheel(wheel, l);
	}
	
	public float getLightness()
	{
		return this.lightness;
	}
	
	public int getWidth()
	{
		return this.w;
	}
	
	public int getHeight()
	{
		return this.h;
	}
	
	public Point getCenter()
	{
		return new Point(x + Math.round(w / 2), y + Math.round(h / 2), this.lightness);
	}
	
	public Point getCenterInternalOrigin()
	{
		return new Point(Math.round(w / 2), Math.round(h / 2));
	}
}
