package cs8803ca.project5.music.generation;

public class LoopNote
{
	private static final float EPSILON = 0.001f;
	//Starts from 0 - 2 * PI
	private float startAngle;
	//Starts from 0 - 2 * PI
	private float endAngle;
	//Starts from 1 to number of levels;
	private int level;
	
	private float startTime;
	
	private float endTime;
	
	private int startNoteInterval;
	
	private int endNoteInterval;
	
	private float timePerBeat;
	
	private float timePerMeasure;
	
	private int notesPerMeasure;
	
	public LoopNote()
	{
		
	}
	
	public LoopNote(float startAngle, float endAngle, int level, float timePerBeat, float timePerMeasure, int notesPerMeasure)
	{
		this.startAngle = startAngle;
		this.endAngle = endAngle;
		this.level = level;
		this.timePerBeat = timePerBeat;
		this.timePerMeasure = timePerMeasure;
		this.notesPerMeasure = notesPerMeasure;
	}
	
	public LoopNote normalize()
	{
		while(startAngle > 2 * Math.PI)
		{
			startAngle = startAngle - (float)(2 * Math.PI);
			endAngle = endAngle - (float)(2 * Math.PI);
		}
		
		if(endAngle - startAngle > 2 * Math.PI)
		{
			startAngle = 0;
			endAngle = (float)(2 * Math.PI);
		}
		
		for(int i = 0; i <= notesPerMeasure; i++)
		{
//			System.out.println("Delta: " + (float)(2 * Math.PI * i / notesPerMeasure));
			if(epsilonEquals(startAngle, (float)(2 * Math.PI * i / notesPerMeasure)))
			{
				startAngle = (float)(2 * Math.PI * i / notesPerMeasure);
//				System.out.println("Start: " + startAngle);
			}
			
			if(epsilonEquals(endAngle, (float)(2 * Math.PI * i / notesPerMeasure)))
			{
				endAngle = (float)(2 * Math.PI * i / notesPerMeasure);
//				System.out.println("End: " + endAngle);
			}
		}
		
		if(endAngle > 2 * Math.PI)
		{
			float overflowEndAngle = endAngle - (float)(2 * Math.PI);
			LoopNote overflowNote = new LoopNote(0, overflowEndAngle, level, timePerBeat, timePerMeasure, notesPerMeasure);
			
			endAngle = (float)(2 * Math.PI);
			computeTimes();
			if(overflowEndAngle > 0)
			{
				overflowNote.computeTimes();
				return overflowNote;
			}
		}
		
		computeTimes();
		return null;
	}
	
	public void computeTimes()
	{
		float angle = 0f;
		float angularSeparation = (float)(2 * Math.PI / notesPerMeasure);
		boolean startSet = false, endSet = false;
		for(int i = 0; i <= notesPerMeasure; i++)
		{
			if(epsilonEquals(angle, startAngle))
			{
				startTime = i * timePerBeat;
				startNoteInterval = i;
				startSet = true;
			}
			
			if(epsilonEquals(angle, endAngle))
			{
				endTime = i * timePerBeat;
				endNoteInterval = i;
				endSet = true;
			}
			
			if(startSet && endSet)
			{
				break;
			}
			
			angle += angularSeparation;
		}
	}
	
	public void adjustTiming(float timePerBeat, float timePerMeasure, int notesPerMeasure)
	{
		startTime = startNoteInterval * timePerBeat;
		endTime = endNoteInterval * timePerBeat;
		startAngle = (float)(2 * Math.PI * startNoteInterval / notesPerMeasure);
		endAngle = (float)(2 * Math.PI * endNoteInterval / notesPerMeasure);
		
		this.timePerBeat = timePerBeat;
		this.timePerMeasure = timePerMeasure;
		this.notesPerMeasure = notesPerMeasure;
	}
	
	public static boolean epsilonEquals(float a, float b)
	{
		return (Math.abs(a - b) < EPSILON);
	}
	
	public float getStartAngle()
	{
		return startAngle;
	}
	public void setStartAngle(float startAngle)
	{
		this.startAngle = startAngle;
	}
	public float getEndAngle()
	{
		return endAngle;
	}
	public void setEndAngle(float endAngle)
	{
		this.endAngle = endAngle;
	}
	public int getLevel()
	{
		return level;
	}
	public void setLevel(int level)
	{
		this.level = level;
	}
	
	public float getStartTime()
	{
		return startTime;
	}

	public void setStartTime(float startTime)
	{
		this.startTime = startTime;
	}

	public float getEndTime()
	{
		return endTime;
	}

	public void setEndTime(float endTime)
	{
		this.endTime = endTime;
	}

	public int getStartNoteInterval()
	{
		return startNoteInterval;
	}

	public void setStartNoteInterval(int startNoteInterval)
	{
		this.startNoteInterval = startNoteInterval;
	}

	public int getEndNoteInterval()
	{
		return endNoteInterval;
	}

	public void setEndNoteInterval(int endNoteInterval)
	{
		this.endNoteInterval = endNoteInterval;
	}

	public float getTimePerBeat()
	{
		return timePerBeat;
	}

	public void setTimePerBeat(float timePerBeat)
	{
		this.timePerBeat = timePerBeat;
	}

	public float getTimePerMeasure()
	{
		return timePerMeasure;
	}

	public void setTimePerMeasure(float timePerMeasure)
	{
		this.timePerMeasure = timePerMeasure;
	}

	public int getNotesPerMeasure()
	{
		return notesPerMeasure;
	}

	public void setNotesPerMeasure(int notesPerMeasure)
	{
		this.notesPerMeasure = notesPerMeasure;
	}

	public String toString()
	{
		String result = "";
		result += "Start Angle: " + startAngle + "\t";
		result += "End Angle: " + endAngle + "\t";
		result += "Start Time: " + startTime + "\t";
		result += "End Time: " + endTime + "\t";
		result += "Start Interval: " + startNoteInterval + "\t";
		result += "End Interval: " + endNoteInterval + "\t";
		result += "Level: " + level + "\t";
		result += "Time Per Beat: " + timePerBeat + "\t";
		result += "Time Per Measure: " + timePerMeasure + "\t";
		result += "Notes Per Measure: " + notesPerMeasure;
		
		return result;
	}
}
