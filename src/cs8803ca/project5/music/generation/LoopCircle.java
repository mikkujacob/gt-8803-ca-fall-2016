package cs8803ca.project5.music.generation;

import java.util.ArrayList;
import java.util.Random;

import cs8803ca.project5.music.generation.MusicPlayer.Scales;
import cs8803ca.project5.shared.ColorSpaces;

import processing.core.PApplet;

public class LoopCircle
{
	private boolean PLAY_MODE = true;
	private boolean isPlaying;
	private boolean isRecording;
	private boolean isRecordingNote;
	private PApplet parent;
	private int x;
	private int y;
	private float boundingWidth;
	private final float widthScaleFactor = 0.5f;
	private float boundingHeight;
	private final float heightScaleFactor = 0.5f;
	private float radius;
	private final float radiusScaleFactor = 0.075f;
	private float noteThickness;
	private final float thicknessScaleFactor = 0.025f;
	private float playheadAngle;
	private int numberOfLevels;
	private ArrayList<LoopNote> notes;
	private ArrayList<Integer> colors;
	private ColorSpaces colorSpaces;
	private float rotationOffset;
	private int tempo;
	private int timeSignatureNumerator;
	private int timeSignatureDenominator;
	private float timePerBeat;
	private float timePerMeasure;
	private long startPlayTime;
	private long startRecordTime;
	private LoopNote currentNote;
	private MusicPlayer player;
	private String instrument;
	private Scales currentScale;
	private String chromaticRoot;
	private int octave;
	private String chordSignature;
	private int playNoteInterval;
	
	
	public LoopCircle(PApplet parent)
	{
		this(0, 0, 5, parent);
	}
	
	public LoopCircle(int x, int y, int numberOfLevels, PApplet parent)
	{
		this(x, y, numberOfLevels, 120, 8, 8, parent);
	}
	
	public LoopCircle(int x, int y, int numberOfLevels, int tempo, int timeSignatureNumerator, int timeSignatureDenominator, PApplet parent)
	{
		this(x, y, numberOfLevels, 120, 8, 8, "Piano", Scales.MAJOR, "C", 5, "", parent);
	}
	//player.setParameters(tempo, timePerMeasure, timePerBeat, timeSignatureNumerator, timeSignatureDenominator, instrument, currentScale, chromaticRoot, octave, chordSignature);
	public LoopCircle(int x, int y, int numberOfLevels, int tempo, int timeSignatureNumerator, int timeSignatureDenominator, String instrument, Scales currentScale, String chromaticRoot, int octave, String chordSignature, PApplet parent)
	{
		this.parent = parent;
		
		this.radius = parent.width * this.radiusScaleFactor;
		this.boundingWidth = parent.width * this.widthScaleFactor;
		this.boundingHeight = parent.height * this.heightScaleFactor;
		this.noteThickness = parent.width * this.thicknessScaleFactor;
		
		this.x = x;
		this.y = y;
		
		this.numberOfLevels = numberOfLevels;
		this.playheadAngle = 0f;
		this.notes = new ArrayList<LoopNote>();
		this.rotationOffset = (float)(-1 * Math.PI / 2f);//OFFSET TO HAVE 0 RADIANS AT TOP VERTICAL
		
		this.tempo = tempo;
		this.timeSignatureNumerator = timeSignatureNumerator;
		this.timeSignatureDenominator = timeSignatureDenominator;
		this.playNoteInterval = 0;
		
		this.instrument = instrument;
		this.currentScale = currentScale;
		this.chromaticRoot = chromaticRoot;
		this.octave = octave;
		this.chordSignature = chordSignature;
		
		this.colors = new ArrayList<Integer>();
		this.colorSpaces = new ColorSpaces(parent);
		
		int randomStartAngle = (new Random()).nextInt(360);
		for(int i = 0; i < this.numberOfLevels; i++)
		{
			double L = 50.0;
			double C = 100.0;
			double H = randomStartAngle + i * 360 / (double)numberOfLevels;
			this.colors.add(colorSpaces.LCHtoColor(L, C, H));
		}
		
		//60 sec / min, TEMPO beats / min, TSD beats / note, TSN beats / measure
		//TimePerBeat = 60 / TEMPO sec / beat
		timePerBeat = 60f / tempo;
		//TimePerMeasure = 60 / TEMPO * TSN
		timePerMeasure = timePerBeat * timeSignatureNumerator;
		
		player = new MusicPlayer();
		player.setParameters(tempo, timePerMeasure, timePerBeat, timeSignatureNumerator, timeSignatureDenominator, instrument, currentScale, chromaticRoot, octave, chordSignature);
		
	}
	
	public void calculateTimes()
	{
		//60 sec / min, TEMPO beats / min, TSD beats / note, TSN beats / measure
		//TimePerBeat = 60 / TEMPO sec / beat
		timePerBeat = 60f / tempo;
		//TimePerMeasure = 60 / TEMPO * TSN
		timePerMeasure = timePerBeat * timeSignatureNumerator;
		
		player.setTimePerBeat(timePerBeat);
		player.setTimePerMeasure(timePerMeasure);
		
		updateNoteTimes();
	}
	
	public void updateNoteTimes()
	{
		for(LoopNote note : notes)
		{
			note.adjustTiming(timePerBeat, timePerMeasure, timeSignatureNumerator);
		}
		
		addNotes();
	}
	
	public void addNotes()
	{
		if(!notes.isEmpty())
		{
			player.addNotes(notes);
		}
	}
	
	public void draw()
	{
		parent.pushMatrix();
		
		int oldStroke = parent.g.strokeColor;
		int oldFill = parent.g.fillColor;
		float oldStrokeWeight = parent.g.strokeWeight;
		parent.stroke(parent.color(255, 255, 255));
		parent.strokeWeight(5);
		
		//Draw bounding box for the circle
		parent.fill(parent.color(0, 0, 0));
		parent.rect(x - boundingWidth / 2f, y - boundingHeight / 2f, boundingWidth, boundingHeight);
		
		//Draw the note intervals
		parent.strokeWeight(noteThickness * 0.05f);
		float r = (radius + (numberOfLevels + 1) * noteThickness) / 2;
		float theta = rotationOffset;
		for(int i = 0; i < timeSignatureNumerator; i++)
		{
			theta = rotationOffset + (float)(2 * Math.PI * i / timeSignatureNumerator);
			parent.line(x, y, x + (float)(r * Math.cos(theta)), y + (float)(r * Math.sin(theta)));	
		}
		
		//Draw the notes
		parent.noFill();
		parent.strokeWeight(noteThickness / 2);
		for(LoopNote note : notes)
		{
			parent.stroke(colors.get(note.getLevel() - 1));
			r = (radius + (note.getLevel()) * noteThickness) / 2;;
			float arcOverflowAngle = PApplet.atan2(noteThickness / 2f,  r);
			parent.arc(x, y, radius + note.getLevel() * noteThickness, radius + note.getLevel() * noteThickness, rotationOffset + note.getStartAngle() + arcOverflowAngle, rotationOffset + note.getEndAngle() - arcOverflowAngle);
		}
		
		//Draw outer edge of vinyl record
		parent.strokeWeight(noteThickness * 0.05f);
		parent.stroke(parent.color(255, 255, 255));
		parent.ellipse(x, y, radius + (numberOfLevels + 1) * noteThickness, radius + (numberOfLevels + 1) * noteThickness);
		
		if(PLAY_MODE && isPlaying)
		{
			//Update the time played
			updatePlayhead();
			
			//Draw the playhead sector
			parent.fill(parent.color(255, 255, 255, 200));
			parent.stroke(parent.color(255, 255, 255, 200));
			parent.arc(x, y, radius + (numberOfLevels + 1) * noteThickness, radius + (numberOfLevels + 1) * noteThickness, rotationOffset + 0, rotationOffset + playheadAngle, PApplet.PIE);
		}
		
		if(!PLAY_MODE && isRecording)
		{
			r = (radius + (numberOfLevels + 1) * noteThickness) / 2;
			parent.stroke(parent.color(255, 255, 255));
			parent.strokeWeight(noteThickness * 0.2f);
			theta = rotationOffset;
			parent.line(x, y, x + (float)((r - noteThickness * 0.2f / 2) * Math.cos(theta)), y + (float)((r - noteThickness * 0.2f / 2) * Math.sin(theta)));
			
			//Update the time recorded
			updateRecordHead();
			
			if(isRecordingNote)
			{
				recordNoteOff(parent.mouseX, parent.mouseY, parent.pmouseX, parent.pmouseY);
			}
			
			//Draw the recordhead sector
			parent.fill(parent.color(255, 255, 255, 200));
			parent.stroke(parent.color(255, 255, 255, 200));
			parent.strokeWeight(noteThickness * 0.05f);
			parent.arc(x, y, radius + (numberOfLevels + 1) * noteThickness, radius + (numberOfLevels + 1) * noteThickness, (float)(-1 * Math.PI / 2 + 2 * Math.PI / timeSignatureNumerator), (float)(-1 * Math.PI / 2 + 2 * Math.PI), PApplet.PIE);
		}
		
		//Draw the circle
		parent.fill(parent.color(255, 255, 255));
		parent.ellipse(x, y, radius, radius);
		
		//Draw the center of the vinyl record
		parent.fill(parent.color(0, 0, 0));
		parent.ellipse(x, y, radius * 0.2f, radius * 0.2f);
		
		parent.strokeWeight(oldStrokeWeight);
		parent.stroke(oldStroke);
		parent.fill(oldFill);
		parent.popMatrix();
	}
	
	public void updatePlayhead()
	{
		long currentPlayTime = System.currentTimeMillis();
		float lapsedPlayTime = (currentPlayTime - startPlayTime) / 1000f;
		
		if(lapsedPlayTime > timePerMeasure)
		{
			startPlayTime += Math.round(timePerMeasure * 1000);
			lapsedPlayTime = (currentPlayTime - startPlayTime) / 1000f;
//			System.out.println("Lapsed Time > Time Per Measure (" + Math.round(timePerMeasure * 1000) + ")");
//			System.out.println("New Start Time: " + startTime + "\n");
		}
		
//		System.out.println("Start Time: " + startTime);
//		System.out.println("Current Time: " + currentTime);
//		System.out.println("Lapsed Time: " + lapsedTime);
//		System.out.println("Time Per Measure: " + timePerMeasure);
//		System.out.println("Delta Playhead Angle: " + (lapsedTime / timePerMeasure) * 2 * Math.PI + "\n");
		
		playheadAngle = (float)((lapsedPlayTime / timePerMeasure) * 2 * Math.PI);
		
//		System.out.println("Playhead Angle: " + playheadAngle + "\n");
		
		if(playheadAngle >= (playNoteInterval * 2 * Math.PI / timeSignatureNumerator))
		{
			playNoteInterval = (playNoteInterval + 1) % timeSignatureNumerator;
		}
	}
	
	public void updateRecordHead()
	{
		long currentRecordTime = System.currentTimeMillis();
		float lapsedRecordTime = (currentRecordTime - startRecordTime) / 1000f;
		
//		System.out.println("Start Time: " + startRecordTime);
//		System.out.println("Current Time: " + currentRecordTime);
//		System.out.println("Lapsed Time: " + lapsedRecordTime);
//		System.out.println("Time Per Measure: " + timePerMeasure);
//		System.out.println("Delta Rotation Offset: " + (lapsedRecordTime / timePerMeasure) * 2 * Math.PI + "\n");
		
		rotationOffset = (float)(-1 * Math.PI / 2f - (lapsedRecordTime / timePerMeasure) * 2 * Math.PI);
//		System.out.println("Rotation Offset: " + rotationOffset + "\n");
		
		if(lapsedRecordTime > timePerMeasure)
		{
//			startRecordTime += Math.round(timePerMeasure * 1000);
//			System.out.println("Lapsed Time > Time Per Measure (" + Math.round(timePerMeasure * 1000) + ")");
//			System.out.println("New Start Time: " + startRecordTime + "\n");
		}
	}
	
	public void startPlaying()
	{
//		System.out.println("LoopCircle.startPlaying(): " + System.currentTimeMillis());
		isPlaying = true;
		calculateTimes();
		setPlayheadAngle(0f);
		setRotationOffset((float)(-1 * Math.PI / 2f));
		startPlayTime = System.currentTimeMillis();
		playNoteInterval = 0;
		player.togglePlayback();
	}
	
	public void stopPlaying()
	{
//		System.out.println("LoopCircle.stopPlaying(): " + System.currentTimeMillis());
		isPlaying = false;
		setPlayheadAngle(0f);
		startPlayTime = 0;
		setRotationOffset((float)(-1 * Math.PI / 2f));
		player.togglePlayback();
	}
	
	public void togglePlaying()
	{
//		System.out.println("LoopCircle.togglePlaying(): " + System.currentTimeMillis());
		if(PLAY_MODE)
		{
			if(!isPlaying)
			{
				startPlaying();
			}
			else
			{
				stopPlaying();
			}
		}
	}
	
	public void startRecording()
	{
		isRecording = true;
		calculateTimes();
		setPlayheadAngle(0f);
		setRotationOffset((float)(-1 * Math.PI / 2f));
		startRecordTime = System.currentTimeMillis();
	}
	
	public void stopRecording()
	{
		isRecording = false;
		setPlayheadAngle(0f);
		startRecordTime = 0;
		setRotationOffset((float)(-1 * Math.PI / 2f));
		player.setParameters(tempo, timePerMeasure, timePerBeat, timeSignatureNumerator, timeSignatureDenominator, instrument, currentScale, chromaticRoot, octave, chordSignature);
		player.addNotes(notes);
	}
	
	public void toggleRecording()
	{
		if(!PLAY_MODE)
		{
			if(!isRecording)
			{
				startRecording();
			}
			else
			{
				stopRecording();
			}
		}
	}
	
	public void mousePressed(int mouseX, int mouseY, int pmouseX, int pmouseY)
	{
		if(mouseX < (x - boundingWidth / 2f) || mouseX > (x + boundingWidth / 2f) ||
				mouseY < (y - boundingHeight / 2f) || mouseY > (y + boundingHeight / 2f) ||
				pmouseX < (x - boundingWidth / 2f) || pmouseX > (x + boundingWidth / 2f) ||
				pmouseY < (y - boundingHeight / 2f) || pmouseY > (y + boundingHeight / 2f))
		{
			return;
		}
		
		float currentDistance = (float)(Math.sqrt((mouseX - x) * (mouseX - x) + (mouseY - y) * (mouseY - y)));
		float maxDistance = (radius + (numberOfLevels) * noteThickness) / 2;
		float minDistance = radius / 2;
		
		if(currentDistance < minDistance || currentDistance > maxDistance)
		{
//			System.out.println("Out of bounds\n");
			return;
		}
		
		if(!PLAY_MODE && isRecording)
		{
			recordNoteOn(mouseX, mouseY, pmouseX, pmouseY);
			isRecordingNote = true;
		}
		
		if(!isPlaying && !isRecording)
		{
			if(parent.mouseButton == PApplet.RIGHT)
			{
				eraseNote(mouseX, mouseY, pmouseX, pmouseY);
			}
		}
	}
	
	public void eraseNote(int mouseX, int mouseY, int pmouseX, int pmouseY)
	{
		float currentAngle = PApplet.atan2(mouseY - y, mouseX - x);
//		System.out.println("CURRENT ANGLE: " + currentAngle);
		currentAngle += (float)Math.PI;
//		System.out.println("CURRENT ANGLE: " + currentAngle);
		currentAngle += (float)(3 * Math.PI / 2);
//		System.out.println("CURRENT ANGLE: " + currentAngle);
		while(currentAngle >= (float)(2 * Math.PI))
		{
			currentAngle -= (float)(2 * Math.PI);
		}
//		System.out.println("CURRENT ANGLE: " + currentAngle);
		
		float currentDistance = (float)(Math.sqrt((mouseX - x) * (mouseX - x) + (mouseY - y) * (mouseY - y)));
		int level = 0;
		
		for(int i = 0; i < numberOfLevels; i++)
		{
			float lowerDistance = (radius + (i + 0.5f) * noteThickness) / 2;
			float upperDistance = (radius + (i + 1.5f) * noteThickness) / 2;
			
			if(lowerDistance <= currentDistance && upperDistance > currentDistance)
			{
				level = i + 1;
				break;
			}
		}
		
		for(int i = 0; i < timeSignatureNumerator; i++)
		{
			float startAngle = (float)(2 * Math.PI * i / timeSignatureNumerator);
			float endAngle = (float)(2 * Math.PI * (i + 1) / timeSignatureNumerator);
			
			if(startAngle <= currentAngle && endAngle > currentAngle)
			{
				for(int noteIndex = 0; noteIndex < notes.size();)
				{
					LoopNote note = notes.get(noteIndex);
					if(note.getStartNoteInterval() == i && note.getLevel() == level)
					{
						notes.remove(noteIndex);
					}
					else
					{
						noteIndex++;
					}
				}
				break;
			}
		}
		
		player.addNotes(notes);
	}
	
	public void recordNoteOn(int mouseX, int mouseY, int pmouseX, int pmouseY)
	{
		float startAngle = 0f;
		float endAngle = 0f;
		int level = 0;
		float theta = 0f;
		float currentAngle = (float)(-1 * Math.PI / 2 - rotationOffset);
		float currentDistance = (float)(Math.sqrt((mouseX - x) * (mouseX - x) + (mouseY - y) * (mouseY - y)));
		float maxDistance = (radius + (numberOfLevels) * noteThickness) / 2;
		float minDistance = radius / 2;
		int i = 0;
		
		if(currentDistance < minDistance || currentDistance > maxDistance)
		{
//			System.out.println("Out of bounds\n");
			return;
		}
		
//		System.out.println("Current Angle: " + currentAngle);
//		System.out.println("Current Distance: " + currentDistance);
//		System.out.println("Min Distance: " + minDistance);
//		System.out.println("Max Distance: " + maxDistance);
		
		for(i = 0; theta <= currentAngle ; i++)
		{
			theta = (float)(2 * Math.PI * i / timeSignatureNumerator);	
		}
		
		startAngle = (float)(2 * Math.PI * (i - 1) / timeSignatureNumerator);
		endAngle = (float)(2 * Math.PI * i / timeSignatureNumerator);
		
		float lowerDistance = 0;
		
		for(i = 1; lowerDistance < currentDistance; i++)
		{
			lowerDistance = (radius + i * noteThickness) / 2;
//			System.out.println("Lower Distance (i): " + lowerDistance + " (" + i + ")");
		}
		
		level = i - 1;
		
//		System.out.println("Start Angle: " + startAngle);
//		System.out.println("End Angle: " + endAngle);
//		System.out.println("Level: " + level + "\n");
		
		currentNote = new LoopNote(startAngle, endAngle, level, timePerBeat, timePerMeasure, timeSignatureNumerator);
		notes.add(currentNote);
	}
	
	public void mouseReleased(int mouseX, int mouseY, int pmouseX, int pmouseY)
	{
		if(mouseX < (x - boundingWidth / 2f) || mouseX > (x + boundingWidth / 2f) ||
				mouseY < (y - boundingHeight / 2f) || mouseY > (y + boundingHeight / 2f) ||
				pmouseX < (x - boundingWidth / 2f) || pmouseX > (x + boundingWidth / 2f) ||
				pmouseY < (y - boundingHeight / 2f) || pmouseY > (y + boundingHeight / 2f))
		{
			return;
		}
		
		if(!PLAY_MODE && isRecording)
		{
			isRecordingNote = false;
			
			float currentDistance = (float)(Math.sqrt((mouseX - x) * (mouseX - x) + (mouseY - y) * (mouseY - y)));
			float maxDistance = (radius + (numberOfLevels) * noteThickness) / 2;
			float minDistance = radius / 2;
			
			if(currentDistance < minDistance || currentDistance > maxDistance)
			{
//				System.out.println("Out of bounds\n");
				return;
			}
			
			recordNoteOff(mouseX, mouseY, pmouseX, pmouseY);
			
			LoopNote overflowNote = notes.get(notes.size() - 1).normalize();
			System.out.println("Current Note: " + notes.get(notes.size() - 1).toString());
			if(notes.get(notes.size() - 1).getStartNoteInterval() == notes.get(notes.size() - 1).getEndNoteInterval())
			{
				notes.remove(notes.size() - 1);
//				System.out.println("Removed empty note");
			}
			if(overflowNote != null)
			{
				notes.add(overflowNote);
				System.out.println("Current Overflow Note: " + notes.get(notes.size() - 1).toString());
			}
		}
	}
	
	public void recordNoteOff(int mouseX, int mouseY, int pmouseX, int pmouseY)
	{
		if(notes.get(notes.size() - 1) != null)
		{
			float endAngle = 0f;
			float theta = 0f;
			float currentAngle = (float)(-1 * Math.PI / 2 - rotationOffset);
			int i = 0;
			for(; theta <= currentAngle ; i++)
			{
				theta = (float)(2 * Math.PI * i / timeSignatureNumerator);	
			}
			
			endAngle = (float)(2 * Math.PI * i / timeSignatureNumerator);
			notes.get(notes.size() - 1).setEndAngle(endAngle);
			
			currentNote = null;
		}
	}
	
	public void mouseDragged(int mouseX, int mouseY, int pmouseX, int pmouseY)
	{
		if(mouseX < (x - boundingWidth / 2f) || mouseX > (x + boundingWidth / 2f) ||
				mouseY < (y - boundingHeight / 2f) || mouseY > (y + boundingHeight / 2f) ||
				pmouseX < (x - boundingWidth / 2f) || pmouseX > (x + boundingWidth / 2f) ||
				pmouseY < (y - boundingHeight / 2f) || pmouseY > (y + boundingHeight / 2f))
		{
			return;
		}
		
		
	}
	
	public float getPlayheadAngle()
	{
		return playheadAngle;
	}

	public void setPlayheadAngle(float playheadAngle)
	{
		this.playheadAngle = playheadAngle;
	}

	public float getRotationOffset()
	{
		return rotationOffset;
	}

	public void setRotationOffset(float rotationOffset)
	{
		this.rotationOffset = rotationOffset;
	}

	public int getTempo()
	{
		return tempo;
	}

	public void setTempo(int tempo)
	{
		this.tempo = tempo;
		player.setTempo(tempo);
		calculateTimes();
	}

	public int getTimeSignatureNumerator()
	{
		return timeSignatureNumerator;
	}

	public void setTimeSignatureNumerator(int timeSignatureNumerator)
	{
		this.timeSignatureNumerator = timeSignatureNumerator;
		player.setTimeSignatureNumerator(timeSignatureNumerator);
		calculateTimes();
	}

	public int getTimeSignatureDenominator()
	{
		return timeSignatureDenominator;
	}

	public void setTimeSignatureDenominator(int timeSignatureDenominator)
	{
		this.timeSignatureDenominator = timeSignatureDenominator;
		player.setTimeSignatureDenominator(timeSignatureDenominator);
		calculateTimes();
	}

	public boolean isPLAY_MODE()
	{
		return PLAY_MODE;
	}

	public void setPLAY_MODE(boolean pLAY_MODE)
	{
		PLAY_MODE = pLAY_MODE;
	}

	public boolean isPlaying()
	{
		return isPlaying;
	}

	public void setPlaying(boolean isPlaying)
	{
		this.isPlaying = isPlaying;
	}

	public boolean isRecording()
	{
		return isRecording;
	}

	public void setRecording(boolean isRecording)
	{
		this.isRecording = isRecording;
	}

	public int getX()
	{
		return x;
	}

	public void setX(int x)
	{
		this.x = x;
	}

	public int getY()
	{
		return y;
	}

	public void setY(int y)
	{
		this.y = y;
	}

	public float getRadius()
	{
		return radius;
	}

	public void setRadius(float radius)
	{
		this.radius = radius;
	}

	public float getNoteThickness()
	{
		return noteThickness;
	}

	public void setNoteThickness(float noteThickness)
	{
		this.noteThickness = noteThickness;
	}

	public int getNumberOfLevels()
	{
		return numberOfLevels;
	}

	public void setNumberOfLevels(int numberOfLevels)
	{
		this.numberOfLevels = numberOfLevels;
	}

	public ArrayList<LoopNote> getNotes()
	{
		return notes;
	}

	public void setNotes(ArrayList<LoopNote> notes)
	{
		this.notes = notes;
	}
	
	public ArrayList<Integer> getColors()
	{
		return colors;
	}

	public void setColors(ArrayList<Integer> colors)
	{
		this.colors = colors;
	}

	public float getTimePerBeat()
	{
		return timePerBeat;
	}

	public void setTimePerBeat(float timePerBeat)
	{
		this.timePerBeat = timePerBeat;
	}

	public float getTimePerMeasure()
	{
		return timePerMeasure;
	}

	public void setTimePerMeasure(float timePerMeasure)
	{
		this.timePerMeasure = timePerMeasure;
	}

	public long getStartTime()
	{
		return startPlayTime;
	}

	public void setStartTime(long startTime)
	{
		this.startPlayTime = startTime;
	}

	public String getInstrument()
	{
		return instrument;
	}

	public void setInstrument(String instrument)
	{
		this.instrument = instrument;
		player.setInstrument(instrument);
		addNotes();
	}

	public Scales getCurrentScale()
	{
		return currentScale;
	}

	public void setCurrentScale(Scales currentScale)
	{
		this.currentScale = currentScale;
		player.setScale(currentScale);
		addNotes();
	}

	public String getChromaticRoot()
	{
		return chromaticRoot;
	}

	public void setChromaticRoot(String chromaticRoot)
	{
		this.chromaticRoot = chromaticRoot;
		player.setChromaticRoot(chromaticRoot);
		addNotes();
	}

	public int getOctave()
	{
		return octave;
	}

	public void setOctave(int octave)
	{
		this.octave = octave;
		player.setOctave(octave);
		addNotes();
	}

	public String getChordSignature()
	{
		return chordSignature;
	}

	public void setChordSignature(String chordSignature)
	{
		this.chordSignature = chordSignature;
		player.setChordSignature(chordSignature);
		addNotes();
	}

	public MusicPlayer getPlayer()
	{
		return player;
	}

	public void setPlayer(MusicPlayer player)
	{
		this.player = player;
	}
}
