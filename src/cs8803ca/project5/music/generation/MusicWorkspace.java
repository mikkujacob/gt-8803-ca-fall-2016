package cs8803ca.project5.music.generation;

import java.util.ArrayList;
import java.util.Arrays;

import org.jfugue.theory.Note;

import controlP5.ControlP5;
import controlP5.ControllerInterface;
import controlP5.DropdownList;
import cs8803ca.project5.music.analysis.MusicAnalysis;
import cs8803ca.project5.music.generation.MusicPlayer.Scales;

import processing.core.PApplet;

public class MusicWorkspace
{
	private PApplet parent;
	private int width;
	private int height;
	private ArrayList<LoopCircle> loops;
	private ControlP5 cp5;
	
	public MusicWorkspace(PApplet parent)
	{
		this.parent = parent;
		this.width = parent.width;
		this.height = parent.height;
		loops = new ArrayList<LoopCircle>();
		cp5 = new ControlP5(this.parent);
		
		float x, y, w, h;
		
		//Add Circle 1
		x = width * 0.25f;
		y = height * 0.25f;
		LoopCircle circle = new LoopCircle(Math.round(x), Math.round(y), 7, parent);
		loops.add(circle);
		MusicAnalysis.setPlayer1(circle);
		
		x = width * 0.025f;
		y = height * 0.05f;
		w = width * 0.035f;
		h = height * 0.05f;
		cp5.addNumberbox("tempoNumberBox1", 120, Math.round(x), Math.round(y), Math.round(w), Math.round(h)).setLabelVisible(false).setCaptionLabel("Tempo").setRange(1, 240);
		
		x = width * 0.025f;
		y = height * 0.15f;
		cp5.addNumberbox("timeSignatureNumeratorNumberBox1", 8, Math.round(x), Math.round(y), Math.round(w), Math.round(h)).setLabelVisible(false).setCaptionLabel("").setRange(1, 32);
		
		x = width * 0.025f;
		y = height * 0.20f;
		cp5.addNumberbox("timeSignatureDenominatorNumberBox1", 8, Math.round(x), Math.round(y), Math.round(w), Math.round(h)).setLabelVisible(false).setCaptionLabel("Time Signature").setRange(1, 32);
		
		//Add Record Button 1
		w = width * 0.075f;
		h = height * 0.05f;
		x = width * 0.025f;
		y = height * 0.3f;
		cp5.addButton("recordButton1").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Start Recording");
		
		//Add Play Button 1
		x = width * 0.025f;
		y = height * 0.375f;
		cp5.addButton("playButton1").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Start Playback");
		
		//Add Note Dropdown List 1
		x = width * 0.4f;
		y = height * 0.05f;
		w = width * 0.075f;
		h = height * 0.05f;
		cp5.addDropdownList("noteList1").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Scale Root").addItems(Note.NOTE_NAMES_COMMON);
		
		//Add Scale Dropdown List 1
		x = width * 0.4f;
		y = height * 0.15f;
		w = width * 0.075f;
		h = height * 0.05f;
		cp5.addDropdownList("scaleList1").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Scale").addItems(new String[]{Scales.MAJOR.toString(), Scales.MINOR.toString(), Scales.CIRCLE_OF_FIFTHS.toString(), Scales.CHROMATIC.toString()});
		
		//Add Octave Number Box 1
		x = width * 0.4f;
		y = height * 0.2f;
		w = width * 0.03f;
		h = height * 0.05f;
		cp5.addNumberbox("octaveBox1", 5, Math.round(x), Math.round(y), Math.round(w), Math.round(h)).setLabelVisible(false).setCaptionLabel("Octave").setRange(1, 10);
		
		//Add Chord Signature Dropdown List 1
		x = width * 0.4f;
		y = height * 0.3f;
		w = width * 0.075f;
		h = height * 0.05f;
		ArrayList<String> chordList = new ArrayList<String>(Arrays.asList(MusicPlayer.ALL_CHORD_SIGNATURES));
		chordList.add(0, "");
		cp5.addDropdownList("chordList1").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Chord Signature").addItems(chordList);
		
		//Add Instrument Dropdown List 1
		x = width * 0.4f;
		y = height * 0.375f;
		w = width * 0.075f;
		h = height * 0.05f;
		
		cp5.addDropdownList("instrumentList1").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Instrument").addItems(MusicPlayer.INSTRUMENTS);
		
		//Add Circle 2
		x = width * 0.25f;
		y = height * 0.75f;
		circle = new LoopCircle(Math.round(x), Math.round(y), 7, parent);
		loops.add(circle);
		MusicAnalysis.setPlayer2(circle);
		
		x = width * 0.025f;
		y = height * 0.55f;
		w = width * 0.035f;
		h = height * 0.05f;
		cp5.addNumberbox("tempoNumberBox2", 120, Math.round(x), Math.round(y), Math.round(w), Math.round(h)).setLabelVisible(false).setCaptionLabel("Tempo").setRange(1, 240);
		
		x = width * 0.025f;
		y = height * 0.65f;
		cp5.addNumberbox("timeSignatureNumeratorNumberBox2", 8, Math.round(x), Math.round(y), Math.round(w), Math.round(h)).setLabelVisible(false).setCaptionLabel("").setRange(1, 32);
		
		x = width * 0.025f;
		y = height * 0.70f;
		cp5.addNumberbox("timeSignatureDenominatorNumberBox2", 8, Math.round(x), Math.round(y), Math.round(w), Math.round(h)).setLabelVisible(false).setCaptionLabel("Time Signature").setRange(1, 32);
		
		//Add Record Button 2
		w = width * 0.075f;
		h = height * 0.05f;
		x = width * 0.025f;
		y = height * 0.8f;
		cp5.addButton("recordButton2").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Start Recording");
		
		//Add Play Button 2
		x = width * 0.025f;
		y = height * 0.875f;
		cp5.addButton("playButton2").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Start Playback");
		
		//Add Note Dropdown List 2
		x = width * 0.4f;
		y = height * 0.55f;
		w = width * 0.075f;
		h = height * 0.05f;
		cp5.addDropdownList("noteList2").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Scale Root").addItems(Note.NOTE_NAMES_COMMON);
		
		//Add Scale Dropdown List 2
		x = width * 0.4f;
		y = height * 0.65f;
		w = width * 0.075f;
		h = height * 0.05f;
		cp5.addDropdownList("scaleList2").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Scale").addItems(new String[]{Scales.MAJOR.toString(), Scales.MINOR.toString(), Scales.CIRCLE_OF_FIFTHS.toString()});
		
		//Add Octave Number Box 2
		x = width * 0.4f;
		y = height * 0.7f;
		w = width * 0.03f;
		h = height * 0.05f;
		cp5.addNumberbox("octaveBox2", 5, Math.round(x), Math.round(y), Math.round(w), Math.round(h)).setLabelVisible(false).setCaptionLabel("Octave").setRange(1, 10);
		
		//Add Chord Signature Dropdown List 2
		x = width * 0.4f;
		y = height * 0.8f;
		w = width * 0.075f;
		h = height * 0.05f;
		cp5.addDropdownList("chordList2").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Chord Signature").addItems(chordList);
		
		//Add Instrument Dropdown List 2
		x = width * 0.4f;
		y = height * 0.875f;
		w = width * 0.075f;
		h = height * 0.05f;
		cp5.addDropdownList("instrumentList2").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Instrument").addItems(MusicPlayer.INSTRUMENTS);
		
		//Add Circle 3
		x = width * 0.75f;
		y = height * 0.25f;
		circle = new LoopCircle(Math.round(x), Math.round(y), 7, parent);
		loops.add(circle);
		MusicAnalysis.setPlayer3(circle);
		
		x = width * 0.525f;
		y = height * 0.05f;
		w = width * 0.035f;
		h = height * 0.05f;
		cp5.addNumberbox("tempoNumberBox3", 120, Math.round(x), Math.round(y), Math.round(w), Math.round(h)).setLabelVisible(false).setCaptionLabel("Tempo").setRange(1, 240);
		
		x = width * 0.525f;
		y = height * 0.15f;
		cp5.addNumberbox("timeSignatureNumeratorNumberBox3", 8, Math.round(x), Math.round(y), Math.round(w), Math.round(h)).setLabelVisible(false).setCaptionLabel("").setRange(1, 32);
		
		x = width * 0.525f;
		y = height * 0.20f;
		cp5.addNumberbox("timeSignatureDenominatorNumberBox3", 8, Math.round(x), Math.round(y), Math.round(w), Math.round(h)).setLabelVisible(false).setCaptionLabel("Time Signature").setRange(1, 32);
		
		//Add Record Button 3
		w = width * 0.075f;
		h = height * 0.05f;
		x = width * 0.525f;
		y = height * 0.3f;
		cp5.addButton("recordButton3").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Start Recording");
		
		//Add Play Button 3
		x = width * 0.525f;
		y = height * 0.375f;
		cp5.addButton("playButton3").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Start Playback");
		
		//Add Note Dropdown List 3
		x = width * 0.9f;
		y = height * 0.05f;
		w = width * 0.075f;
		h = height * 0.05f;
		cp5.addDropdownList("noteList3").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Scale Root").addItems(Note.NOTE_NAMES_COMMON);
		
		//Add Scale Dropdown List 3
		x = width * 0.9f;
		y = height * 0.15f;
		w = width * 0.075f;
		h = height * 0.05f;
		cp5.addDropdownList("scaleList3").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Scale").addItems(new String[]{Scales.MAJOR.toString(), Scales.MINOR.toString(), Scales.CIRCLE_OF_FIFTHS.toString()});
		
		//Add Octave Number Box 3
		x = width * 0.9f;
		y = height * 0.2f;
		w = width * 0.03f;
		h = height * 0.05f;
		cp5.addNumberbox("octaveBox3", 5, Math.round(x), Math.round(y), Math.round(w), Math.round(h)).setLabelVisible(false).setCaptionLabel("Octave").setRange(1, 10);
		
		//Add Chord Signature Dropdown List 3
		x = width * 0.9f;
		y = height * 0.3f;
		w = width * 0.075f;
		h = height * 0.05f;
		cp5.addDropdownList("chordList3").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Chord Signature").addItems(chordList);
		
		//Add Instrument Dropdown List 3
		x = width * 0.9f;
		y = height * 0.375f;
		w = width * 0.075f;
		h = height * 0.05f;
		cp5.addDropdownList("instrumentList3").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Instrument").addItems(MusicPlayer.INSTRUMENTS);
		
		//Add Circle 4
		x = width * 0.75f;
		y = height * 0.75f;
		circle = new LoopCircle(Math.round(x), Math.round(y), 7, parent);
		loops.add(circle);
		MusicAnalysis.setPlayer4(circle);
		
		x = width * 0.525f;
		y = height * 0.55f;
		w = width * 0.035f;
		h = height * 0.05f;
		cp5.addNumberbox("tempoNumberBox4", 120, Math.round(x), Math.round(y), Math.round(w), Math.round(h)).setLabelVisible(false).setCaptionLabel("Tempo").setRange(1, 240);
		
		x = width * 0.525f;
		y = height * 0.65f;
		cp5.addNumberbox("timeSignatureNumeratorNumberBox4", 8, Math.round(x), Math.round(y), Math.round(w), Math.round(h)).setLabelVisible(false).setCaptionLabel("").setRange(1, 32);
		
		x = width * 0.525f;
		y = height * 0.70f;
		cp5.addNumberbox("timeSignatureDenominatorNumberBox4", 8, Math.round(x), Math.round(y), Math.round(w), Math.round(h)).setLabelVisible(false).setCaptionLabel("Time Signature").setRange(1, 32);
		
		//Add Record Button 4
		w = width * 0.075f;
		h = height * 0.05f;
		x = width * 0.525f;
		y = height * 0.8f;
		cp5.addButton("recordButton4").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Start Recording");
		
		//Add Play Button 4
		x = width * 0.525f;
		y = height * 0.875f;
		cp5.addButton("playButton4").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Start Playback");
		
		//Add Note Dropdown List 4
		x = width * 0.9f;
		y = height * 0.55f;
		w = width * 0.075f;
		h = height * 0.05f;
		cp5.addDropdownList("noteList4").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Scale Root").addItems(Note.NOTE_NAMES_COMMON);
		
		//Add Scale Dropdown List 4
		x = width * 0.9f;
		y = height * 0.65f;
		w = width * 0.075f;
		h = height * 0.05f;
		cp5.addDropdownList("scaleList4").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Scale").addItems(new String[]{Scales.MAJOR.toString(), Scales.MINOR.toString(), Scales.CIRCLE_OF_FIFTHS.toString()});
		
		//Add Octave Number Box 4
		x = width * 0.9f;
		y = height * 0.7f;
		w = width * 0.03f;
		h = height * 0.05f;
		cp5.addNumberbox("octaveBox4", 5, Math.round(x), Math.round(y), Math.round(w), Math.round(h)).setLabelVisible(false).setCaptionLabel("Octave").setRange(1, 10);
		
		//Add Chord Signature Dropdown List 4
		x = width * 0.9f;
		y = height * 0.8f;
		w = width * 0.075f;
		h = height * 0.05f;
		cp5.addDropdownList("chordList4").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Chord Signature").addItems(chordList);
		
		//Add Instrument Dropdown List 4
		x = width * 0.9f;
		y = height * 0.875f;
		w = width * 0.075f;
		h = height * 0.05f;
		cp5.addDropdownList("instrumentList4").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Instrument").addItems(MusicPlayer.INSTRUMENTS);
		
		//Add Global Play / Pause Button
		w = width * 0.075f;
		h = height * 0.05f;
		x = width * 0.5f - w / 2f;
		y = height * 0.5f - h / 2f;
		cp5.addButton("playPauseButton").setPosition(x, y).setSize(Math.round(w), Math.round(h)).setCaptionLabel("Start Playback");
	}
	
	public void tempoNumberBox1(int tempo)
	{
		loops.get(0).setTempo(tempo);
	}
	
	public void tempoNumberBox2(int tempo)
	{
		loops.get(1).setTempo(tempo);
	}
	
	public void tempoNumberBox3(int tempo)
	{
		loops.get(2).setTempo(tempo);
	}
	
	public void tempoNumberBox4(int tempo)
	{
		loops.get(3).setTempo(tempo);
	}
	
	public void timeSignatureNumeratorNumberBox1(int numerator)
	{
		loops.get(0).setTimeSignatureNumerator(numerator);
	}
	
	public void timeSignatureNumeratorNumberBox2(int numerator)
	{
		loops.get(1).setTimeSignatureNumerator(numerator);
	}
	
	public void timeSignatureNumeratorNumberBox3(int numerator)
	{
		loops.get(2).setTimeSignatureNumerator(numerator);
	}
	
	public void timeSignatureNumeratorNumberBox4(int numerator)
	{
		loops.get(3).setTimeSignatureNumerator(numerator);
	}

	public void timeSignatureDenominatorNumberBox1(int denominator)
	{
		loops.get(0).setTimeSignatureDenominator(denominator);
	}

	public void timeSignatureDenominatorNumberBox2(int denominator)
	{
		loops.get(1).setTimeSignatureDenominator(denominator);
	}

	public void timeSignatureDenominatorNumberBox3(int denominator)
	{
		loops.get(2).setTimeSignatureDenominator(denominator);
	}

	public void timeSignatureDenominatorNumberBox4(int denominator)
	{
		loops.get(3).setTimeSignatureDenominator(denominator);
	}
	
	public void recordButton1()
	{
		if(cp5.getController("recordButton1").getCaptionLabel().getText().equalsIgnoreCase("Start Recording"))
		{
			cp5.getController("recordButton1").getCaptionLabel().setText("Stop Recording");
		}
		else
		{
			cp5.getController("recordButton1").getCaptionLabel().setText("Start Recording");
		}
		
		loops.get(0).setPLAY_MODE(false);
		loops.get(0).toggleRecording();
	}
	
	public void recordButton2()
	{
		if(cp5.getController("recordButton2").getCaptionLabel().getText().equalsIgnoreCase("Start Recording"))
		{
			cp5.getController("recordButton2").getCaptionLabel().setText("Stop Recording");
		}
		else
		{
			cp5.getController("recordButton2").getCaptionLabel().setText("Start Recording");
		}

		loops.get(1).setPLAY_MODE(false);
		loops.get(1).toggleRecording();
	}
	
	public void recordButton3()
	{
		if(cp5.getController("recordButton3").getCaptionLabel().getText().equalsIgnoreCase("Start Recording"))
		{
			cp5.getController("recordButton3").getCaptionLabel().setText("Stop Recording");
		}
		else
		{
			cp5.getController("recordButton3").getCaptionLabel().setText("Start Recording");
		}

		loops.get(2).setPLAY_MODE(false);
		loops.get(2).toggleRecording();
	}
	
	public void recordButton4()
	{
		if(cp5.getController("recordButton4").getCaptionLabel().getText().equalsIgnoreCase("Start Recording"))
		{
			cp5.getController("recordButton4").getCaptionLabel().setText("Stop Recording");
		}
		else
		{
			cp5.getController("recordButton4").getCaptionLabel().setText("Start Recording");
		}

		loops.get(3).setPLAY_MODE(false);
		loops.get(3).toggleRecording();
	}
	
	public void playButton1()
	{
		if(cp5.getController("playButton1").getCaptionLabel().getText().equalsIgnoreCase("Start Playback"))
		{
			cp5.getController("playButton1").getCaptionLabel().setText("Stop Playback");
		}
		else
		{
			cp5.getController("playButton1").getCaptionLabel().setText("Start Playback");
		}
		
		loops.get(0).setPLAY_MODE(true);
		loops.get(0).togglePlaying();
	}
	
	public void playButton2()
	{
		if(cp5.getController("playButton2").getCaptionLabel().getText().equalsIgnoreCase("Start Playback"))
		{
			cp5.getController("playButton2").getCaptionLabel().setText("Stop Playback");
		}
		else
		{
			cp5.getController("playButton2").getCaptionLabel().setText("Start Playback");
		}

		loops.get(1).setPLAY_MODE(true);
		loops.get(1).togglePlaying();
	}
	
	public void playButton3()
	{
		if(cp5.getController("playButton3").getCaptionLabel().getText().equalsIgnoreCase("Start Playback"))
		{
			cp5.getController("playButton3").getCaptionLabel().setText("Stop Playback");
		}
		else
		{
			cp5.getController("playButton3").getCaptionLabel().setText("Start Playback");
		}

		loops.get(2).setPLAY_MODE(true);
		loops.get(2).togglePlaying();
	}
	
	public void playButton4()
	{
		if(cp5.getController("playButton4").getCaptionLabel().getText().equalsIgnoreCase("Start Playback"))
		{
			cp5.getController("playButton4").getCaptionLabel().setText("Stop Playback");
		}
		else
		{
			cp5.getController("playButton4").getCaptionLabel().setText("Start Playback");
		}

		loops.get(3).setPLAY_MODE(true);
		loops.get(3).togglePlaying();
	}
	
	public void noteList1(int itemIndex)
	{
		loops.get(0).setChromaticRoot((String)((DropdownList)cp5.getController("noteList1")).getItem(itemIndex).get("text"));
	}
	
	public void noteList2(int itemIndex)
	{
		loops.get(1).setChromaticRoot((String)((DropdownList)cp5.getController("noteList2")).getItem(itemIndex).get("text"));
	}
	
	public void noteList3(int itemIndex)
	{
		loops.get(2).setChromaticRoot((String)((DropdownList)cp5.getController("noteList3")).getItem(itemIndex).get("text"));
	}
	
	public void noteList4(int itemIndex)
	{
		loops.get(3).setChromaticRoot((String)((DropdownList)cp5.getController("noteList4")).getItem(itemIndex).get("text"));
	}
	
	public void scaleList1(int itemIndex)
	{
		loops.get(0).setCurrentScale(Scales.valueOf((String)((DropdownList)cp5.getController("scaleList1")).getItem(itemIndex).get("text")));
	}
	
	public void scaleList2(int itemIndex)
	{
		loops.get(1).setCurrentScale(Scales.valueOf((String)((DropdownList)cp5.getController("scaleList2")).getItem(itemIndex).get("text")));
	}
	
	public void scaleList3(int itemIndex)
	{
		loops.get(2).setCurrentScale(Scales.valueOf((String)((DropdownList)cp5.getController("scaleList3")).getItem(itemIndex).get("text")));
	}
	
	public void scaleList4(int itemIndex)
	{
		loops.get(3).setCurrentScale(Scales.valueOf((String)((DropdownList)cp5.getController("scaleList4")).getItem(itemIndex).get("text")));
	}
	
	public void octaveBox1(int octave)
	{
		loops.get(0).setOctave(octave);
	}
	
	public void octaveBox2(int octave)
	{
		loops.get(1).setOctave(octave);
	}
	
	public void octaveBox3(int octave)
	{
		loops.get(2).setOctave(octave);
	}
	
	public void octaveBox4(int octave)
	{
		loops.get(3).setOctave(octave);
	}
	
	public void chordList1(int itemIndex)
	{
		loops.get(0).setChordSignature((String)((DropdownList)cp5.getController("chordList1")).getItem(itemIndex).get("text"));
	}
	
	public void chordList2(int itemIndex)
	{
		loops.get(1).setChordSignature((String)((DropdownList)cp5.getController("chordList2")).getItem(itemIndex).get("text"));
	}
	
	public void chordList3(int itemIndex)
	{
		loops.get(2).setChordSignature((String)((DropdownList)cp5.getController("chordList3")).getItem(itemIndex).get("text"));
	}
	
	public void chordList4(int itemIndex)
	{
		loops.get(3).setChordSignature((String)((DropdownList)cp5.getController("chordList4")).getItem(itemIndex).get("text"));
	}
	
	public void instrumentList1(int itemIndex)
	{
		loops.get(0).setInstrument((String)((DropdownList)cp5.getController("instrumentList1")).getItem(itemIndex).get("text"));
	}
	
	public void instrumentList2(int itemIndex)
	{
		loops.get(1).setInstrument((String)((DropdownList)cp5.getController("instrumentList2")).getItem(itemIndex).get("text"));
	}
	
	public void instrumentList3(int itemIndex)
	{
		loops.get(2).setInstrument((String)((DropdownList)cp5.getController("instrumentList3")).getItem(itemIndex).get("text"));
	}
	
	public void instrumentList4(int itemIndex)
	{
		loops.get(3).setInstrument((String)((DropdownList)cp5.getController("instrumentList4")).getItem(itemIndex).get("text"));
	}
	
	public void playPauseButton()
	{
//		System.out.println("MusicWorkspace.playPauseButton(): " + System.currentTimeMillis());
		if(cp5.getController("playPauseButton").getCaptionLabel().getText().equalsIgnoreCase("Start Playback"))
		{
			cp5.getController("playPauseButton").getCaptionLabel().setText("Stop Playback");
			cp5.getController("playButton1").getCaptionLabel().setText("Stop Playback");
			cp5.getController("playButton2").getCaptionLabel().setText("Stop Playback");
			cp5.getController("playButton3").getCaptionLabel().setText("Stop Playback");
			cp5.getController("playButton4").getCaptionLabel().setText("Stop Playback");
			
			for(LoopCircle loop : loops)
			{
				loop.setPLAY_MODE(true);
				loop.startPlaying();
			}
		}
		else
		{
			cp5.getController("playPauseButton").getCaptionLabel().setText("Start Playback");
			cp5.getController("playButton1").getCaptionLabel().setText("Start Playback");
			cp5.getController("playButton2").getCaptionLabel().setText("Start Playback");
			cp5.getController("playButton3").getCaptionLabel().setText("Start Playback");
			cp5.getController("playButton4").getCaptionLabel().setText("Start Playback");
			
			for(LoopCircle loop : loops)
			{
				loop.setPLAY_MODE(true);
				loop.stopPlaying();
			}
		}
	}
	
	public void draw()
	{
		//Draw loop circles
		for(LoopCircle loop : loops)
		{
			loop.draw();
		}
	}
	
	public void mousePressed(int mouseX, int mouseY, int pmouseX, int pmouseY)
	{
		for(LoopCircle loop : loops)
		{
			loop.mousePressed(mouseX, mouseY, pmouseX, pmouseY);
		}
	}
	
	public void mouseReleased(int mouseX, int mouseY, int pmouseX, int pmouseY)
	{
		for(LoopCircle loop : loops)
		{
			loop.mouseReleased(mouseX, mouseY, pmouseX, pmouseY);
		}
	}
	
	public void mouseDragged(int mouseX, int mouseY, int pmouseX, int pmouseY)
	{
		for(LoopCircle loop : loops)
		{
			loop.mouseDragged(mouseX, mouseY, pmouseX, pmouseY);
		}
	}
	
	public void keyPressed(char key, int keyCode)
	{
		if(key == 'm' || key == 'M')
		{
			for(ControllerInterface<?> control : cp5.getAll())
			{
				control.show();
			}
		}
		
		if(key == 'v' || key == 'V')
		{
			for(ControllerInterface<?> control : cp5.getAll())
			{
				control.hide();
			}
		}
	}
	
	public void keyReleased(char key, int keyCode)
	{
		
	}
}
