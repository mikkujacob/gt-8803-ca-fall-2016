package cs8803ca.project5.music.generation;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.sound.midi.MidiUnavailableException;

import org.jfugue.realtime.RealtimePlayer;
import org.jfugue.theory.Chord;
import org.jfugue.theory.Intervals;
import org.jfugue.theory.Note;
import org.jfugue.theory.Scale;

//import arb.soundcipher.SCScore;
//import arb.soundcipher.SoundCipher;

public class MusicPlayer
{
	private RealtimePlayer player;
	private String[] noteNames = Note.NOTE_NAMES_COMMON;
	public static String[] ALL_CHORD_SIGNATURES = Chord.getChordNames();
	private Scale scale;
	private Scales scaleName;
//	private boolean isPlayingNotes;
	private float timePerMeasure;
	private float timePerBeat;
	private int tempo;
	private int timeSignatureNumerator;
	private int timeSignatureDenominator;
	private String chromaticRoot;
	private String chordSignature;
	private int octave;
	private String instrument;
	private static final float EPSILON = 0.01f;
	private boolean isPlaying;
	public static final ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(8);
	private ScheduledFuture<?> taskFuture;
	private ArrayList<String> musicStrings;
	private int currentNoteIntervalIndex;
	private boolean musicChanged;
	private long startTime;
	private ArrayList<ArrayList<MusicNote>> musicNotesList;
//	private SoundCipher scPlayer;
//	private SCScore scScore;
	
	public static String[] INSTRUMENTS = new String[] { "PERCUSSION", "PIANO",
			"BRIGHT_ACOUSTIC", "ELECTRIC_GRAND", "HONKEY_TONK",
			"ELECTRIC_PIANO", "ELECTRIC_PIANO2", "HARPISCHORD", "CLAVINET",
			"DRAWBAR_ORGAN", "PERCUSSIVE_ORGAN", "ROCK_ORGAN", "CHURCH_ORGAN",
			"REED_ORGAN", "ACCORIDAN", "HARMONICA", "TANGO_ACCORDIAN",
			"ACOUSTIC_BASS", "ELECTRIC_BASS_FINGER", "ELECTRIC_BASS_PICK",
			"FRETLESS_BASS", "SLAP_BASS_1", "SLAP_BASS_2", "SYNTH_BASS_1",
			"SYNTH_BASS_2", "CELESTA", "GLOCKENSPIEL", "MUSIC_BOX",
			"VIBRAPHONE", "MARIMBA", "XYLOPHONE", "TUBULAR_BELLS", "DULCIMER",
			"GUITAR", "STEEL_STRING_GUITAR", "ELECTRIC_JAZZ_GUITAR",
			"ELECTRIC_CLEAN_GUITAR", "ELECTRIC_MUTED_GUITAR",
			"OVERDRIVEN_GUITAR", "DISTORTION_GUITAR", "GUITAR_HARMONICS",
			"VIOLIN", "VIOLA", "CELLO", "CONTRABASS", "TREMOLO_STRINGS",
			"PIZZICATO_STRINGS", "ORCHESTRAL_STRINGS", "TIMPANI",
			"STRING_ENSEMBLE_1", "STRING_ENSEMBLE_2", "SYNTH_STRINGS_1",
			"SYNTH_STRINGS_2", "CHOIR_AAHS", "VOICE_OOHS", "SYNTH_VOICE",
			"ORCHESTRA_HIT", "TRUMPET", "TROMBONE", "TUBA", "MUTED_TRUMPET",
			"FRENCH_HORN", "BRASS_SECTION", "SYNTHBRASS_1", "SYNTHBRASS_2",
			"SOPRANO_SAX", "ALTO_SAX", "TENOR_SAX", "BARITONE_SAX", "OBOE",
			"ENGLISH_HORN", "BASSOON", "CLARINET", "LEAD_SQUARE",
			"LEAD_SAWTOOTH", "LEAD_CALLIOPE", "LEAD_CHIFF", "LEAD_CHARANG",
			"LEAD_VOICE", "LEAD_FIFTHS", "LEAD_BASSLEAD", "PICCOLO", "FLUTE",
			"RECORDER", "PAN_FLUTE", "BLOWN_BOTTLE", "SKAKUHACHI", "WHISTLE",
			"OCARINA", "PAD_NEW_AGE", "PAD_WARM", "PAD_POLYSYNTH", "PAD_CHOIR",
			"PAD_BOWED", "PAD_METALLIC", "PAD_HALO", "PAD_SWEEP",
			"TINKLE_BELL", "AGOGO", "STEEL_DRUMS", "WOODBLOCK", "TAIKO_DRUM",
			"MELODIC_TOM", "SYNTH_DRUM", "REVERSE_CYMBAL", "SITAR", "BANJO",
			"SHAMISEN", "KOTO", "KALIMBA", "BAGPIPE", "FIDDLE", "SHANAI" };
	
	public static String[] INSTRUMENTS_SC = new String[] { "PIANO",
			"ACOUSTIC_GRAND", "BRIGHT_ACOUSTIC", "ELECTRIC_GRAND", "HONKYTONK",
			"HONKYTONK_PIANO", "EPIANO", "ELECTRIC_PIANO", "ELPIANO", "RHODES",
			"EPIANO2", "DX_EPIANO", "HARPSICHORD", "CLAV", "CLAVINET",
			"CELESTE", "CELESTA", "GLOCKENSPIEL", "GLOCK", "MUSIC_BOX",
			"VIBRAPHONE", "VIBES", "MARIMBA", "XYLOPHONE", "TUBULAR_BELL",
			"TUBULAR_BELLS", "ORGAN", "ELECTRIC_ORGAN", "ORGAN2", "JAZZ_ORGAN",
			"ORGAN3", "HAMMOND_ORGAN", "CHURCH_ORGAN", "PIPE_ORGAN",
			"REED_ORGAN", "ACCORDION", "PIANO_ACCORDION", "CONCERTINA",
			"HARMONICA", "BANDNEON", "NYLON_GUITAR", "NGUITAR", "GUITAR",
			"ACOUSTIC_GUITAR", "AC_GUITAR", "STEEL_GUITAR", "SGUITAR",
			"JAZZ_GUITAR", "JGUITAR", "CLEAN_GUITAR", "CGUITAR",
			"ELECTRIC_GUITAR", "EL_GUITAR", "MUTED_GUITAR", "MGUITAR",
			"OVERDRIVE_GUITAR", "OGUITAR", "DISTORTED_GUITAR", "DGUITAR",
			"DIST_GUITAR", "GUITAR_HARMONICS", "GT_HARMONICS", "HARMONICS",
			"ACOUSTIC_BASS", "ABASS", "FINGERED_BASS", "BASS", "FBASS",
			"ELECTRIC_BASS", "EL_BASS", "EBASS", "PICKED_BASS", "PBASS",
			"FRETLESS_BASS", "FRETLESS", "SLAP_BASS", "SBASS", "SLAP",
			"SYNTH_BASS", "VIOLIN", "VIOLA", "CELLO", "VIOLIN_CELLO",
			"CONTRABASS", "CONTRA_BASS", "DOUBLE_BASS", "TREMOLO_STRINGS",
			"TREMOLO", "PIZZICATO_STRINGS", "PIZZ", "PITZ", "PSTRINGS", "HARP",
			"TIMPANI", "TIMP", "STRINGS", "STR", "SLOW_STRINGS",
			"SYNTH_STRINGS", "SYN_STRINGS", "AAH", "AHHS", "CHOIR", "OOH",
			"OOHS", "VOICE", "SYNVOX", "VOX", "ORCHESTRA_HIT", "TRUMPET",
			"TROMBONE", "TUBA", "MUTED_TRUMPET", "FRENCH_HORN", "HORN",
			"BRASS", "SYNTH_BRASS", "SOPRANO_SAX", "SOPRANO",
			"SOPRANO_SAXOPHONE", "SOP", "ALTO_SAX", "ALTO", "ALTO_SAXOPHONE",
			"TENOR_SAX", "TENOR", "TENOR_SAXOPHONE", "SAX", "SAXOPHONE",
			"BARITONE_SAX", "BARI", "BARI_SAX", "BARITONE",
			"BARITONE_SAXOPHONE", "OBOE", "ENGLISH_HORN", "BASSOON",
			"CLARINET", "CLAR", "PICCOLO", "PIC", "PICC", "FLUTE", "RECORDER",
			"PAN_FLUTE", "PANFLUTE", "BOTTLE_BLOW", "BOTTLE", "SHAKUHACHI",
			"WHISTLE", "OCARINA", "GMSQUARE_WAVE", "SQUARE", "GMSAW_WAVE",
			"SAW", "SAWTOOTH", "SYNTH_CALLIOPE", "CALLOPE", "SYN_CALLIOPE",
			"CHIFFER_LEAD", "CHIFFER", "CHARANG", "SOLO_VOX", "FANTASIA",
			"WARM_PAD", "PAD", "POLYSYNTH", "POLY_SYNTH", "SPACE_VOICE",
			"BOWED_GLASS", "METAL_PAD", "HALO_PAD", "HALO", "SWEEP_PAD",
			"SWEEP", "ICE_RAIN", "ICERAIN", "SOUNDTRACK", "CRYSTAL",
			"ATMOSPHERE", "BRIGHTNESS", "GOBLIN", "ECHO_DROPS", "DROPS",
			"ECHOS", "ECHO", "ECHO_DROP", "STAR_THEME", "SITAR", "BANJO",
			"SHAMISEN", "KOTO", "KALIMBA", "THUMB_PIANO", "BAGPIPES",
			"BAG_PIPES", "BAGPIPE", "PIPES", "FIDDLE", "SHANNAI",
			"TINKLE_BELL", "BELL", "BELLS", "AGOGO", "STEEL_DRUMS",
			"STEELDRUMS", "STEELDRUM", "STEEL_DRUM", "WOODBLOCK", "WOODBLOCKS",
			"TAIKO", "DRUM", "TOM", "TOMS", "TOM_TOM", "TOM_TOMS",
			"SYNTH_DRUM", "SYNTH_DRUMS", "REVERSE_CYMBAL", "CYMBAL",
			"FRETNOISE", "FRET_NOISE", "FRET", "FRETS", "BREATHNOISE",
			"BREATH", "SEASHORE", "SEA", "RAIN", "THUNDER", "WIND", "STREAM",
			"SFX", "SOUNDEFFECTS", "SOUNDFX", "BIRD", "TELEPHONE", "PHONE",
			"HELICOPTER", "APPLAUSE" };
	
	public static String[] PERCUSSION_INSTRUMENTS = new String[] {
			"ACOUSTIC_BASS_DRUM", "BASS_DRUM", "SIDE_STICK", "LO_FLOOR_TOM",
			"HIGH_FLOOR_TOM", "LO_TOM", "LO_MID_TOM", "HI_MID_TOM",
			"HI_TOM", "ACOUSTIC_SNARE", "ELECTRIC_SNARE", "CLOSED_HI_HAT",
			"OPEN_HI_HAT", "PEDAL_HI_HAT", "CRASH_CYMBAL_1", "CRASH_CYMBAL_2",
			"RIDE_CYMBAL_1", "RIDE_CYMBAL_2", "RIDE_BELL", "CHINESE_CYMBAL",
			"SPLASH_CYMBAL" };
	
	public static String[] PERCUSSION_INSTRUMENTS_SC = new String[] {
			"ACOUSTIC_BASS_DRUM", "BASS_DRUM", "KICK", "KICK_DRUM",
			"SIDE_STICK", "ACOUSTIC_SNARE", "SNARE", "SNARE_DRUM", "HAND_CLAP",
			"ELECTRIC_SNARE", "LOW_FLOOR_TOM", "CLOSED_HI_HAT", "HIHAT",
			"HI_HAT", "HIGH_FLOOR_TOM", "PEDAL_HI_HAT", "LOW_TOM",
			"OPEN_HI_HAT", "LOW_MID_TOM", "HI_MID_TOM", "CRASH_CYMBAL_1",
			"CRASH", "HIGH_TOM", "RIDE_CYMBAL_1", "RIDE", "CHINESE_CYMBAL",
			"RIDE_BELL", "TAMBOURINE", "SPLASH_CYMBAL", "COWBELL",
			"CRASH_CYMBAL_2", "VIBRASLAP", "RIDE_CYMBAL_2", "HI_BONGO",
			"LOW_BONGO", "MUTE_HI_CONGA", "OPEN_HI_CONGA", "LOW_CONGA",
			"HIGH_TIMBALE", "LOW_TIMBALE", "HIGH_AGOGO", "LOW_AGOGO", "CABASA",
			"MARACAS", "SHORT_WHISTLE", "LONG_WHISTLE", "SHORT_GUIRO",
			"LONG_GUIRO", "CLAVES", "HI_WOOD_BLOCK", "LOW_WOOD_BLOCK",
			"MUTE_CUICA", "OPEN_CUICA", "MUTE_TRIANGLE", "OPEN_TRIANGLE",
			"TRIANGLE" };
	
	public static String[] PERCUSSION_NOTES = new String[] {
			"BASS_DRUM", "LO_TOM", "HI_MID_TOM", "ACOUSTIC_SNARE",
			"OPEN_HI_HAT", "CRASH_CYMBAL_1", "RIDE_BELL" };

	public static enum Scales
	{
		MAJOR, MINOR, CIRCLE_OF_FIFTHS, CHROMATIC
	};
	
	public static enum ScalesSC
	{
		MAJOR, MINOR, CIRCLE_OF_FIFTHS, CHROMATIC, WHOLETONE, AUGMENTED, BLUES, HARMONIC_MINOR, MELODIC_MINOR, NATURAL_MINOR, DIATONIC_MINOR, AEOLIAN, IONIAN, DORIAN, PHRYGIAN, LOCRIAN, LYDIAN, MIXOLYDIAN, PENTATONIC, MAJOR_PENTATONIC, MINOR_PENTATONIC, TURKISH, INDIAN, MAJOR_TRIAD, MINOR_TRIAD, ROOT_FIFTH, ROOT
	};
	
	public MusicPlayer()
	{
		try
		{
			player = new RealtimePlayer();
		}
		catch (MidiUnavailableException e)
		{
			e.printStackTrace();
		}
		
		isPlaying = false;
//		isPlayingNotes = true;
		currentNoteIntervalIndex = 0;
		
		musicStrings = new ArrayList<String>();
		musicNotesList = new ArrayList<ArrayList<MusicNote>>();
		
//		scPlayer = new SoundCipher();
//		scScore = new SCScore();
		
//		System.out.print("PERCUSSION NOTES:");
//		for(int i = 0; i < Note.PERCUSSION_NAMES.length; i++)
//		{
//			System.out.print("\t" + Note.PERCUSSION_NAMES[i]);
//		}
//		System.out.println();
//		
//		System.out.print("CHROMATIC SCALE:");
//		for(int i = 0; i < SCScore.CHROMATIC.length; i++)
//		{
//			System.out.print("\t" + SCScore.CHROMATIC[i]);
//		}
//		System.out.println();
		
//		System.out.print("SCSCORE FIELDS:");
//		for(int i = 0; i < scScore.getClass().getFields().length; i++)
//		{
//			System.out.print(", \"" + scScore.getClass().getFields()[i].getName() + "\"");
//		}
//		System.out.println();
		
		for(int index = 0; index < ALL_CHORD_SIGNATURES.length / 2; index++)
		{
			String temp = ALL_CHORD_SIGNATURES[index];
			ALL_CHORD_SIGNATURES[index] = ALL_CHORD_SIGNATURES[ALL_CHORD_SIGNATURES.length - 1 - index];
			ALL_CHORD_SIGNATURES[ALL_CHORD_SIGNATURES.length - 1 - index] = temp;
		}
	}
	
	public void setParameters(int tempo, float timePerMeasure, float timePerBeat, int timeSignatureNumerator, int timeSignatureDenominator, String instrument, Scales currentScale, String chromaticRoot, int octave, String chordSignature)
	{
		this.tempo = tempo;
		this.timePerMeasure = timePerMeasure;
		this.timePerBeat = timePerBeat;
		
		scaleName = currentScale;
		if(currentScale.compareTo(Scales.MAJOR) == 0)
		{
			scale = Scale.MAJOR;
		}
		else if(currentScale.compareTo(Scales.MINOR) == 0)
		{
			scale = Scale.MINOR;
		}
		else if(currentScale.compareTo(Scales.CIRCLE_OF_FIFTHS) == 0)
		{
			scale = Scale.CIRCLE_OF_FIFTHS;
		}
		
		this.timeSignatureNumerator = timeSignatureNumerator;
		
		this.timeSignatureDenominator = timeSignatureDenominator;
		
		this.octave = octave;
		
		this.chromaticRoot = chromaticRoot;
		
		this.chordSignature = chordSignature;
		
		this.instrument = instrument;
	}
	
	public void addNotes(ArrayList<LoopNote> notes)
	{
		musicStrings.clear();
		String musicString = "";
		
		if(scale != null)
		{
			Intervals intervals = scale.getIntervals();
			intervals.setRoot(chromaticRoot);
			List<Note> scaleNotes = intervals.getNotes();
			ArrayList<String> noteString = new ArrayList<String>(); 
			for(Note n : scaleNotes)
			{
				noteString.add(Note.NOTE_NAMES_COMMON[n.getPositionInOctave()]);
			}
			noteNames = new String[noteString.size()];
			noteString.toArray(noteNames);
		}
		
		System.out.print("NOTES IN SCALE: ");
		for(int i = 0;i < noteNames.length; i++)
		{
			System.out.print(noteNames[i] + " ");
		}
		System.out.println();
		
//		isPlayingNotes = chordSignature.equalsIgnoreCase("") ? true : false;
		
		float noteIntervalDuration = (1f / timeSignatureDenominator * 120 / tempo);
		
		boolean isPercussion = false;
		if(!instrument.equalsIgnoreCase("PERCUSSION"))
		{
			System.out.println(instrument);
			player.changeTrack(0);
			player.changeInstrument(instrument);
//			for(int instrumentIndex = 0; instrumentIndex < INSTRUMENTS.length; instrumentIndex++)
//			{
//				if(INSTRUMENTS[instrumentIndex].equalsIgnoreCase(instrument))
//				{
//					scPlayer.instrument(instrumentIndex);
//					scPlayer.channel(0);
//				}
//			}
		}
		else
		{
			player.changeTrack(9);
			System.out.println(instrument);
//			scPlayer.channel(9);
			isPercussion = true;
		}
		
		musicNotesList.clear();
		for(int i = 0; i < timeSignatureNumerator; i++)
		{
			musicString = "";
			musicNotesList.add(new ArrayList<MusicNote>());
			for(int index = 0; index < notes.size(); index++)
			{
				LoopNote note = notes.get(index);
				if(i == note.getStartNoteInterval())
				{
					if(musicString != "")
					{
						musicString += "+";
					}
					
					String[] noteNamesChromatic = Note.NOTE_NAMES_COMMON;
					int rootOffset = 0;
					for(; rootOffset < Note.NOTE_NAMES_COMMON.length; rootOffset++)
					{
						if(noteNamesChromatic[rootOffset].equals(chromaticRoot))
						{	
							break;
						}
					}
					int noteNumber =  octave * 12 + rootOffset + (note.getLevel() - 1);
					
					System.out.println("Note Number: " + noteNumber);
					
					String noteName = noteNames[note.getLevel() - 1];
					float duration = (note.getEndNoteInterval() - note.getStartNoteInterval()) * noteIntervalDuration;
					
					if(!isPercussion)
					{
						Note noteFromNumber = new Note(noteNumber);
						noteFromNumber.setDuration(duration);
//						System.out.println("Musicstring from number: " + noteFromNumber.getPattern());
						
						if(!chordSignature.equalsIgnoreCase(""))
						{
							Chord chord = new Chord(noteFromNumber, Chord.chordMap.get(chordSignature));
//							System.out.println("Chord from number: " + chord.getPattern());
							musicString += chord.getPattern();
						}
						else
						{
							musicString += noteFromNumber.getPattern();
						}
					}
					else
					{
						musicString += "[" + PERCUSSION_NOTES[note.getLevel() - 1] + "]";
					}
					
					MusicNote musicNote = new MusicNote(noteName, chordSignature, octave, noteIntervalDuration, note.getLevel(), noteNumber);
					musicNotesList.get(musicNotesList.size() - 1).add(musicNote);
				}
			}
			
			if(musicString == "")
			{
				musicString = "R/" + (1f / timeSignatureDenominator);
				MusicNote musicNote = new MusicNote("R", chordSignature, octave, (1f / timeSignatureDenominator), 0, 0);
				musicNotesList.get(musicNotesList.size() - 1).add(musicNote);
			}
			
			System.out.println("Music: " + musicString);
			
			musicStrings.add(musicString);
		}
		
		System.out.println();
		
		musicChanged = true;
	}
	
	public static boolean epsilonEquals(float a, float b)
	{
		return (Math.abs(a - b) < EPSILON);
	}
	
	public void play()
	{
//		System.out.println("MusicPlayer.play(): " + System.currentTimeMillis());
		
		taskFuture = scheduledExecutorService.scheduleWithFixedDelay(
				new Runnable() {

					@Override
					public void run()
					{
//						System.out.println("MusicPlayer.play().run(): " + System.currentTimeMillis());
						
						if(isPlaying)
						{
//							System.out.println("Playing: " + System.currentTimeMillis());
//							if(isPlayingNotes)
//							{
//								player.play(getCurrentMusicString());
////								player.startNote(new Note(musicStrings.get(currentNoteIntervalIndex)));
//							}
//							else
//							{
//								player.play(getCurrentMusicString());
////								player.startChord(new Chord(musicStrings.get(currentNoteIntervalIndex)));
//							}
							if(currentNoteIntervalIndex == 0)
							{
								startTime = System.currentTimeMillis();
							}
							System.out.println("Playing: " + getCurrentMusicString());
							player.play(getCurrentMusicString());
							
//							ArrayList<Float> musicValues = new ArrayList<Float>();
//							for(int index = 0; index < musicNotesList.get(currentNoteIntervalIndex).size(); index++)
//							{
//								if(chordSignature.equalsIgnoreCase(""))
//								{
//									System.out.println("In Note Land: " + musicNotesList.get(currentNoteIntervalIndex).get(index).toString());
//									musicValues.add((float)musicNotesList.get(currentNoteIntervalIndex).get(index).getNoteNumber());
//								}
//								else
//								{
//									System.out.println("In Chord Land: " + musicNotesList.get(currentNoteIntervalIndex).get(index).toString());
//									Intervals intervals = Chord.chordMap.get(chordSignature);
//									intervals.setRoot(chromaticRoot + octave);
//									ArrayList<Note> notes = new ArrayList<Note>(intervals.getNotes());
//									
//									for(int index2 = 0; index2 < notes.size(); index2++)
//									{
//										Note note = notes.get(index2);
//										System.out.println("Octave: " + note.getOctave());
//										musicValues.add((float)note.getValue());
//									}
//								}
//							}
//							
//							float[] noteFloats = new float[musicValues.size()];
//							double loudnessFloats = 80;
//							double durationFloats = 1;
//							for(int index = 0; index < musicValues.size(); index++)
//							{
//								noteFloats[index] = musicValues.get(index);
//							}
//							
//							scPlayer.playChord(noteFloats, loudnessFloats, durationFloats);
							
							currentNoteIntervalIndex = (currentNoteIntervalIndex + 1) % musicStrings.size();
						}
						else
						{
//							System.out.println("Stopping: " + System.currentTimeMillis());
							taskFuture.cancel(false);
						}
					}}
				, 0, Math.round(timePerBeat * 1000), TimeUnit.MILLISECONDS);
	}
	
	public void togglePlayback()
	{
//		System.out.println("MusicPlayer.togglePlayback(): " + System.currentTimeMillis());
		if(!isPlaying)
		{
			isPlaying = true;
			currentNoteIntervalIndex = 0;
			play();
		}
		else
		{
			isPlaying = false;
		}
	}

	public Scale getScale()
	{
		return scale;
	}

	public void setScale(Scale scale)
	{
		this.scale = scale;
	}
	
	public void setScale(Scales currentScale)
	{
		scaleName = currentScale;
		if(currentScale.compareTo(Scales.MAJOR) == 0)
		{
			this.scale = Scale.MAJOR;
		}
		else if(currentScale.compareTo(Scales.MINOR) == 0)
		{
			this.scale = Scale.MINOR;
		}
		else if(currentScale.compareTo(Scales.CIRCLE_OF_FIFTHS) == 0)
		{
			this.scale = Scale.CIRCLE_OF_FIFTHS;
		}
	}

//	public boolean isPlayingNotes()
//	{
//		return isPlayingNotes;
//	}
//
//	public void setPlayingNotes(boolean isPlayingNotes)
//	{
//		this.isPlayingNotes = isPlayingNotes;
//	}

	public float getTimePerMeasure()
	{
		return timePerMeasure;
	}

	public void setTimePerMeasure(float timePerMeasure)
	{
		this.timePerMeasure = timePerMeasure;
	}

	public float getTimePerBeat()
	{
		return timePerBeat;
	}

	public void setTimePerBeat(float timePerBeat)
	{
		this.timePerBeat = timePerBeat;
	}

	public int getTimeSignatureNumerator()
	{
		return timeSignatureNumerator;
	}

	public void setTimeSignatureNumerator(int timeSignatureNumerator)
	{
		this.timeSignatureNumerator = timeSignatureNumerator;
	}

	public int getTimeSignatureDenominator()
	{
		return timeSignatureDenominator;
	}

	public void setTimeSignatureDenominator(int timeSignatureDenominator)
	{
		this.timeSignatureDenominator = timeSignatureDenominator;
	}

	public String getChromaticRoot()
	{
		return chromaticRoot;
	}

	public void setChromaticRoot(String chromaticRoot)
	{
		this.chromaticRoot = chromaticRoot;
	}

	public String getChordSignature()
	{
		return chordSignature;
	}

	public void setChordSignature(String chordSignature)
	{
		this.chordSignature = chordSignature;
	}

	public int getOctave()
	{
		return octave;
	}

	public void setOctave(int octave)
	{
		this.octave = octave;
	}

	public String getInstrument()
	{
		return instrument;
	}

	public void setInstrument(String instrument)
	{
		this.instrument = instrument;
	}

	public int getNextNoteIntervalIndex()
	{
		return (currentNoteIntervalIndex + 1) % timeSignatureNumerator;
	}

	public int getCurrentNoteIntervalIndex()
	{
		return currentNoteIntervalIndex;
	}
	
	public int getPreviousNoteIntervalIndex()
	{
		return (timeSignatureNumerator + currentNoteIntervalIndex - 1) % timeSignatureNumerator;
	}
	
	public int getTwoPreviousNoteIntervalIndex()
	{
		return (timeSignatureNumerator + currentNoteIntervalIndex - 2) % timeSignatureNumerator;
	}
	
	public int getThreePreviousNoteIntervalIndex()
	{
		return (timeSignatureNumerator + currentNoteIntervalIndex - 3) % timeSignatureNumerator;
	}

	public void setCurrentNoteIntervalIndex(int currentNoteIntervalIndex)
	{
		this.currentNoteIntervalIndex = currentNoteIntervalIndex;
	}
	
	public int getTempo()
	{
		return tempo;
	}
	
	public void setTempo(int tempo)
	{
		this.tempo = tempo;
	}

	public ArrayList<String> getMusicStrings()
	{
		return musicStrings;
	}

	public void setMusicStrings(ArrayList<String> musicStrings)
	{
		this.musicStrings = musicStrings;
	}
	
	public String getCurrentMusicString()
	{
		return musicStrings.get(currentNoteIntervalIndex);
	}
	
	public String getPreviousMusicString()
	{
		return musicStrings.get(currentNoteIntervalIndex - 1);
	}

	public boolean isMusicChanged()
	{
		return musicChanged;
	}

	public void setMusicChanged(boolean musicChanged)
	{
		this.musicChanged = musicChanged;
	}

	public String[] getNoteNames()
	{
		return noteNames;
	}

	public void setNoteNames(String[] noteNames)
	{
		this.noteNames = noteNames;
	}

	public long getStartTime()
	{
		return startTime;
	}

	public void setStartTime(long startTime)
	{
		this.startTime = startTime;
	}
	
	public boolean isMusicPlaying()
	{
		return isPlaying;
	}

	public Scales getScaleName()
	{
		return scaleName;
	}

	public ArrayList<ArrayList<MusicNote>> getMusicNotesList()
	{
		return musicNotesList;
	}

	public void setMusicNotesList(ArrayList<ArrayList<MusicNote>> musicNotesList)
	{
		this.musicNotesList = musicNotesList;
	}
}
