package cs8803ca.project5.music.generation;

public class MusicNote
{
	private String noteName;
	private String chordSignature;
	private int octave;
	private float duration;
	private int level;
	private boolean isRest;
	private int noteNumber;
	
	public MusicNote(String noteName, String chordSignature, int octave, float duration, int level, int noteNumber)
	{
		this.noteName = noteName;
		this.chordSignature = chordSignature;
		this.octave = octave;
		this.duration = duration;
		this.level = level;
		this.isRest = (noteName.startsWith("R"));
		this.noteNumber = noteNumber;
	}

	public String getNoteName()
	{
		return noteName;
	}

	public void setNoteName(String noteName)
	{
		this.noteName = noteName;
	}

	public String getChordSignature()
	{
		return chordSignature;
	}

	public void setChordSignature(String chordSignature)
	{
		this.chordSignature = chordSignature;
	}

	public int getOctave()
	{
		return octave;
	}

	public void setOctave(int octave)
	{
		this.octave = octave;
	}

	public float getDuration()
	{
		return duration;
	}

	public void setDuration(float duration)
	{
		this.duration = duration;
	}

	public int getLevel()
	{
		return level;
	}

	public void setLevel(int level)
	{
		this.level = level;
	}

	public boolean isRest()
	{
		return isRest;
	}

	public void setRest(boolean isRest)
	{
		this.isRest = isRest;
	}

	public int getNoteNumber()
	{
		return noteNumber;
	}

	public void setNoteNumber(int noteNumber)
	{
		this.noteNumber = noteNumber;
	}
	
	public String toString()
	{
		if(noteName.startsWith("R"))
		{
			return "Rest/" + duration;
		}
		return "" + noteName + octave + "("+ level + " : " + noteNumber +")" + chordSignature + "/" + duration;
	}
}
