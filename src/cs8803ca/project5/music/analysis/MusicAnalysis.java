package cs8803ca.project5.music.analysis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;

import org.jfugue.theory.Note;

import cs8803ca.project5.music.generation.LoopCircle;
import cs8803ca.project5.music.generation.MusicNote;
import cs8803ca.project5.music.generation.MusicPlayer;
import cs8803ca.project5.music.generation.MusicPlayer.Scales;
import processing.core.PApplet;
import processing.core.PVector;

public class MusicAnalysis
{
	private PApplet parent;
	private static MusicPlayer player1 = null;
	private static MusicPlayer player2 = null;
	private static MusicPlayer player3 = null;
	private static MusicPlayer player4 = null;
	private float chaos1;
	private float chaos2;
	private float chaos3;
	private float chaos4;
	private boolean musicChanged1;
	private boolean musicChanged2;
	private boolean musicChanged3;
	private boolean musicChanged4;
	private boolean isAnyMusicChanged;
	private HashMap<String, Integer> historicalNoteCounts;
	private float currentQuality;
	private float currentNovelty;
	private float currentSurprise;
	private float currentBeauty;
	private long changeTime;
	private float changeNovelty;
	private final float timeForBoredom = 10;
	private int noteLevelEMA;
	private final float ALPHA_NOTE_LEVEL = 0.5f;
	

	public MusicAnalysis(PApplet parent)
	{
		this.parent = parent;
		historicalNoteCounts = new HashMap<String, Integer>();
		for(String noteName : Note.NOTE_NAMES_COMMON)
		{
			historicalNoteCounts.put(noteName, 0);
		}
		noteLevelEMA = -1;
	}

	public void draw()
	{
		int changeCount = 0;
		float changeAmount = 0;
		if(player1.isMusicChanged())
		{
			musicChanged1 = true;
			changeAmount += updateNovelty(player1);
			changeCount++;
			player1.setMusicChanged(false);
		}
		
		if(player2.isMusicChanged())
		{
			musicChanged2 = true;
			changeAmount += updateNovelty(player2);
			changeCount++;
			player2.setMusicChanged(false);
		}
		
		if(player3.isMusicChanged())
		{
			musicChanged3 = true;
			changeAmount += updateNovelty(player3);
			changeCount++;
			player3.setMusicChanged(false);
		}
		
		if(player4.isMusicChanged())
		{
			musicChanged4 = true;
			changeAmount += updateNovelty(player4);
			changeCount++;
			player4.setMusicChanged(false);
		}
		
		if(musicChanged1 || musicChanged2 || musicChanged3 || musicChanged4)
		{
			isAnyMusicChanged = true;
			changeTime = System.currentTimeMillis();
			changeNovelty = changeAmount / changeCount;
		}
		
		if(player1.isMusicPlaying() || player2.isMusicPlaying() || player3.isMusicPlaying() || player4.isMusicPlaying())
		{
			currentQuality = updateQuality();
			currentNovelty = updateNovelty();
			currentNovelty = updateBoredom();
			currentSurprise = updateSurprise();
			currentBeauty = updateBeauty();
			
			chaos1 = updateChaos(player1);
			chaos2 = updateChaos(player2);
			chaos3 = updateChaos(player3);
			chaos4 = updateChaos(player4);
		}
	}
	
	public ArrayList<MusicNote> getAllMusicNotes()
	{
		ArrayList<MusicNote> notes = new ArrayList<MusicNote>();
		
		if(!player1.getMusicNotesList().isEmpty())
		{
			notes.addAll(player1.getMusicNotesList().get(player1.getPreviousNoteIntervalIndex()));
		}
		if(!player2.getMusicNotesList().isEmpty())
		{
			notes.addAll(player2.getMusicNotesList().get(player2.getPreviousNoteIntervalIndex()));
		}
		if(!player3.getMusicNotesList().isEmpty())
		{
			notes.addAll(player3.getMusicNotesList().get(player3.getPreviousNoteIntervalIndex()));
		}
		if(!player4.getMusicNotesList().isEmpty())
		{
			notes.addAll(player4.getMusicNotesList().get(player4.getPreviousNoteIntervalIndex()));
		}
		
		return notes;
	}
	
	public float updateChaos(MusicPlayer player)
	{
		float chaos = 0;
		
		//TODO FINISH
		
		return chaos;
	}
	
	public float updateSurprise()
	{
		float S = 0;
		//S is a time-varying sum of many normalized factors.
		//Difference from predicted note level
		
		ArrayList<MusicNote> notes = getAllMusicNotes();
		int divergence = 0;
		for(MusicNote note : notes)
		{
			if(noteLevelEMA == -1)
			{
				noteLevelEMA = note.getLevel();
			}
			else
			{
				divergence += Math.abs(noteLevelEMA - note.getLevel());
				noteLevelEMA = Math.round(ALPHA_NOTE_LEVEL * note.getLevel() + (1 - ALPHA_NOTE_LEVEL) * noteLevelEMA);
			}
		}
		
		int maxDivergence = 7 * notes.size();
		
		S = (float)divergence / (float)maxDivergence;
		
		System.out.println("Surprise: " + S);
		
		return S;
	}
	
	public float updateNovelty(MusicPlayer player)
	{
		if(player.getMusicNotesList().isEmpty())
		{
			return 0;
		}
		float N = 0;
		//How much did the note count grow by the last increment
		//Percentage of the population
		N = updateHistoricalNoteNovelty(player.getMusicNotesList(), true);
//		System.out.println("Update Novelty - Notes: " + player.getMusicNotesList().toString());
		
		System.out.println("Novelty for entire loop: " + N);
		
		return N;
	}
	
	public float updateNovelty(MusicPlayer player, int currentIndex)
	{
		if(player.getMusicNotesList().isEmpty())
		{
			return 0;
		}
		float N = 0;
		//How much did the note count grow by the last increment
		//Percentage of the population
		N = updateHistoricalNoteNovelty(player.getMusicNotesList().get(currentIndex));
//		System.out.println("Update Novelty - Notes: " + player.getMusicNotesList().get(currentIndex));
		
		System.out.println("Novelty for one player for one index: " + N);
		
		return N;
	}
	
	public float updateNovelty()
	{
		float N = 0;
		int changeCount = 0;
		float changeAmount = 0;
		
		if(player1 != null)
		{
			changeAmount += updateNovelty(player1, player1.getPreviousNoteIntervalIndex());
			changeCount++;
		}
		if(player2 != null)
		{
			changeAmount += updateNovelty(player2, player2.getPreviousNoteIntervalIndex());
			changeCount++;
		}
		if(player3 != null)
		{
			changeAmount += updateNovelty(player3, player3.getPreviousNoteIntervalIndex());
			changeCount++;
		}
		if(player4 != null)
		{
			changeAmount += updateNovelty(player4, player4.getPreviousNoteIntervalIndex());
			changeCount++;
		}
		
		N = changeAmount / changeCount;
		
		System.out.println("Updated Novelty all players one index: " + N);
		
		return N;
	}
	
	public float updateBoredom()
	{
		//Novelty is a time-varying sum of many normalized factors.
		//Time till last change
		float timeElapsed = (System.currentTimeMillis() - changeTime) / 1000f;
		float timeEpochs = timeElapsed / timeForBoredom;
		float novelty = (1 / (timeEpochs + 1) * (currentNovelty + changeNovelty) / 2f);
		
		System.out.println("CurrentNovelty: " + currentNovelty);
		System.out.println("ChangeNovelty: " + changeNovelty);
		System.out.println("Total Bored Novelty: " + novelty);
		
		return novelty;
	}
	
	public float updateQuality()
	{
		float Q = 0;
		//Q is normalized sum of many normalized factors.
		//1. Ordering of scales: CoF > Min > Maj > Chr
		float scaleGoodness = calculateScaleGoodness();
//		System.out.println("Scale Quality: " + scaleGoodness);
		//2. a. Ordering of chord signatures: Follows list
		float chordSignatureGoodness = calculateChordSignatureGoodness();
//		System.out.println("Chord Signature Quality: " + chordSignatureGoodness);
		//2. b. Ordering of harmony of intervals
		float noteIntervalHarmonyGoodness = calculateNoteIntervalGoodness();
//		System.out.println("Note Interval Quality: " + noteIntervalHarmonyGoodness);
		//3. Octave near the middle
		float octaveScore = calculateOctaveGoodness();
//		System.out.println("Octave Quality: " + octaveScore);
		//4. Rate of leaps
		//TODO
		//5. Time since last leap
		//TODO
		//6. Acceleration of note levels
		//TODO
		//7. Number of notes
		float noteNumberScore = calculateNoteNumberGoodness();
//		System.out.println("Note Number Quality: " + noteNumberScore);
		//8. AND MORE...
		
		//Weighted mean of normalized factors
		Q = (1 * scaleGoodness + 1 * chordSignatureGoodness + 1 * octaveScore + 1 * noteIntervalHarmonyGoodness + 1 * noteNumberScore) / 5f;
		
		System.out.println("Quality: " + Q);
		
		return Q;
	}
	
	public float updateBeauty()
	{
		//Beauty = Novelty + Quality + Surprise
		//Novelty = Amount of change
		//Quality = Order / Complexity = Positives - Negatives 
		//Surprise = Amount of change from expectation
		
		float M = (1 * currentNovelty + 1 * currentQuality + 1 * currentSurprise) / 3f;
		
		System.out.println("Beauty: " + M);
		
		//Formula is a weighted normalized sum of many normalized factors
		return M;
	}
	
	public float updateHistoricalNoteNovelty(ArrayList<MusicNote> musicNotes)
	{
		float noteCountNoveltyScore = 0;
		float populationNoveltyScore = 0;
		int comparisons = 0;
		int totalPopulationSize = 0;
		for(Entry<String, Integer> entry : historicalNoteCounts.entrySet())
		{
			totalPopulationSize += entry.getValue();
		}
		
		for(MusicNote musicNote : musicNotes)
		{
			if(musicNote.isRest())
			{
				continue;
			}
			
			int currentCount = historicalNoteCounts.get(musicNote.getNoteName());
			noteCountNoveltyScore += 1f / (currentCount + 1f);
			populationNoveltyScore += (1f - ((float)(currentCount + 1) / (float)(totalPopulationSize + 1)));
			comparisons++;
			
//			System.out.println("Total Notes Heard: " + totalPopulationSize);
//			System.out.println("Current Count: " + currentCount);
//			System.out.println("Note Count Novelty: " + (1f / (currentCount + 1f)));
//			System.out.println("Population Novelty: " + (1f - ((float)(currentCount + 1) / (float)(totalPopulationSize + 1))));
			
			historicalNoteCounts.put(musicNote.getNoteName(), currentCount + 1);
			totalPopulationSize++;
		}
		
//		System.out.println("Note Count Novelty Score: " + noteCountNoveltyScore);
//		System.out.println("Population Novelty Score: " + populationNoveltyScore);
		
		return (noteCountNoveltyScore + populationNoveltyScore) / ((comparisons + 0.5f) * 2);
	}
	
	public float updateHistoricalNoteNovelty(ArrayList<ArrayList<MusicNote>> musicNotesList, boolean dummy)
	{
		float noteCountNoveltyScore = 0;
		float populationNoveltyScore = 0;
		int comparisons = 0;
		int totalPopulationSize = 0;
		for(Entry<String, Integer> entry : historicalNoteCounts.entrySet())
		{
			totalPopulationSize += entry.getValue();
		}
		for(ArrayList<MusicNote> musicNotes: musicNotesList)
		{
			for(MusicNote musicNote : musicNotes)
			{
				if(musicNote.isRest())
				{
					continue;
				}
				
				int currentCount = historicalNoteCounts.get(musicNote.getNoteName());
				noteCountNoveltyScore += 1f / (currentCount + 1f);
				populationNoveltyScore += (1f - ((float)(currentCount + 1) / (float)(totalPopulationSize + 1)));
				comparisons++;
				
	//			System.out.println("Total Notes Heard: " + totalPopulationSize);
	//			System.out.println("Current Count: " + currentCount);
	//			System.out.println("Note Count Novelty: " + (1f / (currentCount + 1f)));
	//			System.out.println("Population Novelty: " + (1f - ((float)(currentCount + 1) / (float)(totalPopulationSize + 1))));
				
				historicalNoteCounts.put(musicNote.getNoteName(), currentCount + 1);
				totalPopulationSize++;
			}
		}
		
//		System.out.println("Note Count Novelty Score: " + noteCountNoveltyScore);
//		System.out.println("Population Novelty Score: " + populationNoveltyScore);
		
		return (noteCountNoveltyScore + populationNoveltyScore) / ((comparisons + 0.5f) * 2);
	}
	
	public float calculateScaleGoodness()
	{
		float score = 0f;
		
		if(player1.getScaleName().compareTo(Scales.CIRCLE_OF_FIFTHS) == 0)
		{
			score += 4;
		}
		else if(player1.getScaleName().compareTo(Scales.MAJOR) == 0)
		{
			score += 3;
		}
		else if(player1.getScaleName().compareTo(Scales.MINOR) == 0)
		{
			score += 2;
		}
		else if(player1.getScaleName().compareTo(Scales.CHROMATIC) == 0)
		{
			score += 1;
		}
		
		if(player2.getScaleName().compareTo(Scales.CIRCLE_OF_FIFTHS) == 0)
		{
			score += 4;
		}
		else if(player2.getScaleName().compareTo(Scales.MAJOR) == 0)
		{
			score += 3;
		}
		else if(player2.getScaleName().compareTo(Scales.MINOR) == 0)
		{
			score += 2;
		}
		else if(player2.getScaleName().compareTo(Scales.CHROMATIC) == 0)
		{
			score += 1;
		}
		
		if(player3.getScaleName().compareTo(Scales.CIRCLE_OF_FIFTHS) == 0)
		{
			score += 4;
		}
		else if(player3.getScaleName().compareTo(Scales.MAJOR) == 0)
		{
			score += 3;
		}
		else if(player3.getScaleName().compareTo(Scales.MINOR) == 0)
		{
			score += 2;
		}
		else if(player3.getScaleName().compareTo(Scales.CHROMATIC) == 0)
		{
			score += 1;
		}
		
		if(player4.getScaleName().compareTo(Scales.CIRCLE_OF_FIFTHS) == 0)
		{
			score += 4;
		}
		else if(player4.getScaleName().compareTo(Scales.MAJOR) == 0)
		{
			score += 3;
		}
		else if(player4.getScaleName().compareTo(Scales.MINOR) == 0)
		{
			score += 2;
		}
		else if(player4.getScaleName().compareTo(Scales.CHROMATIC) == 0)
		{
			score += 1;
		}
		
		score = score / (4f * 4f); //4 MusicPlayers 
		
		return score;
	}
	
	public float calculateChordSignatureGoodness()
	{
		float score = 0;
		float score1 = 0, score2 = 0, score3 = 0, score4 = 0;
		ArrayList<String> chordList = new ArrayList<String>(Arrays.asList(MusicPlayer.ALL_CHORD_SIGNATURES));
		chordList.add(0, "");
		for(int i = 0; i < chordList.size(); i++)
		{
			if(player1.getChordSignature().equalsIgnoreCase(chordList.get(i)))
			{
				score1 = chordList.size() - i;
			}
			
			if(player2.getChordSignature().equalsIgnoreCase(chordList.get(i)))
			{
				score2 = chordList.size() - i;
			}
			
			if(player3.getChordSignature().equalsIgnoreCase(chordList.get(i)))
			{
				score3 = chordList.size() - i;
			}
			
			if(player4.getChordSignature().equalsIgnoreCase(chordList.get(i)))
			{
				score4 = chordList.size() - i;
			}
		}
		
		score = (score1 + score2 + score3 + score4) / chordList.size() / 4f;
		
		return score;
	}
	
	public float calculateNoteIntervalGoodness()
	{
		float score = 0;
		float score1 = 0, score2 = 0, score3 = 0, score4 = 0;
		int comparisons = 0;
		int activePlayers = 0;
		if(!player1.getMusicNotesList().isEmpty())
		{
			activePlayers++;
			ArrayList<MusicNote> musicNotes1 = player1.getMusicNotesList().get(player1.getPreviousNoteIntervalIndex());
			if(musicNotes1.size() == 1)
			{
				if(!musicNotes1.get(0).isRest())
				{
					score1 += 1f;
				}
				else
				{
					score1 += 0;
				}
				
				comparisons = 1;
			}
			else if(musicNotes1.size() == 0)
			{
				score1 += 0;
				comparisons = 0;
			}
			else
			{
				comparisons = Math.round(musicNotes1.size() * (musicNotes1.size() - 1) / 2f);
				for(int i = 0; i < musicNotes1.size() - 1; i++)
				{
					for(int j = i + 1; j < musicNotes1.size(); j++)
					{
						score1 += singleIntervalGoodness(musicNotes1.get(i).getLevel(), musicNotes1.get(j).getLevel());
					}
				}
			}
			
			score1 = score1 / comparisons;
		}
		
		comparisons = 0;
		if(!player2.getMusicNotesList().isEmpty())
		{
			activePlayers++;
			ArrayList<MusicNote> musicNotes2 = player2.getMusicNotesList().get(player2.getPreviousNoteIntervalIndex());
			if(musicNotes2.size() == 1)
			{
				if(!musicNotes2.get(0).isRest())
				{
					score2 += 1f;
				}
				else
				{
					score2 += 0;
				}
				
				comparisons = 1;
			}
			else if(musicNotes2.size() == 0)
			{
				score2 += 0;
				comparisons = 0;
			}
			else
			{
				comparisons = Math.round(musicNotes2.size() * (musicNotes2.size() - 1) / 2f);
				for(int i = 0; i < musicNotes2.size() - 1; i++)
				{
					for(int j = i + 1; j < musicNotes2.size(); j++)
					{
						score2 += singleIntervalGoodness(musicNotes2.get(i).getLevel(), musicNotes2.get(j).getLevel());
					}
				}
			}
			
			score2 = score2 / comparisons;
		}
		
		comparisons = 0;
		if(!player3.getMusicNotesList().isEmpty())
		{
			activePlayers++;
			ArrayList<MusicNote> musicNotes3 = player3.getMusicNotesList().get(player3.getPreviousNoteIntervalIndex());
			if(musicNotes3.size() == 1)
			{
				if(!musicNotes3.get(0).isRest())
				{
					score3 += 1f;
				}
				else
				{
					score3 += 0;
				}
				
				comparisons = 1;
			}
			else if(musicNotes3.size() == 0)
			{
				score3 += 0;
				comparisons = 0;
			}
			else
			{
				comparisons = Math.round(musicNotes3.size() * (musicNotes3.size() - 1) / 2f);
				for(int i = 0; i < musicNotes3.size() - 1; i++)
				{
					for(int j = i + 1; j < musicNotes3.size(); j++)
					{
						score3 += singleIntervalGoodness(musicNotes3.get(i).getLevel(), musicNotes3.get(j).getLevel());
					}
				}
			}
			
			score3 = score3 / comparisons;
		}
		
		comparisons = 0;
		if(!player4.getMusicNotesList().isEmpty())
		{
			activePlayers++;
			ArrayList<MusicNote> musicNotes4 = player4.getMusicNotesList().get(player4.getPreviousNoteIntervalIndex());
			if(musicNotes4.size() == 1)
			{
				if(!musicNotes4.get(0).isRest())
				{
					score4 += 1f;
				}
				else
				{
					score4 += 0;
				}
				
				comparisons = 1;
			}
			else if(musicNotes4.size() == 0)
			{
				score4 += 0;
				comparisons = 0;
			}
			else
			{
				comparisons = Math.round(musicNotes4.size() * (musicNotes4.size() - 1) / 2f);
				for(int i = 0; i < musicNotes4.size() - 1; i++)
				{
					for(int j = i + 1; j < musicNotes4.size(); j++)
					{
						score4 += singleIntervalGoodness(musicNotes4.get(i).getLevel(), musicNotes4.get(j).getLevel());
					}
				}
			}
			
			score4 = score4 / comparisons;
		}
		
		if(activePlayers == 0)
		{
			return 0;
		}
		
		score = (score1 + score2 + score3 + score4) / activePlayers;
		
		return score;
	}
	
	public float singleIntervalGoodness(int noteLevel1, int noteLevel2)
	{
		if(Math.abs(noteLevel1 - noteLevel2) == 0)
		{
			return 1;
		}
		else if(Math.abs(noteLevel1 - noteLevel2) == 1)
		{
			return 0.2f;
		}
		else if(Math.abs(noteLevel1 - noteLevel2) == 2)
		{
			return 0.5f;
		}
		else if(Math.abs(noteLevel1 - noteLevel2) == 3)
		{
			return 0.3f;
		}
		else if(Math.abs(noteLevel1 - noteLevel2) == 4)
		{
			return 0.4f;
		}
		else if(Math.abs(noteLevel1 - noteLevel2) == 5)
		{
			return 0.7f;
		}
		else if(Math.abs(noteLevel1 - noteLevel2) == 6)
		{
			return 0.4f;
		}
		else if(Math.abs(noteLevel1 - noteLevel2) == 7)
		{
			return 0.8f;
		}
		else if(Math.abs(noteLevel1 - noteLevel2) == 8)
		{
			return 0.5f;
		}
		else if(Math.abs(noteLevel1 - noteLevel2) == 9)
		{
			return 0.6f;
		}
		else if(Math.abs(noteLevel1 - noteLevel2) == 10)
		{
			return 0.6f;
		}
		else if(Math.abs(noteLevel1 - noteLevel2) == 11)
		{
			return 0.5f;
		}
		else if(Math.abs(noteLevel1 - noteLevel2) == 12)
		{
			return 0.9f;
		}
		
		return 0;
	}
	
	public float calculateOctaveGoodness()
	{
		float score = 0;
		float score1, score2, score3, score4;
		
		score1 = Math.abs(player1.getOctave() - 5) / 5;
		score2 = Math.abs(player2.getOctave() - 5) / 5;
		score3 = Math.abs(player3.getOctave() - 5) / 5;
		score4 = Math.abs(player4.getOctave() - 5) / 5;
		
		score = 1 - (score1 + score2 + score3 + score4) / 4f;
		
		return score;
	}
	
	public float calculateNoteNumberGoodness()
	{
		int noteCount1 = 0;
		for(ArrayList<MusicNote> musicNotes : player1.getMusicNotesList())
		{
			for(MusicNote musicNote : musicNotes)
			{
				if(musicNote.isRest())
				{
					continue;
				}
				noteCount1++;
			}
		}
		int noteCount2 = 0;
		for(ArrayList<MusicNote> musicNotes : player2.getMusicNotesList())
		{
			for(MusicNote musicNote : musicNotes)
			{
				if(musicNote.isRest())
				{
					continue;
				}
				noteCount2++;
			}
		}
		int noteCount3 = 0;
		for(ArrayList<MusicNote> musicNotes : player3.getMusicNotesList())
		{
			for(MusicNote musicNote : musicNotes)
			{
				if(musicNote.isRest())
				{
					continue;
				}
				noteCount3++;
			}
		}
		int noteCount4 = 0;
		for(ArrayList<MusicNote> musicNotes : player4.getMusicNotesList())
		{
			for(MusicNote musicNote : musicNotes)
			{
				if(musicNote.isRest())
				{
					continue;
				}
				noteCount4++;
			}
		}
		
		float score1, score2, score3, score4;
		
//		System.out.println("Note1Count: " + ((float)noteCount1 / (player1.getTimeSignatureNumerator() * 7f)));
//		System.out.println("Note2Count: " + ((float)noteCount2 / (player2.getTimeSignatureNumerator() * 7f)));
//		System.out.println("Note3Count: " + ((float)noteCount3 / (player3.getTimeSignatureNumerator() * 7f)));
//		System.out.println("Note4Count: " + ((float)noteCount4 / (player4.getTimeSignatureNumerator() * 7f)));
		
		score1 = (float)noteCount1 / (player1.getTimeSignatureNumerator() * 7f);//Max levels = 7 , i.e. 1 to 7
		score2 = (float)noteCount2 / (player2.getTimeSignatureNumerator() * 7f);
		score3 = (float)noteCount3 / (player3.getTimeSignatureNumerator() * 7f);
		score4 = (float)noteCount4 / (player4.getTimeSignatureNumerator() * 7f);
		
//		System.out.println("Final Note Count Score: " + (1f - (score1 + score2 + score3 + score4) / 4f));
		
		return (1f - (score1 + score2 + score3 + score4) / 4f);
	}
	
	public PVector convertmusicNotesToPosition(ArrayList<MusicNote> musicNotes, MusicPlayer player, float totalLimbLength, float startAngle, float endAngle)
	{
		ArrayList<Integer> levels = new ArrayList<Integer>();
		boolean isRest = true;
		
		for(MusicNote musicNote : musicNotes)
		{
			if(musicNote.isRest())
			{
				continue;
			}
			isRest = false;
			levels.add(musicNote.getLevel());
		}
		
		if(isRest)
		{
			return null;
		}
		
		int level = averageLevel(levels);
		int maxLevel = 8;
		
		float angle = (float)(Math.PI / 2) + parent.random(startAngle, endAngle);
		float radius = totalLimbLength / 3f + (2 * totalLimbLength / 3f * level / maxLevel);
		
		return new PVector(radius * PApplet.cos(angle), radius * PApplet.sin(angle));
	}
	
	public int averageLevel(ArrayList<Integer>levels)
	{
		float sum = 0f;
		for(Integer level : levels)
		{
			sum += level;
		}
		
		return Math.round(sum / levels.size());
	}

	public static MusicPlayer getPlayer1()
	{
		return player1;
	}

	public static void setPlayer1(LoopCircle circle)
	{
		player1 = circle.getPlayer();
	}

	public static MusicPlayer getPlayer2()
	{
		return player2;
	}

	public static void setPlayer2(LoopCircle circle)
	{
		player2 = circle.getPlayer();
	}

	public static MusicPlayer getPlayer3()
	{
		return player3;
	}

	public static void setPlayer3(LoopCircle circle)
	{
		player3 = circle.getPlayer();
	}

	public static MusicPlayer getPlayer4()
	{
		return player4;
	}

	public static void setPlayer4(LoopCircle circle)
	{
		player4 = circle.getPlayer();
	}

	public float getChaos1()
	{
		return chaos1;
	}

	public void setChaos1(float chaos1)
	{
		this.chaos1 = chaos1;
	}

	public float getChaos2()
	{
		return chaos2;
	}

	public void setChaos2(float chaos2)
	{
		this.chaos2 = chaos2;
	}

	public float getChaos3()
	{
		return chaos3;
	}

	public void setChaos3(float chaos3)
	{
		this.chaos3 = chaos3;
	}

	public float getChaos4()
	{
		return chaos4;
	}

	public void setChaos4(float chaos4)
	{
		this.chaos4 = chaos4;
	}

	public ArrayList<String> getMusicStrings1()
	{
		return player1.getMusicStrings();
	}

	public ArrayList<String> getMusicStrings2()
	{
		return player2.getMusicStrings();
	}

	public ArrayList<String> getMusicStrings3()
	{
		return player3.getMusicStrings();
	}

	public ArrayList<String> getMusicStrings4()
	{
		return player4.getMusicStrings();
	}
	
	public int getCurrentNoteIndex1()
	{
		return player1.getCurrentNoteIntervalIndex();
	}
	
	public int getCurrentNoteIndex2()
	{
		return player2.getCurrentNoteIntervalIndex();
	}
	
	public int getCurrentNoteIndex3()
	{
		return player3.getCurrentNoteIntervalIndex();
	}
	
	public int getCurrentNoteIndex4()
	{
		return player4.getCurrentNoteIntervalIndex();
	}

	public boolean isMusicChanged1()
	{
		return musicChanged1;
	}

	public void setMusicChanged1(boolean musicChanged1)
	{
		this.musicChanged1 = musicChanged1;
	}

	public boolean isMusicChanged2()
	{
		return musicChanged2;
	}

	public void setMusicChanged2(boolean musicChanged2)
	{
		this.musicChanged2 = musicChanged2;
	}

	public boolean isMusicChanged3()
	{
		return musicChanged3;
	}

	public void setMusicChanged3(boolean musicChanged3)
	{
		this.musicChanged3 = musicChanged3;
	}

	public boolean isMusicChanged4()
	{
		return musicChanged4;
	}

	public void setMusicChanged4(boolean musicChanged4)
	{
		this.musicChanged4 = musicChanged4;
	}

	public float getCurrentQuality()
	{
		return currentQuality;
	}

	public void setCurrentQuality(float currentQuality)
	{
		this.currentQuality = currentQuality;
	}

	public float getCurrentNovelty()
	{
		return currentNovelty;
	}

	public void setCurrentNovelty(float currentNovelty)
	{
		this.currentNovelty = currentNovelty;
	}

	public float getCurrentSurprise()
	{
		return currentSurprise;
	}

	public void setCurrentSurprise(float currentSurprise)
	{
		this.currentSurprise = currentSurprise;
	}

	public float getCurrentBeauty()
	{
		return currentBeauty;
	}

	public void setCurrentBeauty(float currentBeauty)
	{
		this.currentBeauty = currentBeauty;
	}

	public boolean isAnyMusicChanged()
	{
		return isAnyMusicChanged;
	}

	public void setAnyMusicChanged(boolean isAnyMusicChanged)
	{
		this.isAnyMusicChanged = isAnyMusicChanged;
	}
}
