package cs8803ca.project5.visualization;

import java.util.ArrayList;
import java.util.Random;

import cs8803ca.project5.shared.ColorSpaces;
import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;

public class Visualization
{
	private PApplet parent;
	private Random rand;
	private Marionette puppet;
	private PImage bed;
	private ColorSpaces colorSpaces;
	private boolean usingLAB;
	private float hueOffset;
	private float hueOffset2;
	private ArrayList<Cloud> clouds;
	private int cloudCount;
	private ArrayList<PVector> cloudPositions;
	private int color;
	private float surprise;
	private float novelty;
	private float quality;
	private float beauty;
	private float lScaled;
	private float aScaled;
	private float bScaled;
	private float bScaled2;
	private float cScaled;
	private float hScaled;
	private float hScaled2;
	
	public Visualization(PApplet parent)
	{
		this.parent = parent;
		rand = new Random();
		puppet = new Marionette(this.parent);
		bed = this.parent.loadImage("data/bed.png");
		colorSpaces = new ColorSpaces(parent);
		usingLAB = false;
		createClouds();
	}
	
	public void draw()
	{
		//Draw background and assign fill colors to stuff
		computeColorScheme();
		
		//Draw bed
		drawBed();
		
		//Draw puppet
		puppet.draw();
		
		//Draw clouds
		drawClouds();
	}
	
	public void computeColorScheme()
	{
		if(puppet.getAnalysis().isAnyMusicChanged())
		{
			hueOffset = parent.random(0, 360);
			System.out.println("Hue Offset: " + hueOffset);
			
			for(Cloud cloud : clouds)
			{
				cloud.calculateHOffset();
			}
			
			puppet.getAnalysis().setAnyMusicChanged(false);
		}
		
		//Draw background according to music.
		surprise = puppet.getAnalysis().getCurrentSurprise();
		novelty = puppet.getAnalysis().getCurrentNovelty();
		quality = puppet.getAnalysis().getCurrentQuality();
		beauty = puppet.getAnalysis().getCurrentBeauty();
		lScaled = 100 * surprise;
		aScaled = 255 * quality - 128;
		bScaled = 255 * novelty - 128;
		bScaled2 = 255 * beauty - 128;
		cScaled = (200 * novelty <= 100f) ? 200 * novelty : 100 * novelty;
//		System.out.println("CSCALED: " + cScaled);
		hScaled = (hueOffset + 360 * (quality)) > 360 ? (hueOffset + 360 * (quality) - 360) : (hueOffset + 360 * (quality));
//		System.out.println("HSCALED: " + hScaled);
		hScaled2 = (hueOffset + 360 * (beauty)) > 360 ? (hueOffset + 360 * (beauty) - 360) : (hueOffset + 360 * (beauty));
//		System.out.println("HSCALED2: " + hScaled2);
		if(usingLAB)
		{
			color = colorSpaces.LABtoColor(lScaled, aScaled, bScaled);
			parent.background(color);
			puppet.setBodyColor(color);
		}
		else
		{
			color = colorSpaces.LCHtoColor(lScaled, cScaled, hScaled);
			parent.background(color);
			puppet.setBodyColor(color);
		}
	}
	
	public void createClouds()
	{
		cloudCount = rand.nextInt(10);
		if(clouds == null)
		{
			clouds = new ArrayList<Cloud>();
		}
		else
		{
			clouds.clear();
		}
		
		for(int i = 0; i < cloudCount; i++)
		{
			PVector position = new PVector();
			clouds.add(new Cloud(this.parent.loadImage("data/cloud.png"), new PVector(), colorSpaces, parent));
			boolean drawOnLeft = rand.nextBoolean();
			if(drawOnLeft)
			{
				position.x = parent.random(clouds.get(i).getWidth() / 2f, parent.width / 3f - clouds.get(i).getWidth());
				position.y = parent.random(0, parent.height - clouds.get(i).getHeight());
			}
			else
			{
				position.x = parent.random(2 * parent.width / 3f + clouds.get(i).getWidth() / 2f, parent.width - clouds.get(i).getWidth());
				position.y = parent.random(0, parent.height - clouds.get(i).getHeight());
			}
			clouds.get(i).setPosition(position);
		}
	}
	
	public void drawClouds()
	{
		for(Cloud cloud : clouds)
		{
			if(usingLAB)
			{
				cloud.setLAB(lScaled, aScaled, bScaled);
			}
			else
			{
				cloud.setLCH(lScaled, cScaled, hScaled);
			}
			cloud.setMusicParams(novelty, surprise, quality, beauty);
			cloud.draw(usingLAB);
		}
	}
	
	public void drawBed()
	{
		//Draw Bed
//		float bodySize = puppet.getBody().characteristicSize();
		float bedWidth = 1f * parent.width / 3f;
//		float bedHeight = 2f * bodySize;
		float x1 = parent.width / 2f - bedWidth / 2f;
		float y1 = 0;
		float x2 = parent.width / 2f + bedWidth / 2f;
		float y2 = parent.displayHeight;
//		float x = parent.width / 2f - bedWidth / 2f;
//		float y = 0;
		float width = Math.abs(x2 - x1);
		float height = Math.abs(y2 - y1);
//		bed.resize(Math.round(bedWidth), Math.round(bedHeight));
		parent.image(bed, x1, y1, width, height);
	}
	
	public void run()
	{
		puppet.run();
	}

	public boolean isUsingLAB()
	{
		return usingLAB;
	}

	public void setUsingLAB(boolean usingLAB)
	{
		this.usingLAB = usingLAB;
	}
	
	public void setDebugPoints(boolean debug)
	{
		puppet.setDebugEndPoints(debug);
	}
	
	public boolean isDebugPoints()
	{
		return puppet.isDebugEndPoints();
	}
}
