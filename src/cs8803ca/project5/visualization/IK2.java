package cs8803ca.project5.visualization;

import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PVector;

/**
 * Basic inverse kinematics code rewritten for processing from Keith Peters of
 * bit-101. The idea is to replicate the motion of your shoulder, upper arm,
 * elbow, lower arm, and hand in two dimensions. The angular value of the
 * "shoulder" and "elbow" will be written to corresponding servo motors that are
 * being controlled by Arduino.
 * 
 * <A
 * href="http://idblab.blogspot.com/2008/11/robotic-arm-drawing-machine.html">
 * Video of the arm working</A> <A href="http://idblab.blogspot.com">Check out
 * my blog</A>
 * 
 * @author Matt Richards
 * 
 */
public class IK2
{
	PApplet parent;
	PVector elbowPoint;
	PVector actualEndPoint;
	
	public IK2(PApplet parent)
	{
		this.parent = parent;
	}
	
	void solveIKTwoLink(PVector targetPoint, PVector originPoint, float upperArmLength, float lowerArmLength)
	{
		float dx = targetPoint.x - originPoint.x;
		float dy = targetPoint.y - originPoint.y;
		
//		System.out.println("dx: " + dx + ", dy: " + dy);
		
		float distance = PApplet.sqrt(dx * dx + dy * dy);
//		System.out.println("distance: " + distance);
		
		float sx = originPoint.x, sy = originPoint.y, ex, ey, hx, hy;

		float c = PApplet.min(distance, upperArmLength + lowerArmLength);
//		System.out.println("c: " + c);

		float B = PApplet.acos((lowerArmLength * lowerArmLength - upperArmLength * upperArmLength - c * c) / (-2 * upperArmLength * c));
//		System.out.println("BCalc: " + (lowerArmLength * lowerArmLength - upperArmLength * upperArmLength - c * c) / (-2 * upperArmLength * c));
//		System.out.println("B: " + PApplet.degrees(B));
		float C = PApplet.acos((c * c - upperArmLength * upperArmLength - lowerArmLength * lowerArmLength) / (-2 * upperArmLength * lowerArmLength));
//		System.out.println("CCalc: " + (c * c - upperArmLength * upperArmLength - lowerArmLength * lowerArmLength) / (-2 * upperArmLength * lowerArmLength));
//		System.out.println("C: " + PApplet.degrees(C));
		
		float D = PApplet.atan2(dy, dx);
//		System.out.println("DCalc: " + PApplet.atan2(dy, dx));
//		System.out.println("D: " + PApplet.degrees(D));
		float E = D + B + PConstants.PI + C;

		ex = ((PApplet.cos(E) * upperArmLength)) + sx;
		ey = ((PApplet.sin(E) * upperArmLength)) + sy;
//		System.out.println("ex: " + ex + ", ey: " + ey);
//		System.out.println("UpperArm Angle=  " + PApplet.degrees(E));
		elbowPoint = new PVector(ex, ey);

		hx = ((PApplet.cos(D + B) * lowerArmLength)) + ex;
		hy = ((PApplet.sin(D + B) * lowerArmLength)) + ey;
//		System.out.println("hx: " + hx + ", hy: " + hy);
//		System.out.println("LowerArm Angle=  " + PApplet.degrees((D + B)));
		actualEndPoint = new PVector(hx, hy);
	}

	public PVector getElbowPoint()
	{
		return elbowPoint;
	}

	public PVector getActualEndPoint()
	{
		return actualEndPoint;
	}
}
