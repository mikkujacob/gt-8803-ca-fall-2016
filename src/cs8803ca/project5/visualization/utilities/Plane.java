package cs8803ca.project5.visualization.utilities;

import processing.core.PVector;

public class Plane
{
	public Plane(PVector origin, PVector norm)
	{
		this.origin = origin;
		this.norm = norm;
	}

	public PVector origin;
	public PVector norm;
}
