package cs8803ca.project5.visualization.utilities;

import java.util.HashMap;
import java.util.Map;
import java.io.Serializable;

import processing.core.*;

public class Body extends SkeletalData implements Serializable, Cloneable
{

	/**
	 * Serial Version UID - DO NOT CHANGE PLEASE!!!
	 */
	private static final long serialVersionUID = 2610605569934643630L;

	public void set(JIDX iJidx, PVector iPvector)
	{
		put(iJidx, iPvector);
	}

	@SuppressWarnings("deprecation")
	public void initialize(HashMap<JIDX, PVector> body, int userId, float timestamp)
	{
		setTimestamp(timestamp);

		// note: jointPos.get() is copying each PVector as they are
		// call-by-reference

		// Get Skeleton Coordinates
		set(JIDX.HEAD, body.get(JIDX.HEAD).get());
		set(JIDX.NECK, body.get(JIDX.NECK).get());
		set(JIDX.LEFT_SHOULDER, body.get(JIDX.LEFT_SHOULDER).get());
		set(JIDX.LEFT_ELBOW, body.get(JIDX.LEFT_ELBOW).get());
		set(JIDX.LEFT_HAND, body.get(JIDX.LEFT_HAND).get());
		set(JIDX.LEFT_FINGERTIP, body.get(JIDX.LEFT_FINGERTIP).get());
		set(JIDX.RIGHT_SHOULDER, body.get(JIDX.RIGHT_SHOULDER).get());
		set(JIDX.RIGHT_ELBOW, body.get(JIDX.RIGHT_ELBOW).get());
		set(JIDX.RIGHT_HAND, body.get(JIDX.RIGHT_HAND).get());
		set(JIDX.RIGHT_FINGERTIP, body.get(JIDX.RIGHT_FINGERTIP).get());
		set(JIDX.TORSO, body.get(JIDX.TORSO).get());
		set(JIDX.LEFT_HIP, body.get(JIDX.LEFT_HIP).get());
		set(JIDX.LEFT_KNEE, body.get(JIDX.LEFT_KNEE).get());
		set(JIDX.LEFT_FOOT, body.get(JIDX.LEFT_FOOT).get());
		set(JIDX.RIGHT_HIP, body.get(JIDX.RIGHT_HIP).get());
		set(JIDX.RIGHT_KNEE, body.get(JIDX.RIGHT_KNEE).get());
		set(JIDX.RIGHT_FOOT, body.get(JIDX.RIGHT_FOOT).get());
	}

	public PVector[] localBasis()
	{
		PVector localFwddir = PVecUtilities.orthonormalization(orientation(),
				PVecUtilities.UPVEC);
		PVector localSidedir = PVecUtilities.UPVEC.cross(localFwddir);
		PVector[] localBasis =
		{ localSidedir, PVecUtilities.UPVEC, localFwddir };
		return localBasis;
	}

	public PVector center()
	{
		return get(JIDX.TORSO);
	}

	public Body transformed(PVector[] sourceBasis, PVector[] targetBasis,
			PVector targetCenter)
	{
		Body transformedPose = new Body();
		PVector center = center();
		for (JIDX jidx : JIDX.ALL_JIDX)
		{
			if (JIDX.TORSO == jidx)
			{
				transformedPose.set(JIDX.TORSO, targetCenter);
			}
			else
			{
				PVector jointPosition = get(jidx);
				PVector radiusVector = PVector.sub(jointPosition, center);
				float[] dissolution = PVecUtilities.dissolve(radiusVector,
						sourceBasis);
				PVector normalizedPosition = PVecUtilities.assemble(
						targetBasis, dissolution);
				transformedPose.set(jidx,
						PVector.add(targetCenter, normalizedPosition));
			}
		}
		transformedPose.setTimestamp(getTimestamp());
		return transformedPose;
	}

	public Body translated(PVector targetCenter)
	{
		Body translatedPose = new Body();
		PVector center = center();
		for (JIDX jidx : JIDX.ALL_JIDX)
		{
			if (JIDX.TORSO == jidx)
			{
				translatedPose.set(jidx, targetCenter);
			}
			else
			{
				PVector jointPosition = get(jidx);
				PVector radiusVector = PVector.sub(jointPosition, center);
				translatedPose.set(jidx,
						PVector.add(targetCenter, radiusVector));
			}
		}
		return translatedPose;
	}

	public PVector orientation()
	{
		return PVecUtilities.normTo3Pt(get(JIDX.TORSO),
				get(JIDX.LEFT_SHOULDER), get(JIDX.RIGHT_SHOULDER));
	}

	public PVector upperUpvec()
	{
		PVector upvec = PVector.sub(get(JIDX.NECK), get(JIDX.TORSO));
		upvec.div(upvec.mag());
		return upvec;
	}

	public PVector lowerUpvec()
	{
		return PVecUtilities.UPVEC;
	}

	@Override
	public Object clone()
	{
		Body cln = new Body();

		cln.setTimestamp(getTimestamp());

		for (JIDX jidx : keySet())
		{
			cln.set(jidx, PVecUtilities.clone(get(jidx)));
		}

		return cln;
	}

	// interpolates a position of the body from the positions and their
	// coefficients given in parameter
	public static Body interpolate(Body body1, Body body2, float coefficient)
	{

		// position of the body to be returned
		Body body = new Body();

		// to store the part of the body which position is being computed
		JIDX iJidx;

		// browses all parts of the body
		for (Map.Entry<JIDX, PVector> joints_iterator : body1.entrySet())
		{

			// part of the body to interpolate
			iJidx = joints_iterator.getKey();

			// interpolates the position of this part of the body and stores it
			body.set(iJidx, PVector.lerp(joints_iterator.getValue(),
					body2.get(iJidx), coefficient));

		}

		// the interpolated position of the body is returned
		return body;

	}

	public boolean isSimilar(Body other, float relativeDeviationTolerance)
	{
		PVector centerA = get(JIDX.TORSO);
		PVector centerB = other.get(JIDX.TORSO);
		for (JIDX jidx : JIDX.ALL_JIDX)
		{
			if (JIDX.TORSO == jidx)
				continue;
			PVector radialA = PVector.sub(get(jidx), centerA);
			PVector radialB = PVector.sub(other.get(jidx), centerB);
			PVector deviation = PVector.sub(radialB, radialA);
			float referenceMag = Math.max(radialA.mag(), radialB.mag());
			float deviationMag = deviation.mag();
			if (deviationMag > referenceMag * relativeDeviationTolerance)
			{
				return false;
			}
		}
		return true;
	}
	
	public float difference(Body other)
	{
		PVector centerA = get(JIDX.TORSO);
		PVector centerB = other.get(JIDX.TORSO);
		float deviationMag = 0f;
		for (JIDX jidx : JIDX.ALL_JIDX)
		{
			if (JIDX.TORSO == jidx)
				continue;
			PVector radialA = PVector.sub(get(jidx), centerA);
			PVector radialB = PVector.sub(other.get(jidx), centerB);
			PVector deviation = PVector.sub(radialB, radialA);
			deviationMag += Math.abs(deviation.mag());
		}
		return deviationMag;
	}

	public float characteristicSize()
	{
		float sumsize = 0.0f;

		for (Pair<JIDX, JIDX> jidxpair : LIDX.LIMB_ENDS().values())
		{
			sumsize += PVector.sub(get(jidxpair.second), get(jidxpair.first)).mag();
		}

		return sumsize / LIDX.LIMB_ENDS().size();
	}

	public Body scale(float factor)
	{
		Body scaled = new Body();

		for (JIDX jidx : JIDX.ALL_JIDX)
		{
			scaled.set(jidx, PVector.mult(get(jidx), factor));
		}

		scaled.setTimestamp(getTimestamp());
		return scaled;
	}
	
	public String toString()
	{
		String text = "[Body: [Timestamp: " + getTimestamp() + ", Joints: [";
		JIDX[] jointIndices = JIDX.ALL_JIDX;
		text += "" + jointIndices[0].toString() + ": " + get(jointIndices[0]).toString();
		for(int index = 1; index < jointIndices.length; index++)
		{
			text += ", " + jointIndices[index].toString() + ": " + get(jointIndices[index]).toString();
		}
		
		text += "]]]";
		return text;
	}
	
	public static Body getIdealUserPose()
	{
		Body idealUserPose = new Body();
		//Set Skeleton Coordinates for ideal user Body
		idealUserPose.set(JIDX.TORSO,new PVector(56.47932f, 240.32893f, 1834.6368f));
		idealUserPose.set(JIDX.HEAD,new PVector(61.86306f, 800.0279f, idealUserPose.center().z * 1.5f));
		idealUserPose.set(JIDX.NECK,new PVector(49.31178f, 482.05545f, idealUserPose.center().z * 1.5f));
		idealUserPose.set(JIDX.LEFT_SHOULDER,new PVector(-117.19889f, 473.39166f, idealUserPose.center().z * 1.5f));
		idealUserPose.set(JIDX.LEFT_ELBOW,new PVector(-234.26807f, 214.76712f, idealUserPose.center().z * 1.5f));
		idealUserPose.set(JIDX.LEFT_HAND,new PVector(-502.1734f, 154.27269f, idealUserPose.center().z * 1.5f));
		idealUserPose.set(JIDX.LEFT_FINGERTIP,new PVector(61.86306f, 800.0279f, idealUserPose.center().z * 1.5f));
		idealUserPose.set(JIDX.RIGHT_SHOULDER,new PVector(215.82245f, 490.7192f, idealUserPose.center().z * 1.5f));
		idealUserPose.set(JIDX.RIGHT_ELBOW,new PVector(380.55743f, 234.44539f, idealUserPose.center().z * 1.5f));
		idealUserPose.set(JIDX.RIGHT_HAND,new PVector(641.58655f, 210.31554f, idealUserPose.center().z * 1.5f));
		idealUserPose.set(JIDX.RIGHT_FINGERTIP,new PVector(61.86306f, 800.0279f, idealUserPose.center().z * 1.5f));
		idealUserPose.set(JIDX.LEFT_HIP,new PVector(-43.330074f, -6.9635806f, idealUserPose.center().z * 1.5f));
		idealUserPose.set(JIDX.LEFT_KNEE,new PVector(-31.571592f, -493.167f, idealUserPose.center().z * 1.5f));
		idealUserPose.set(JIDX.LEFT_FOOT,new PVector(18.669317f, -967.7855f, idealUserPose.center().z * 1.5f));
		idealUserPose.set(JIDX.RIGHT_HIP,new PVector(170.6238f, 4.1684284f, idealUserPose.center().z * 1.5f));
		idealUserPose.set(JIDX.RIGHT_KNEE,new PVector(193.66336f, -464.59735f, idealUserPose.center().z * 1.5f));
		idealUserPose.set(JIDX.RIGHT_FOOT,new PVector(221.68254f, -936.3788f, idealUserPose.center().z * 1.5f));
		idealUserPose = idealUserPose.translated(new PVector(0, 0, idealUserPose.center().z));
		return idealUserPose;
	}
}
