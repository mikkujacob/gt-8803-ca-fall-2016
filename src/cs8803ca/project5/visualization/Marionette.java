package cs8803ca.project5.visualization;

import java.util.ArrayList;

import cs8803ca.project5.music.analysis.MusicAnalysis;
import cs8803ca.project5.music.generation.MusicNote;
import cs8803ca.project5.music.generation.MusicPlayer;
import cs8803ca.project5.visualization.utilities.Body;
import cs8803ca.project5.visualization.utilities.JIDX;
import cs8803ca.project5.visualization.utilities.LIDX;
import cs8803ca.project5.visualization.utilities.Pair;
import processing.core.PApplet;
import processing.core.PVector;

public class Marionette
{
	private PApplet parent;
	private Body body;
	private int bodyColor;
	private MusicAnalysis analysis;
	IK2 solver;
	private boolean debugEndPoints;
	private ArrayList<PVector> positions1;
	private ArrayList<PVector> positions2;
	private ArrayList<PVector> positions3;
	private ArrayList<PVector> positions4;
	private int previousIndex1;
	private int previousIndex2;
	private int previousIndex3;
	private int previousIndex4;
	
	public Marionette(PApplet parent)
	{
		this.parent = parent;
		body = Body.getIdealUserPose();
		analysis = new MusicAnalysis(parent);
		solver = new IK2(parent);
		previousIndex1 = 0;
		previousIndex2 = 0;
		previousIndex3 = 0;
		previousIndex4 = 0;
	}
	
	public void run()
	{
//		System.out.println("Running puppet");
		
		//Running MusicAnalysis
		analysis.draw();
		
		//Check to see if music has changed.
		hasMusicChanged();

		//Update the current position of the puppet
		updateBody();
	}
	
	public void draw()
	{
		//Draw marionette
		drawBody();
	}
	
	@SuppressWarnings("deprecation")
	public void updateBody()
	{
		boolean isMusicPlayingAnywhere = false;
//		System.out.println("Updating body of puppet");
		Body clone = (Body)body.clone();
		PVector previousPosition = new PVector();
		PVector futurePosition = new PVector();
		PVector twoPriorPosition = new PVector();
		PVector threePriorPosition = new PVector();
		float timeElapsed = 0;
		float timeRatio = 0;
		PVector currentPosition = new PVector();
		
		if(positions1 != null && MusicAnalysis.getPlayer1().isMusicPlaying())
		{
			isMusicPlayingAnywhere = true;
//			System.out.println("Positions1: " + positions1.toString());
//			
//			System.out.println("Max X: " + parent.width / 2f + ", Min X: " + -1 * parent.width / 2f);
//			System.out.println("Max Y: " + parent.height / 2f + ", Min Y: " + -1 * parent.height / 2f);
			int currentNoteIndex1 = MusicAnalysis.getPlayer1().getCurrentNoteIntervalIndex();
//			System.out.println("Current Note Index: " + currentNoteIndex1);
			threePriorPosition = positions1.get(MusicAnalysis.getPlayer1().getThreePreviousNoteIntervalIndex()).get();
			twoPriorPosition = positions1.get(MusicAnalysis.getPlayer1().getTwoPreviousNoteIntervalIndex()).get();
			previousPosition = positions1.get(MusicAnalysis.getPlayer1().getPreviousNoteIntervalIndex()).get();
			futurePosition = positions1.get(currentNoteIndex1).get();
//			if(previousPosition == new PVector() || futurePosition == new PVector())
//			{
//				System.out.println("REST");
//				return;
//			}
//			else
//			{
//				System.out.println("Prev Pos: " + previousPosition.toString());
//				System.out.println("Future Pos: " + futurePosition.toString());
//			}
			timeElapsed = (System.currentTimeMillis() - MusicAnalysis.getPlayer1().getStartTime()) / 1000f;
//			System.out.println("Time Elapsed1: " + timeElapsed);
			if(timeElapsed > MusicAnalysis.getPlayer1().getTimePerBeat())
			{
//				System.out.println("timeElapsed > MusicAnalysis.getPlayer1().getTimePerBeat()");
				timeElapsed = timeElapsed - (currentNoteIndex1 - 1) * MusicAnalysis.getPlayer1().getTimePerBeat();
//				System.out.println("Time Elapsed2 (currentNoteIndex): " + timeElapsed + "(" + currentNoteIndex1 + ")");
			}
			timeRatio = timeElapsed / MusicAnalysis.getPlayer1().getTimePerBeat();
//			System.out.println("Time Ratio / LERP Coeff: " + timeRatio);
			currentPosition = interpolate(threePriorPosition, twoPriorPosition, previousPosition, futurePosition, timeRatio);
			
//			System.out.println("Desired End Point: " + LIDX.getLimbs(LIDX.LEFT_FOREARM).second + ": " + currentPosition.toString());
			
			solver.solveIKTwoLink(
					currentPosition.get(), 
					clone.get(LIDX.getLimbs(LIDX.LEFT_UPPER_ARM).first).get(), 
					getLimbLength(LIDX.LEFT_UPPER_ARM), 
					getLimbLength(LIDX.LEFT_FOREARM)
					);
			if(!Float.isNaN(solver.getActualEndPoint().x) && !Float.isNaN(solver.getActualEndPoint().y))
			{
//				System.out.println("Original Position: " + clone.get(LIDX.getLimbs(LIDX.LEFT_FOREARM).second));
				PVector actualEndPoint = solver.getActualEndPoint().get();
				actualEndPoint.z = clone.center().z;
				clone.set(LIDX.getLimbs(LIDX.LEFT_FOREARM).second, actualEndPoint);
//				System.out.println("Actual End Point: " + LIDX.getLimbs(LIDX.LEFT_FOREARM).second + ": " + actualEndPoint.toString());
			}
			else
			{
//				System.out.println("Actual End Point: NAN");
			}
			if(!Float.isNaN(solver.getElbowPoint().x) && !Float.isNaN(solver.getElbowPoint().y))
			{
//				System.out.println("Original Position: " + clone.get(LIDX.getLimbs(LIDX.LEFT_FOREARM).first));
				PVector elbowPoint = solver.getElbowPoint().get();
				elbowPoint.z = clone.center().z;
				clone.set(LIDX.getLimbs(LIDX.LEFT_FOREARM).first, elbowPoint);
//				System.out.println("Elbow Point: " + LIDX.getLimbs(LIDX.LEFT_FOREARM).first + ": " + elbowPoint.toString());
			}
			else
			{
//				System.out.println("Elbow Point: NAN");
			}
		}
		
		if(positions2 != null && MusicAnalysis.getPlayer2().isMusicPlaying())
		{
			isMusicPlayingAnywhere = true;
			int currentNoteIndex2 = MusicAnalysis.getPlayer2().getCurrentNoteIntervalIndex();
			threePriorPosition = positions2.get(MusicAnalysis.getPlayer2().getThreePreviousNoteIntervalIndex()).get();
			twoPriorPosition = positions2.get(MusicAnalysis.getPlayer2().getTwoPreviousNoteIntervalIndex()).get();
			previousPosition = positions2.get(MusicAnalysis.getPlayer2().getPreviousNoteIntervalIndex()).get();
			futurePosition = positions2.get(currentNoteIndex2).get();
			timeElapsed = (System.currentTimeMillis() - MusicAnalysis.getPlayer2().getStartTime()) / 1000f;
			if(timeElapsed > MusicAnalysis.getPlayer2().getTimePerBeat())
			{
				timeElapsed = timeElapsed - (currentNoteIndex2 - 1) * MusicAnalysis.getPlayer2().getTimePerBeat();
			}
			timeRatio = timeElapsed / MusicAnalysis.getPlayer2().getTimePerBeat();
			currentPosition = interpolate(threePriorPosition, twoPriorPosition, previousPosition, futurePosition, timeRatio);
			
			solver.solveIKTwoLink(
					currentPosition.get(), 
					clone.get(LIDX.getLimbs(LIDX.LEFT_THIGH).first).get(), 
					getLimbLength(LIDX.LEFT_THIGH), 
					getLimbLength(LIDX.LEFT_SHIN)
					);
			if(!Float.isNaN(solver.getActualEndPoint().x) && !Float.isNaN(solver.getActualEndPoint().y))
			{
				PVector actualEndPoint = solver.getActualEndPoint().get();
				actualEndPoint.z = clone.center().z;
				clone.set(LIDX.getLimbs(LIDX.LEFT_SHIN).second, actualEndPoint);
			}
			if(!Float.isNaN(solver.getElbowPoint().x) && !Float.isNaN(solver.getElbowPoint().y))
			{
				PVector elbowPoint = solver.getElbowPoint().get();
				elbowPoint.z = clone.center().z;
				clone.set(LIDX.getLimbs(LIDX.LEFT_SHIN).first, elbowPoint);
			}
		}
		
		if(positions3 != null && MusicAnalysis.getPlayer3().isMusicPlaying())
		{
			isMusicPlayingAnywhere = true;
			int currentNoteIndex3 = MusicAnalysis.getPlayer3().getCurrentNoteIntervalIndex();
			threePriorPosition = positions3.get(MusicAnalysis.getPlayer3().getThreePreviousNoteIntervalIndex()).get();
			twoPriorPosition = positions3.get(MusicAnalysis.getPlayer3().getTwoPreviousNoteIntervalIndex()).get();
			previousPosition = positions3.get(MusicAnalysis.getPlayer3().getPreviousNoteIntervalIndex()).get();
			futurePosition = positions3.get(currentNoteIndex3).get();
			timeElapsed = (System.currentTimeMillis() - MusicAnalysis.getPlayer3().getStartTime()) / 1000f;
			if(timeElapsed > MusicAnalysis.getPlayer3().getTimePerBeat())
			{
				timeElapsed = timeElapsed - (currentNoteIndex3 - 1) * MusicAnalysis.getPlayer3().getTimePerBeat();
			}
			timeRatio = timeElapsed / MusicAnalysis.getPlayer3().getTimePerBeat();
			currentPosition = interpolate(threePriorPosition, twoPriorPosition, previousPosition, futurePosition, timeRatio);
			
			solver.solveIKTwoLink(
					currentPosition.get(), 
					clone.get(LIDX.getLimbs(LIDX.RIGHT_UPPER_ARM).first).get(), 
					getLimbLength(LIDX.RIGHT_UPPER_ARM), 
					getLimbLength(LIDX.RIGHT_FOREARM)
					);
			if(!Float.isNaN(solver.getActualEndPoint().x) && !Float.isNaN(solver.getActualEndPoint().y))
			{
				PVector actualEndPoint = solver.getActualEndPoint().get();
				actualEndPoint.z = clone.center().z;
				clone.set(LIDX.getLimbs(LIDX.RIGHT_FOREARM).second, actualEndPoint);
			}
			if(!Float.isNaN(solver.getElbowPoint().x) && !Float.isNaN(solver.getElbowPoint().y))
			{
				PVector elbowPoint = solver.getElbowPoint().get();
				elbowPoint.z = clone.center().z;
				clone.set(LIDX.getLimbs(LIDX.RIGHT_FOREARM).first, elbowPoint);
			}
		}
		
		if(positions4 != null && MusicAnalysis.getPlayer4().isMusicPlaying())
		{
			isMusicPlayingAnywhere = true;
			int currentNoteIndex4 = MusicAnalysis.getPlayer4().getCurrentNoteIntervalIndex();
			threePriorPosition = positions4.get(MusicAnalysis.getPlayer4().getThreePreviousNoteIntervalIndex()).get();
			twoPriorPosition = positions4.get(MusicAnalysis.getPlayer4().getTwoPreviousNoteIntervalIndex()).get();
			previousPosition = positions4.get(MusicAnalysis.getPlayer4().getPreviousNoteIntervalIndex()).get();
			futurePosition = positions4.get(currentNoteIndex4).get();
			timeElapsed = (System.currentTimeMillis() - MusicAnalysis.getPlayer4().getStartTime()) / 1000f;
			if(timeElapsed > MusicAnalysis.getPlayer4().getTimePerBeat())
			{
				timeElapsed = timeElapsed - (currentNoteIndex4 - 1) * MusicAnalysis.getPlayer4().getTimePerBeat();
			}
			timeRatio = timeElapsed / MusicAnalysis.getPlayer4().getTimePerBeat();
			currentPosition = interpolate(threePriorPosition, twoPriorPosition, previousPosition, futurePosition, timeRatio);
			
			solver.solveIKTwoLink(
					currentPosition.get(), 
					clone.get(LIDX.getLimbs(LIDX.RIGHT_THIGH).first).get(), 
					getLimbLength(LIDX.RIGHT_THIGH), 
					getLimbLength(LIDX.RIGHT_SHIN)
					);
			if(!Float.isNaN(solver.getActualEndPoint().x) && !Float.isNaN(solver.getActualEndPoint().y))
			{
				PVector actualEndPoint = solver.getActualEndPoint().get();
				actualEndPoint.z = clone.center().z;
				clone.set(LIDX.getLimbs(LIDX.RIGHT_SHIN).second, actualEndPoint);
			}
			if(!Float.isNaN(solver.getElbowPoint().x) && !Float.isNaN(solver.getElbowPoint().y))
			{
				PVector elbowPoint = solver.getElbowPoint().get();
				elbowPoint.z = clone.center().z;
				clone.set(LIDX.getLimbs(LIDX.RIGHT_SHIN).first, elbowPoint);
			}
		}
		
		if(isMusicPlayingAnywhere)
		{
			body = clone;
		}
		else
		{
			body = Body.getIdealUserPose();
		}
	}
	
	public PVector interpolate(PVector A, PVector B, PVector C, PVector D, float coefficient)
	{
		if(coefficient < 0)
		{
			coefficient = 0;
		}
		else if(coefficient > 1)
		{
			coefficient = 1;
		}
		
		float t = (float)(scaleRange(coefficient, 0, 1, 0.75, 1));
		
//		A, B, C, D => E, F, G => H, I => J
		
		PVector E = generalizedLERP(0, A, 11f/3f, B, t);
		PVector F = generalizedLERP(1f/3f, B, 2f/3f, C, t);
		PVector G = generalizedLERP(2f/3f, C, 1, D, t);
		
		PVector H = generalizedLERP(0, E, 1f/2f, F, t);
		PVector I = generalizedLERP(1f/2f, F, 1, G, t);
		
		PVector J = generalizedLERP(0, H, 1, I, t);
		
		return J.get();
//		return PVector.lerp(A.get(), B.get(), coefficient);
	}
	
	public double scaleRange(double oldValue, double oldMin, double oldMax, double newMin, double newMax)
	{
		return (((oldValue - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
	}
	
	public PVector generalizedLERP(float a, PVector A, float b, PVector B, float t)
	{
		return new PVector(((b-t) / (b-a) * A.x + (t-a) / (b-a) * B.x), ((b-t) / (b-a) * A.y + (t-a) / (b-a) * B.y), ((b-t) / (b-a) * A.z + (t-a) / (b-a) * B.z));
	}
	
	public double[] getDoubleArray(PVector P)
	{
		double[] d = new double[3];
		d[0] = P.x;
		d[1] = P.y;
		d[2] = P.z;
		return d;
	}
	
	public ArrayList<PVector> translatePositions(ArrayList<PVector> positions, PVector translatedOrigin)
	{
		for(int index = 0; index < positions.size(); index++)
		{
			PVector position = positions.get(index);
			position.x += translatedOrigin.x;
			position.y += translatedOrigin.y;
			positions.set(index, position);
		}
		return positions;
	}
	
	public void hasMusicChanged()
	{
//		System.out.println("Detecting music change");
		if(analysis.isMusicChanged1() || (positions1 != null && previousIndex1 == positions1.size() - 1 && MusicAnalysis.getPlayer1().getPreviousNoteIntervalIndex() == 0))
		{
			positions1 = calculatePositions(MusicAnalysis.getPlayer1(), totalLimbLength(LIDX.LEFT_UPPER_ARM, LIDX.LEFT_FOREARM), getStartAngle(LIDX.LEFT_FOREARM), getEndAngle(LIDX.LEFT_FOREARM));
			positions1 = translatePositions(positions1, body.get(JIDX.LEFT_SHOULDER));
			analysis.setMusicChanged1(false);
		}
		previousIndex1 = MusicAnalysis.getPlayer1().getPreviousNoteIntervalIndex();
		
		if(analysis.isMusicChanged2() || (positions2 != null && previousIndex2 == positions2.size() - 1 && MusicAnalysis.getPlayer2().getPreviousNoteIntervalIndex() == 0))
		{
			positions2 = calculatePositions(MusicAnalysis.getPlayer2(), totalLimbLength(LIDX.LEFT_THIGH, LIDX.LEFT_SHIN), getStartAngle(LIDX.LEFT_SHIN), getEndAngle(LIDX.LEFT_SHIN));
			positions2 = translatePositions(positions2, body.get(JIDX.LEFT_HIP));
			analysis.setMusicChanged2(false);
		}
		previousIndex2 = MusicAnalysis.getPlayer2().getPreviousNoteIntervalIndex();
		
		if(analysis.isMusicChanged3() || (positions3 != null && previousIndex3 == positions3.size() - 1 && MusicAnalysis.getPlayer3().getPreviousNoteIntervalIndex() == 0))
		{
			positions3 = calculatePositions(MusicAnalysis.getPlayer3(), totalLimbLength(LIDX.RIGHT_UPPER_ARM, LIDX.RIGHT_FOREARM), getStartAngle(LIDX.RIGHT_FOREARM), getEndAngle(LIDX.RIGHT_FOREARM));
			positions3 = translatePositions(positions3, body.get(JIDX.RIGHT_SHOULDER));
			analysis.setMusicChanged3(false);
		}
		previousIndex3 = MusicAnalysis.getPlayer3().getPreviousNoteIntervalIndex();
		
		if(analysis.isMusicChanged4() || (positions4 != null && previousIndex4 == positions4.size() - 1 && MusicAnalysis.getPlayer4().getPreviousNoteIntervalIndex() == 0))
		{
			positions4 = calculatePositions(MusicAnalysis.getPlayer4(), totalLimbLength(LIDX.RIGHT_THIGH, LIDX.RIGHT_SHIN), getStartAngle(LIDX.RIGHT_SHIN), getEndAngle(LIDX.RIGHT_SHIN));
			positions4 = translatePositions(positions4, body.get(JIDX.RIGHT_HIP));
			analysis.setMusicChanged4(false);
		}
		previousIndex4 = MusicAnalysis.getPlayer4().getPreviousNoteIntervalIndex();
	}
	
	public void drawBody()
	{
		parent.pushMatrix();
		parent.translate(parent.width/2f, 2*parent.height/5f);
		int strokeColor = parent.g.strokeColor;
		float strokeWeight = parent.g.strokeWeight;
		int fillColor = parent.g.fillColor;
		
		float limbThickness = 1000f;
		float jointThickness = 10f;
		for (LIDX lidx : LIDX.ALL_LIDX)
		{
			Pair<JIDX, JIDX> limbEnds = LIDX.getLimbs(lidx);
			PVector pos1 = getScreenPos(body.get(limbEnds.first));
			PVector pos2 = getScreenPos(body.get(limbEnds.second));
			limbThickness = LIDX.getLimbThickness(lidx);
			drawSkinnedLimb(pos1, pos2, limbThickness);
		}
		
		parent.noStroke();
		parent.fill(127);
		
		for (LIDX lidx : LIDX.ALL_LIDX)
		{
			if (lidx == LIDX.HEAD || lidx == LIDX.TORSO
					|| lidx == LIDX.LEFT_FOREARM || lidx == LIDX.RIGHT_FOREARM
					|| lidx == LIDX.LEFT_SHIN || lidx == LIDX.RIGHT_SHIN)
			{
				continue;
			}
			Pair<JIDX, JIDX> limbEnds = LIDX.getLimbs(lidx);
			PVector pos1 = getScreenPos(body.get(limbEnds.first));
			PVector pos2 = getScreenPos(body.get(limbEnds.second));
			
			parent.ellipse(pos1.x, pos1.y, jointThickness, jointThickness);
			parent.ellipse(pos2.x, pos2.y, jointThickness, jointThickness);
		}
		
		parent.fill(fillColor);
		parent.stroke(strokeColor);
		parent.strokeWeight(strokeWeight);
		Pair<JIDX, JIDX> limbEnds = LIDX.getLimbs(LIDX.HEAD);
		PVector pos1 = getScreenPos(body.get(limbEnds.first));
		PVector pos2 = getScreenPos(body.get(limbEnds.second));
		limbThickness = LIDX.getLimbThickness(LIDX.HEAD);
		drawSkinnedLimb(pos1, pos2, limbThickness);
		
		parent.noStroke();
		parent.fill(127);
		
		PVector pos = getScreenPos(body.get(JIDX.NECK));
		parent.ellipse(pos.x, pos.y, jointThickness, jointThickness);
		
		if(positions1 != null && isDebugEndPoints())
		{
			for(PVector position1: positions1)
			{
				parent.fill(parent.color(125,55, 235));
//				System.out.println("Position1: " + position1.toString());
				pos = getScreenPos(position1);
				parent.ellipse(pos.x, pos.y, jointThickness, jointThickness);
				
				parent.fill(parent.color(55, 235, 125));
//				System.out.println("Translated Origin: " + body.get(JIDX.LEFT_SHOULDER).toString());
				pos = getScreenPos(body.get(JIDX.LEFT_SHOULDER));
				parent.ellipse(pos.x, pos.y, jointThickness, jointThickness);
			}
		}
		
		if(positions2 != null && isDebugEndPoints())
		{
			for(PVector position2: positions2)
			{
				parent.fill(parent.color(125,55, 235));
//				System.out.println("Position2: " + position2.toString());
				pos = getScreenPos(position2);
				parent.ellipse(pos.x, pos.y, jointThickness, jointThickness);
				
				parent.fill(parent.color(55, 235, 125));
//				System.out.println("Translated Origin: " + body.get(JIDX.LEFT_HIP).toString());
				pos = getScreenPos(body.get(JIDX.LEFT_SHOULDER));
				parent.ellipse(pos.x, pos.y, jointThickness, jointThickness);
			}
		}
		
		if(positions3 != null && isDebugEndPoints())
		{
			for(PVector position3: positions3)
			{
				parent.fill(parent.color(125,55, 235));
//				System.out.println("Position3: " + position3.toString());
				pos = getScreenPos(position3);
				parent.ellipse(pos.x, pos.y, jointThickness, jointThickness);
				
				parent.fill(parent.color(55, 235, 125));
//				System.out.println("Translated Origin: " + body.get(JIDX.RIGHT_SHOULDER).toString());
				pos = getScreenPos(body.get(JIDX.LEFT_SHOULDER));
				parent.ellipse(pos.x, pos.y, jointThickness, jointThickness);
			}
		}
		
		if(positions4 != null && isDebugEndPoints())
		{
			for(PVector position4: positions4)
			{
				parent.fill(parent.color(125,55, 235));
//				System.out.println("Position4: " + position4.toString());
				pos = getScreenPos(position4);
				parent.ellipse(pos.x, pos.y, jointThickness, jointThickness);
				
				parent.fill(parent.color(55, 235, 125));
//				System.out.println("Translated Origin: " + body.get(JIDX.RIGHT_HIP).toString());
				pos = getScreenPos(body.get(JIDX.LEFT_SHOULDER));
				parent.ellipse(pos.x, pos.y, jointThickness, jointThickness);
			}
		}
		
		parent.fill(fillColor);
		parent.stroke(strokeColor);
		parent.strokeWeight(strokeWeight);
		parent.popMatrix();
	}
	
	public void drawSkinnedLimb(PVector position1, PVector position2, float limbThickness)
	{
		parent.pushMatrix();
		
		int strokeColor = parent.g.strokeColor;
		float strokeWeight = parent.g.strokeWeight;
		int fillColor = parent.g.fillColor;
		parent.fill(bodyColor);
		parent.stroke(200);
		parent.strokeWeight(6);
//		parent.noStroke();
		
		float distance = PVector.dist(position2, position1);
		float angle = PApplet.atan2((position2.y - position1.y), (position2.x - position1.x));
		
		PVector midPoint = new PVector(position1.x + 0.5f * (position2.x - position1.x), position1.y + 0.5f * (position2.y - position1.y));
		
		parent.translate(midPoint.x, midPoint.y);
		parent.rotate(angle);
		parent.translate(-1 * midPoint.x,-1 * midPoint.y);
		
		parent.ellipse(midPoint.x, midPoint.y, distance, limbThickness);
		
		parent.fill(fillColor);
		parent.stroke(strokeColor);
		parent.strokeWeight(strokeWeight);
		
		parent.popMatrix();
	}

	@SuppressWarnings("deprecation")
	private PVector getScreenPos(PVector pos)
	{
//		PVector screenPos = PVector.mult(pos.get(), 0.3f);
		PVector screenPos = pos.get();
		screenPos.x = -screenPos.x;
		screenPos.y = -screenPos.y;
		screenPos.z = (parent.height / 900.0f) * 800.0f / screenPos.z;
		screenPos.x *= screenPos.z;
		screenPos.y *= screenPos.z;
		return screenPos;
	}
	
	@SuppressWarnings({ "unused", "deprecation" })
	private Body getScreenPos(Body pose)
	{
		Body screenPose = (Body) pose.clone();
		for (JIDX jidx : JIDX.ALL_JIDX)
		{
			PVector tempVector = getScreenPos(screenPose.get(jidx).get());
			
			screenPose.set(jidx, tempVector.get());
		}
		
		return screenPose;
	}
	
	public ArrayList<PVector> calculatePositions(MusicPlayer player, float totalLimbLength, float startAngle, float endAngle)
	{
		ArrayList<PVector> result = new ArrayList<PVector>();
		
		for(ArrayList<MusicNote> musicNotes : player.getMusicNotesList())
		{
			result.add(analysis.convertmusicNotesToPosition(musicNotes, player, totalLimbLength, startAngle, endAngle));
			if(result.get(result.size() - 1) == null)
			{
				result.set(result.size() - 1, new PVector());
			}
			result.get(result.size() - 1).z = Body.getIdealUserPose().center().z;
		}
		
		return result;
	}
	
	public float getLimbLength(LIDX limb)
	{
		return (PVector.dist(Body.getIdealUserPose().get(LIDX.getLimbs(limb).first), Body.getIdealUserPose().get(LIDX.getLimbs(limb).second)) * 0.75f);
	}
	
	public float totalLimbLength(LIDX limb1, LIDX limb2)
	{
		
		float d1 = getLimbLength(limb1);
		float d2 = getLimbLength(limb2);
		return Math.abs(d1) + Math.abs(d2);
	}
	
	public float getStartAngle(LIDX endLimb)
	{
		if(endLimb == LIDX.LEFT_FOREARM)
		{
			return 0;
		}
		else if(endLimb == LIDX.RIGHT_FOREARM)
		{
			return (float)(3 * Math.PI / 2);
		}
		else if(endLimb == LIDX.LEFT_SHIN)
		{
			return (float)(Math.PI / 2);
		}
		else if(endLimb == LIDX.RIGHT_SHIN)
		{
			return (float)(Math.PI);
		}
		
		return 0;
	}
	
	public float getEndAngle(LIDX endLimb)
	{
		if(endLimb == LIDX.LEFT_FOREARM)
		{
			return (float)(Math.PI / 2);
		}
		else if(endLimb == LIDX.RIGHT_FOREARM)
		{
			return (float)(2 * Math.PI);
		}
		else if(endLimb == LIDX.LEFT_SHIN)
		{
			return (float)(Math.PI);
		}
		else if(endLimb == LIDX.RIGHT_SHIN)
		{
			return (float)(3 * Math.PI / 2);
		}
		
		return 0;
	}

	public Body getBody()
	{
		return body;
	}

	public MusicAnalysis getAnalysis()
	{
		return analysis;
	}

	public void setAnalysis(MusicAnalysis analysis)
	{
		this.analysis = analysis;
	}

	public int getBodyColor()
	{
		return bodyColor;
	}

	public void setBodyColor(int bodyColor)
	{
		this.bodyColor = bodyColor;
	}

	public boolean isDebugEndPoints()
	{
		return debugEndPoints;
	}

	public void setDebugEndPoints(boolean debugEndPoints)
	{
		this.debugEndPoints = debugEndPoints;
	}
}
