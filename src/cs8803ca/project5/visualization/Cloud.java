package cs8803ca.project5.visualization;

import cs8803ca.project5.shared.ColorSpaces;
import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;

public class Cloud
{
	private PApplet parent;
	private PImage cloud;
	private PVector position;
	private float width;
	private float height;
	private ColorSpaces colorSpaces;
	private float l;
	private float c;
	private float h;
	private float a;
	private float b;
	private float hOffset;
	private float novelty;
	private float surprise;
	private float quality;
	private float beauty;
	
	public Cloud(PImage cloud, PVector position, ColorSpaces colorSpaces, PApplet parent)
	{
		this.cloud = cloud;
		this.position = position;
		this.colorSpaces = colorSpaces;
		this.parent = parent;
		this.width = this.parent.height * 0.2f;
		this.height = this.parent.height * 0.1f;
		cloud.resize(Math.round(this.width), Math.round(this.height));
	}
	
	public void setLCH(float l, float c, float h)
	{
		this.l = l;
		this.c = c;
		this.h = h;
	}
	
	public void setLAB(float l, float a, float b)
	{
		this.l = l;
		this.a = a;
		this.b = b;
	}
	
	public void setMusicParams(float novelty, float surprise, float quality, float beauty)
	{
		this.novelty = novelty;
		this.surprise = surprise;
		this.quality = quality;
		this.beauty = beauty;
	}
	
	public void calculateHOffset()
	{
		hOffset = parent.random(0, 360);
	}
	
	public void draw(boolean usingLAB)
	{
		int newColor = 0;
		
		if(usingLAB)
		{
			float lScaled = 50;
			float aScaled = (a + 50 > 100) ? a - 50 : a + 50;
			float bScaled = (b + 50 > 100) ? b - 50 : b + 50;
			newColor = colorSpaces.LABtoColor(lScaled, aScaled, bScaled);
		}
		else
		{
			float lNewScaled = 50;
			float cNewScaled = 100;
			float hNewScaled = (hOffset + 360 * (quality)) > 360 ? (hOffset + 360 * (quality) - 360) : (hOffset + 360 * (quality));
			newColor = colorSpaces.LCHtoColor(lNewScaled, cNewScaled, hNewScaled);
		}
		
		parent.tint(newColor, 200);
		parent.image(cloud, position.x, position.y);
		parent.noTint();
	}

	public PVector getPosition()
	{
		return position;
	}

	public void setPosition(PVector position)
	{
		this.position = position;
	}

	public float getWidth()
	{
		return width;
	}

	public void setWidth(float width)
	{
		this.width = width;
	}

	public float getHeight()
	{
		return height;
	}

	public void setHeight(float height)
	{
		this.height = height;
	}
}
