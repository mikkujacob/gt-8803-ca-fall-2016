package cs8803ca.project5.application;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import cs8803ca.project5.music.generation.MusicWorkspace;
import cs8803ca.project5.visualization.Visualization;

import processing.core.PApplet;
import processing.core.PImage;

public class Project5 extends PApplet
{
	private static final Boolean isFullScreen = true;
	private int screenX, screenY, windowW = 600, windowH = 600;
	private ArrayList<String> listGUIText;
	private ArrayList<Long> listGUIDuration;
	
	private boolean VISUALIZATION_MODE = false, MUSIC_WORKSPACE_MODE = true;
	private boolean DRAW_GUI = true;
	private boolean filming = false;
	private int frameCounter;
	
	private MusicWorkspace musicWorkspace;
	private Visualization visualization;
	
	
	public static void main(String[] args)
	{
		if(isFullScreen)
		{
			PApplet.main(new String[] { "--present", "cs8803ca.project5.application.Project5" });
		}
		else
		{
			PApplet.main(new String[] { "cs8803ca.project5.application.Project5" });
		}
	}
	
	public void settings()
	{
		if(isFullScreen)
		{
			fullScreen();
		}
		else
		{
			size(windowW, windowH); // window size
		}
		screenX = width;
		screenY = height;
		smooth(); // turn on antialiasing
	}
	
	public void setup()
	{
		listGUIText = new ArrayList<String>();
		listGUIDuration = new ArrayList<Long>();
		
		musicWorkspace = new MusicWorkspace(this);
		visualization = new Visualization(this);
		
		frameCounter = 0;
		
//		addGUIText("Edit loops on each turntable\nWhile recording, left click adds notes.\nOtherwise, right click removes notes.", 10000);
	}
	
	public void draw()
	{
		if(screenX != width && screenY != height)
		{
			//Window size changed
			
			screenX = width;
			screenY = height;
		}
		
		background(color(0,0,0));
		
		if(MUSIC_WORKSPACE_MODE)
		{
			musicWorkspace.draw();
		}
		else if(VISUALIZATION_MODE)
		{
			visualization.draw();
		}
		
		visualization.run();
		
		if(DRAW_GUI)
		{
			drawGUI();
		}
		
		if(filming)
		{
			PImage screenShot = takeScreenShot();
			screenShot.save("FRAMES/F" + nf(frameCounter++, 5) + ".tif");
		}
	}
	
	public PImage takeScreenShot()
	{
	    Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
	    
	    try {
	        
	        BufferedImage screenBuffer = new Robot().createScreenCapture(screenRect);
	        
//	        PImage screenShot = new PImage(screenBuffer.getWidth(), screenBuffer.getHeight(), ARGB);
	        PImage screenShot = createImage(screenBuffer.getWidth(), screenBuffer.getHeight(), ARGB);
	        screenBuffer.getRGB(0, 0, screenShot.width, screenShot.height, screenShot.pixels, 0, screenShot.width);
	        screenShot.updatePixels();
	        
	        return screenShot;
	        
	    } catch ( AWTException e ) {
	        e.printStackTrace();
	    }
	    
	    return null;
	}
	
	public void tempoNumberBox1(int tempo)
	{
		if(musicWorkspace != null)
			musicWorkspace.tempoNumberBox1(tempo);
	}
	
	public void tempoNumberBox2(int tempo)
	{
		if(musicWorkspace != null)
			musicWorkspace.tempoNumberBox2(tempo);
	}
	
	public void tempoNumberBox3(int tempo)
	{
		if(musicWorkspace != null)
			musicWorkspace.tempoNumberBox3(tempo);
	}
	
	public void tempoNumberBox4(int tempo)
	{
		if(musicWorkspace != null)
			musicWorkspace.tempoNumberBox4(tempo);
	}
	
	public void timeSignatureNumeratorNumberBox1(int numerator)
	{
		if(musicWorkspace != null)
			musicWorkspace.timeSignatureNumeratorNumberBox1(numerator);
	}
	
	public void timeSignatureNumeratorNumberBox2(int numerator)
	{
		if(musicWorkspace != null)
			musicWorkspace.timeSignatureNumeratorNumberBox2(numerator);
	}
	
	public void timeSignatureNumeratorNumberBox3(int numerator)
	{
		if(musicWorkspace != null)
			musicWorkspace.timeSignatureNumeratorNumberBox3(numerator);
	}
	
	public void timeSignatureNumeratorNumberBox4(int numerator)
	{
		if(musicWorkspace != null)
			musicWorkspace.timeSignatureNumeratorNumberBox4(numerator);
	}

	public void timeSignatureDenominatorNumberBox1(int denominator)
	{
		if(musicWorkspace != null)
			musicWorkspace.timeSignatureDenominatorNumberBox1(denominator);
	}

	public void timeSignatureDenominatorNumberBox2(int denominator)
	{
		if(musicWorkspace != null)
			musicWorkspace.timeSignatureDenominatorNumberBox2(denominator);
	}

	public void timeSignatureDenominatorNumberBox3(int denominator)
	{
		if(musicWorkspace != null)
			musicWorkspace.timeSignatureDenominatorNumberBox3(denominator);
	}

	public void timeSignatureDenominatorNumberBox4(int denominator)
	{
		if(musicWorkspace != null)
			musicWorkspace.timeSignatureDenominatorNumberBox4(denominator);
	}
	
	public void recordButton1()
	{
		if(musicWorkspace != null)
			musicWorkspace.recordButton1();
	}
	
	public void recordButton2()
	{
		if(musicWorkspace != null)
			musicWorkspace.recordButton2();
	}
	
	public void recordButton3()
	{
		if(musicWorkspace != null)
			musicWorkspace.recordButton3();
	}
	
	public void recordButton4()
	{
		if(musicWorkspace != null)
			musicWorkspace.recordButton4();
	}
	
	public void playButton1()
	{
		if(musicWorkspace != null)
			musicWorkspace.playButton1();
	}
	
	public void playButton2()
	{
		if(musicWorkspace != null)
			musicWorkspace.playButton2();
	}
	
	public void playButton3()
	{
		if(musicWorkspace != null)
			musicWorkspace.playButton3();
	}
	
	public void playButton4()
	{
		if(musicWorkspace != null)
			musicWorkspace.playButton4();
	}
	
	public void noteList1(int itemIndex)
	{
		if(musicWorkspace != null)
			musicWorkspace.noteList1(itemIndex);
	}
	
	public void noteList2(int itemIndex)
	{
		if(musicWorkspace != null)
			musicWorkspace.noteList2(itemIndex);
	}
	
	public void noteList3(int itemIndex)
	{
		if(musicWorkspace != null)
			musicWorkspace.noteList3(itemIndex);
	}
	
	public void noteList4(int itemIndex)
	{
		if(musicWorkspace != null)
			musicWorkspace.noteList4(itemIndex);
	}
	
	public void scaleList1(int itemIndex)
	{
		if(musicWorkspace != null)
			musicWorkspace.scaleList1(itemIndex);
	}
	
	public void scaleList2(int itemIndex)
	{
		if(musicWorkspace != null)
			musicWorkspace.scaleList2(itemIndex);
	}
	
	public void scaleList3(int itemIndex)
	{
		if(musicWorkspace != null)
			musicWorkspace.scaleList3(itemIndex);
	}
	
	public void scaleList4(int itemIndex)
	{
		if(musicWorkspace != null)
			musicWorkspace.scaleList4(itemIndex);
	}
	
	public void octaveBox1(int octave)
	{
		if(musicWorkspace != null)
			musicWorkspace.octaveBox1(octave);
	}
	
	public void octaveBox2(int octave)
	{
		if(musicWorkspace != null)
			musicWorkspace.octaveBox2(octave);
	}
	
	public void octaveBox3(int octave)
	{
		if(musicWorkspace != null)
			musicWorkspace.octaveBox3(octave);
	}
	
	public void octaveBox4(int octave)
	{
		if(musicWorkspace != null)
			musicWorkspace.octaveBox4(octave);
	}
	
	public void chordList1(int itemIndex)
	{
		if(musicWorkspace != null)
			musicWorkspace.chordList1(itemIndex);
	}
	
	public void chordList2(int itemIndex)
	{
		if(musicWorkspace != null)
			musicWorkspace.chordList2(itemIndex);
	}
	
	public void chordList3(int itemIndex)
	{
		if(musicWorkspace != null)
			musicWorkspace.chordList3(itemIndex);
	}
	
	public void chordList4(int itemIndex)
	{
		if(musicWorkspace != null)
			musicWorkspace.chordList4(itemIndex);
	}
	
	public void instrumentList1(int itemIndex)
	{
		if(musicWorkspace != null)
			musicWorkspace.instrumentList1(itemIndex);
	}
	
	public void instrumentList2(int itemIndex)
	{
		if(musicWorkspace != null)
			musicWorkspace.instrumentList2(itemIndex);
	}
	
	public void instrumentList3(int itemIndex)
	{
		if(musicWorkspace != null)
			musicWorkspace.instrumentList3(itemIndex);
	}
	
	public void instrumentList4(int itemIndex)
	{
		if(musicWorkspace != null)
			musicWorkspace.instrumentList4(itemIndex);
	}
	
	public void playPauseButton()
	{
		if(musicWorkspace != null)
			musicWorkspace.playPauseButton();
	}
	
	public void mousePressed()
	{
		if(musicWorkspace != null)
			musicWorkspace.mousePressed(mouseX, mouseY, pmouseX, pmouseY);
	}
	
	public void mouseReleased()
	{
		if(musicWorkspace != null)
			musicWorkspace.mouseReleased(mouseX, mouseY, pmouseX, pmouseY);
	}
	
	public void mouseDragged()
	{
		if(musicWorkspace != null)
			musicWorkspace.mouseDragged(mouseX, mouseY, pmouseX, pmouseY);
	}
	
	public void keyPressed()
	{
		if(musicWorkspace != null)
			musicWorkspace.keyPressed(key, keyCode);
		
		if(key == 'm' || key == 'M')
		{
			MUSIC_WORKSPACE_MODE = true;
			VISUALIZATION_MODE = false;
		}
		
		if(key == 'v' || key == 'V')
		{
			if(VISUALIZATION_MODE == false)
			{
				visualization.createClouds();
			}
			MUSIC_WORKSPACE_MODE = false;
			VISUALIZATION_MODE = true;
		}
		
		if(VISUALIZATION_MODE && (key == 'l' || key == 'L'))
		{
			visualization.setUsingLAB(!visualization.isUsingLAB());
		}
		
		if(VISUALIZATION_MODE && (key == 'p' || key == 'P'))
		{
			visualization.setDebugPoints(!visualization.isDebugPoints());
		}
		
		if(key == '`' || key == '~')
		{
			filming = !filming;
		}
	}
	
	public void keyReleased()
	{
		if(musicWorkspace != null)
			musicWorkspace.keyReleased(key, keyCode);
	}
	
	public void addGUIText(String message, long duration)
	{
		for (int index = 0; index < this.listGUIText.size(); index++)
		{
			String testMessage = this.listGUIText.get(index);
			if (message.split(":")[0].equalsIgnoreCase(testMessage.split(":")[0]))
			{
				this.listGUIText.remove(index);
				this.listGUIDuration.remove(index);
				break;
			}
		}
		this.listGUIText.add(message.toUpperCase());
		this.listGUIDuration.add(System.currentTimeMillis() + duration);
	}

	public void drawGUI()
	{
		int oldFill = this.g.fillColor;
		
		fill(color(255, 255, 255));
		textAlign(LEFT, TOP);
		long timeNowMillis = System.currentTimeMillis();
		textSize(32);
		float xPosPercent = 2f / 100f * (float) width;
		float yPosPercent = 3f / 100f * (float) height;
		for (int index = 0; index < this.listGUIText.size(); index++)
		{
			String message = this.listGUIText.get(index);
			long displayTime = this.listGUIDuration.get(index);
			text(message, xPosPercent, yPosPercent, 75f);
			yPosPercent += 5f / 100f * (float) displayHeight;
			if (displayTime < timeNowMillis)
			{
				this.listGUIText.remove(index);
				this.listGUIDuration.remove(index);
			}
		}
		
		fill(oldFill);
	}
}
