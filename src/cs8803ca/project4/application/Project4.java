package cs8803ca.project4.application;

import java.util.ArrayList;
import java.util.List;
//import java.util.Random;

import org.jfugue.theory.Chord;
import org.jfugue.theory.Intervals;
import org.jfugue.theory.Note;
import org.jfugue.theory.Scale;

import cs8803ca.project4.interaction.leapmotion.LeapMotionInteraction;
import cs8803ca.project4.music.ChordCell;
import cs8803ca.project4.music.ChordMatrix;
import cs8803ca.project4.shared.Point;
import processing.core.PApplet;

public class Project4 extends PApplet
{
	private static final Boolean isFullScreen = true;
	private int screenX, screenY, windowW = 600, windowH = 600;
//	private Random random;
	
	public final static String INFO_TEXT = "MUSIC IS LIFE...   A-LIFE TO BE SPECIFIC...\n\nCLICK AND/OR DRAG THE MOUSE WITH THE LEFT BUTTON \n        TO ADD MUSICAL PARTS.\nREPEAT WITH RIGHT BUTTON TO ERASE (APPLIES EVERYWHERE).\nHOLD THE 'A' KEY WHILE CLICKING / DRAGGING TO ADD ANTS \n        THAT CAN MODIFY THE MUSIC PARTS IN THEIR ENVIRONMENT.\nHOLD THE 'C' KEY SIMILARLY TO ADD CELLULAR AUTOMATA \n        THAT CAN MODIFY MUSIC PARTS TOO.\nHOLD THE 'B' KEY SIMILARLY TO ADD BOIDS \n        TO READ THE MUSIC PARTS AND PLAY THEM.\nPRESS THE 'N' KEY TO TOGGLE BETWEEN CHORDS AND NOTES.\nPRESS THE 'S' KEY TO CYCLE THROUGH THE VARIOUS SCALES.\nPRESS THE LEFT / DOWN AND RIGHT / UP ARROW KEYS \n        TO CYCLE THE SCALE'S ROOT NOTES.\nPRESS 'M' KEY TO TOGGLE MULTIPLE PLAYHEADS.\nPRESS THE 'H' OR '?' KEY TO SEE THIS INFO AGAIN.";
	public final static String[] ALL_CHORD_SIGNATURES = Chord.getChordNames();
	private static String[] CHORD_SIGNATURES;
	public final static int selectedChords = 2;
	public final static String[] ALL_NOTES = Note.NOTE_NAMES_COMMON;
	public static String[] NOTES = Note.NOTE_NAMES_COMMON;
	public static Scale scale;
	public static int rootNoteIndex;
	public static Scales currentScale = Scales.MAJOR;
	public static Scales previousScale = Scales.MAJOR;
	public final static String[] DURATIONS = new String[] {
		"W", "h", "q", "i", "s", "t"
	};
	public final static int octaveRange = 2;
	public final static int defaultChordOctave = 2;
	public final static int defaultNoteOctave = 2;
	
	public enum Scales {
		MAJOR,
		MINOR,
		CIRCLE_OF_FIFTHS,
		CHROMATIC
	};
	
	private ChordMatrix matrix;
	private final static int bars = 4;
	private final static int notesPerBar = 4;
	
	private boolean CREATE_ANT = false;
	private boolean CREATE_CELLULAR_AUTOMATON = false;
	private boolean CREATE_BOID = false;
	
	private ArrayList<String> listGUIText;
	private ArrayList<Long> listGUIDuration;
	
	private LeapMotionInteraction leapMotion;

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		if(isFullScreen)
		{
			PApplet.main(new String[] { "--present", "cs8803ca.project4.application.Project4" });
		}
		else
		{
			PApplet.main(new String[] { "cs8803ca.project4.application.Project4" });
		}
	}
	
	public void settings()
	{
		if(isFullScreen)
		{
			fullScreen(PApplet.P3D);
		}
		else
		{
			size(windowW, windowH, PApplet.P3D); // window size
		}
		screenX = width;
		screenY = height;
		smooth(); // turn on antialiasing
	}
	
	public void setup()
	{
//		random = new Random();
		CHORD_SIGNATURES = new String[(selectedChords <= ALL_CHORD_SIGNATURES.length) ? selectedChords : ALL_CHORD_SIGNATURES.length];
		for(int index = ALL_CHORD_SIGNATURES.length - 1, chordCount = 0; index >= 0 && chordCount < selectedChords;index--, chordCount++)
		{
			CHORD_SIGNATURES[chordCount] = ALL_CHORD_SIGNATURES[index];
		}
		
		if(currentScale.compareTo(Scales.MAJOR) == 0)
		{
			scale = Scale.MAJOR;
			
		}
		else if(currentScale.compareTo(Scales.MINOR) == 0)
		{
			scale = Scale.MINOR;
		}
		else if(currentScale.compareTo(Scales.CIRCLE_OF_FIFTHS) == 0)
		{
			scale = Scale.CIRCLE_OF_FIFTHS;
		}
		else if(currentScale.compareTo(Scales.CHROMATIC) == 0)
		{
			scale = Scale.CIRCLE_OF_FIFTHS;
		}
		
		rootNoteIndex = 0;
		Intervals intervals = scale.getIntervals();
		intervals.setRoot(Note.NOTE_NAMES_COMMON[rootNoteIndex]);
		List<Note> notes = intervals.getNotes();
		ArrayList<String> noteString = new ArrayList<String>(); 
		for(Note n : notes)
		{
			noteString.add(Note.NOTE_NAMES_COMMON[n.getPositionInOctave()]);
		}
		NOTES = new String[noteString.size()];
		noteString.toArray(NOTES);
		
		System.out.print("NOTES IN SCALE: ");
		for(int i = 0;i < NOTES.length; i++)
		{
			System.out.print(NOTES[i] + " ");
		}
		System.out.println();
		
		matrix = new ChordMatrix(bars * notesPerBar, CHORD_SIGNATURES.length * NOTES.length * octaveRange, this);
//		matrix = new ChordMatrix(CHORD_SIGNATURES.length * NOTES.length * octaveRange, CHORD_SIGNATURES.length * NOTES.length * octaveRange, this);
		
		listGUIText = new ArrayList<String>();
		listGUIDuration = new ArrayList<Long>();
		
		addGUIText(INFO_TEXT, 10000);
		
//		leapMotion = new LeapMotionInteraction(this);
	}
	
	
	
	public void draw()
	{
		if(screenX != width && screenY != height)
		{
			//Window size changed
			
			screenX = width;
			screenY = height;
		}
		
		background(color(0,0,0));
		
		drawChordMatrix();
		
		drawGUI();
		
//		handleLeapInteractions();
	}
	
	public void handleLeapInteractions()
	{
		leapMotion.setMinMaxSizes(matrix.getDimensionX(), matrix.getDimensionY());
		leapMotion.draw();
		if(leapMotion.isLeftPressed())
		{
			int leapX = (int)(leapMotion.getLeftHandLocation().getX() * displayWidth);
			int leapY = (int)(displayHeight - leapMotion.getLeftHandLocation().getY() * displayHeight);
			
			if(leapMotion.isLeftPressedDirty())
			{
				leapPressed(leapX, leapY, true);
				leapMotion.setLeftPressedDirty(false);
			}
			else
			{
				int pLeapX = (int)(leapMotion.getPreviousLeftHandLocation().getX() * displayWidth);
				int pLeapY = (int)(displayHeight - leapMotion.getPreviousLeftHandLocation().getY() * displayWidth);;
				leapDragged(leapX, leapY, pLeapX, pLeapY, true);
			}
		}
		if(leapMotion.isRightPressed())
		{
			int leapX = (int)(leapMotion.getRightHandLocation().getX() * displayWidth);
			int leapY = (int)(displayHeight - leapMotion.getRightHandLocation().getY() * displayHeight);
			
			if(leapMotion.isRightPressedDirty())
			{
				leapPressed(leapX, leapY, false);
				leapMotion.setRightPressedDirty(false);
			}
			else
			{
				int pLeapX = (int)(leapMotion.getPreviousRightHandLocation().getX() * displayWidth);
				int pLeapY = (int)(displayHeight - leapMotion.getPreviousRightHandLocation().getY() * displayWidth);;
				leapDragged(leapX, leapY, pLeapX, pLeapY, false);
			}
		}
	}
	
	public void drawChordMatrix()
	{
		matrix.draw();
	}
	
	public void mousePressed()
	{
		ChordCell cell = matrix.getCellFromPoint(new Point(mouseX, mouseY));
		if(mouseButton == LEFT)
		{
			if(CREATE_ANT)
			{
				matrix.addAnt(new Point(mouseX, mouseY));
			}
			else if(CREATE_CELLULAR_AUTOMATON)
			{
				matrix.addCellularAutomaton(new Point(mouseX, mouseY));
			}
			else if (CREATE_BOID)
			{
				matrix.addBoids(new Point(mouseX, mouseY), 10);
				System.out.println("Total Boids: " + matrix.getBoids().size());
			}
			else
			{
				cell.setIsOn(true);
			}
		}
		else if(mouseButton == RIGHT)
		{
			if(CREATE_ANT)
			{
				matrix.removeAnt(new Point(mouseX, mouseY));
			}
			else if(CREATE_CELLULAR_AUTOMATON)
			{
				matrix.removeCellularAutomaton(new Point(mouseX, mouseY));
			}
			else if (CREATE_BOID)
			{
				matrix.removeBoid(new Point(mouseX, mouseY));
				System.out.println("Total Boids: " + matrix.getBoids().size());
			}
			else
			{
				cell.setIsOn(false);
			}
		}
	}
	
	public void leapPressed(int leapX, int leapY, boolean isLeft)
	{
		if(leapX < 0 || leapX > displayWidth || leapY < 0 || leapY > displayHeight)
		{
			return;
		}
		
		ChordCell cell = matrix.getCellFromPoint(new Point(leapX, leapY));
		
		if(cell == null)
		{
			return;
		}
		
		if(!isLeft)
		{
			if(CREATE_ANT)
			{
				matrix.addAnt(new Point(leapX, leapY));
			}
			else if(CREATE_CELLULAR_AUTOMATON)
			{
				matrix.addCellularAutomaton(new Point(leapX, leapY));
			}
			else if (CREATE_BOID)
			{
				matrix.addBoids(new Point(leapX, leapY), 10);
				System.out.println("Total Boids: " + matrix.getBoids().size());
			}
			else
			{
				cell.setIsOn(true);
			}
		}
		else
		{
			if(CREATE_ANT)
			{
				matrix.removeAnt(new Point(leapX, leapY));
			}
			else if(CREATE_CELLULAR_AUTOMATON)
			{
				matrix.removeCellularAutomaton(new Point(leapX, leapY));
			}
			else if (CREATE_BOID)
			{
				matrix.removeBoid(new Point(leapX, leapY));
				System.out.println("Total Boids: " + matrix.getBoids().size());
			}
			else
			{
				cell.setIsOn(false);
			}
		}
	}
	
	public void mouseDragged()
	{
		ChordCell cell = matrix.getCellFromPoint(new Point(mouseX, mouseY));
		ChordCell prevCell = matrix.getCellFromPoint(new Point(pmouseX, pmouseY));
		if(prevCell != null && cell != null && !cell.equalTo(prevCell))
		{
			if(mouseButton == LEFT)
			{
				if(CREATE_ANT)
				{
					matrix.addAnt(new Point(mouseX, mouseY));
				}
				else if(CREATE_CELLULAR_AUTOMATON)
				{
					matrix.addCellularAutomaton(new Point(mouseX, mouseY));
				}
				else if (CREATE_BOID)
				{
					matrix.addBoids(new Point(mouseX, mouseY), 10);
					System.out.println("Total Boids: " + matrix.getBoids().size());
				}
				else
				{
					cell.setIsOn(true);
				}
			}
			else if(mouseButton == RIGHT)
			{
				if(CREATE_ANT)
				{
					matrix.removeAnt(new Point(mouseX, mouseY));
				}
				else if(CREATE_CELLULAR_AUTOMATON)
				{
					matrix.removeCellularAutomaton(new Point(mouseX, mouseY));
				}
				else if (CREATE_BOID)
				{
					matrix.removeBoid(new Point(mouseX, mouseY));
					System.out.println("Total Boids: " + matrix.getBoids().size());
				}
				else
				{
					cell.setIsOn(false);
				}
			}
		}
	}
	
	public void leapDragged(int leapX, int leapY, int pLeapX, int pLeapY, boolean isLeft)
	{
		if(leapX < 0 || leapX > displayWidth || leapY < 0 || leapY > displayHeight || pLeapX < 0 || pLeapX > displayWidth || pLeapY < 0 || pLeapY > displayHeight)
		{
			return;
		}
		
		ChordCell cell = matrix.getCellFromPoint(new Point(leapX, leapY));
		ChordCell prevCell = matrix.getCellFromPoint(new Point(pLeapX, pLeapY));
		
		if(cell == null || prevCell == null)
		{
			return;
		}
		
		if(prevCell != null && cell != null && !cell.equalTo(prevCell))
		{
			if(!isLeft)
			{
				if(CREATE_ANT)
				{
					matrix.addAnt(new Point(leapX, leapY));
				}
				else if(CREATE_CELLULAR_AUTOMATON)
				{
					matrix.addCellularAutomaton(new Point(leapX, leapY));
				}
				else if (CREATE_BOID)
				{
					matrix.addBoids(new Point(leapX, leapY), 10);
					System.out.println("Total Boids: " + matrix.getBoids().size());
				}
				else
				{
					cell.setIsOn(true);
				}
			}
			else if(isLeft)
			{
				if(CREATE_ANT)
				{
					matrix.removeAnt(new Point(leapX, leapY));
				}
				else if(CREATE_CELLULAR_AUTOMATON)
				{
					matrix.removeCellularAutomaton(new Point(leapX, leapY));
				}
				else if (CREATE_BOID)
				{
					matrix.removeBoid(new Point(leapX, leapY));
					System.out.println("Total Boids: " + matrix.getBoids().size());
				}
				else
				{
					cell.setIsOn(false);
				}
			}
		}
	}
	
	public void keyPressed()
	{	
		if(key == CODED)
		{
			if(keyCode == LEFT)
			{
				rootNoteIndex = (Note.NOTE_NAMES_COMMON.length + rootNoteIndex - 1) % Note.NOTE_NAMES_COMMON.length;
				changeScale();
			}
			if(keyCode == RIGHT)
			{
				rootNoteIndex = (rootNoteIndex + 1) % Note.NOTE_NAMES_COMMON.length;
				changeScale();
			}
			if(keyCode == UP)
			{
				rootNoteIndex = (rootNoteIndex + 1) % Note.NOTE_NAMES_COMMON.length;
				changeScale();
			}
			if(keyCode == DOWN)
			{
				rootNoteIndex = (Note.NOTE_NAMES_COMMON.length + rootNoteIndex - 1) % Note.NOTE_NAMES_COMMON.length;
				changeScale();
			}
		}
		
		if(key == 'a' || key == 'A')
		{
			CREATE_ANT = true;
			addGUIText("CREATE ANTS NOW", 1000);
		}
		
		if(key == 'c' || key == 'C')
		{
			CREATE_CELLULAR_AUTOMATON = true;
			addGUIText("CREATE CELLULAR AUTOMATA NOW", 1000);
		}
		
		if(key == 'b' || key == 'B')
		{
			CREATE_BOID = true;
			addGUIText("CREATE BOIDS NOW", 1000);
		}
		
		if(key == 'n' || key == 'N')
		{
			ChordMatrix.togglePlayingNotes();
			if(ChordMatrix.isPlayingNotes())
			{
				this.matrix.reInitChordMatrix(bars * notesPerBar, NOTES.length * octaveRange);
			}
			else
			{
				this.matrix.reInitChordMatrix(bars * notesPerBar, CHORD_SIGNATURES.length * NOTES.length * octaveRange);
			}
			addGUIText("PLAYING NOTES (NOT CHORDS): " + ChordMatrix.isPlayingNotes(), 1000);
		}
		
		if(key == 's' || key == 'S')
		{
			previousScale = currentScale;
			currentScale = Scales.values()[(currentScale.ordinal() + 1) % Scales.values().length];
			
			if(currentScale.compareTo(Scales.MAJOR) == 0)
			{
				scale = Scale.MAJOR;
			}
			else if(currentScale.compareTo(Scales.MINOR) == 0)
			{
				scale = Scale.MINOR;
			}
			else if(currentScale.compareTo(Scales.CIRCLE_OF_FIFTHS) == 0)
			{
				scale = Scale.CIRCLE_OF_FIFTHS;
			}
			
			changeScale();
		}
		
		if(key == 'h' || key == 'H' || key == '?' || key == ' ')
		{
			addGUIText(INFO_TEXT, 10000);
		}
		
		if(key == 'm' || key == 'M')
		{
			ChordMatrix.DRAW_CLUSTER_CENTERS = !ChordMatrix.DRAW_CLUSTER_CENTERS;
			addGUIText("MULTIPLE PLAYHEADS: " + ChordMatrix.DRAW_CLUSTER_CENTERS, 1000);
		}
	}
	
	public void changeScale()
	{
//		String root = Note.NOTE_NAMES_COMMON[random.nextInt(Note.NOTE_NAMES_COMMON.length)];
		String root = Note.NOTE_NAMES_COMMON[rootNoteIndex];
		if(currentScale.compareTo(Scales.CHROMATIC) != 0)
		{
			Intervals intervals = scale.getIntervals();
			intervals.setRoot(root);
			List<Note> notes = intervals.getNotes();
			ArrayList<String> noteString = new ArrayList<String>(); 
			for(Note n : notes)
			{
				noteString.add(Note.NOTE_NAMES_COMMON[n.getPositionInOctave()]);
			}
			NOTES = new String[noteString.size()];
			noteString.toArray(NOTES);
		}
		else
		{
			NOTES = ALL_NOTES;
		}
		scale.setName(root + " " + currentScale.toString());

		System.out.print("NOTES IN SCALE: ");
		for(int i = 0;i < NOTES.length; i++)
		{
			System.out.print(NOTES[i] + " ");
		}
		System.out.println();
		
		if(currentScale.compareTo(Scales.CHROMATIC) != 0 && previousScale.compareTo(Scales.CHROMATIC) != 0)
		{
			this.matrix.reInitChordMatrix(bars * notesPerBar, CHORD_SIGNATURES.length * NOTES.length * octaveRange);
		}
		else
		{
			this.matrix = new ChordMatrix(bars * notesPerBar, CHORD_SIGNATURES.length * NOTES.length * octaveRange, this);
		}
		addGUIText("CURRENT SCALE IS: " + scale.getName(), 1000);
	}
	
	public void keyReleased()
	{
		if(key == 'a' || key == 'A')
		{
			CREATE_ANT = false;
		}
		
		if(key == 'c' || key == 'C')
		{
			CREATE_CELLULAR_AUTOMATON = false;
		}
		
		if(key == 'b' || key == 'B')
		{
			CREATE_BOID = false;
		}
	}
	
	public void addGUIText(String message, long duration)
	{
		for (int index = 0; index < this.listGUIText.size(); index++)
		{
			String testMessage = this.listGUIText.get(index);
			if (message.split(":")[0].equalsIgnoreCase(testMessage.split(":")[0]))
			{
				this.listGUIText.remove(index);
				this.listGUIDuration.remove(index);
				break;
			}
		}
		this.listGUIText.add(message.toUpperCase());
		this.listGUIDuration.add(System.currentTimeMillis() + duration);
	}

	public void drawGUI()
	{
		int oldFill = this.g.fillColor;
		
		fill(color(255, 255, 255));
		textAlign(LEFT, TOP);
		long timeNowMillis = System.currentTimeMillis();
		textSize(32);
		float xPosPercent = 2f / 100f * (float) width;
		float yPosPercent = 3f / 100f * (float) height;
		for (int index = 0; index < this.listGUIText.size(); index++)
		{
			String message = this.listGUIText.get(index);
			long displayTime = this.listGUIDuration.get(index);
			text(message, xPosPercent, yPosPercent, 0f);
			yPosPercent += 5f / 100f * (float) displayHeight;
			if (displayTime < timeNowMillis)
			{
				this.listGUIText.remove(index);
				this.listGUIDuration.remove(index);
			}
		}
		
		fill(oldFill);
	}
}