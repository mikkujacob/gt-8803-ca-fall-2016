package cs8803ca.project4.music;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import javax.sound.midi.MidiUnavailableException;

import org.jfugue.realtime.RealtimePlayer;
import org.jfugue.theory.Chord;
import org.jfugue.theory.Note;

import cs8803ca.project4.application.Project4;
import cs8803ca.project4.organisms.alife.Ant;
import cs8803ca.project4.organisms.alife.Boid;
import cs8803ca.project4.organisms.alife.CellularAutomaton;
import cs8803ca.project4.shared.Clock;
import cs8803ca.project4.shared.Point;
import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;

public class ChordMatrix
{
	private ChordCell[][] matrix;
	private PApplet parent;
	private int dimensionX;
	private int dimensionY;
	
	private int width;
	private int height;
	
	private final static String[] ALL_CHORD_SIGNATURES = Project4.ALL_CHORD_SIGNATURES;
	private static String[] CHORD_SIGNATURES;
	private final static int selectedChords = Project4.selectedChords;
	private final static String[] NOTES = Project4.NOTES;
	private final static String[] DURATIONS = Project4.DURATIONS;
	private int durationIndex;
	private final static int octaveRange = Project4.octaveRange;
	private static Boolean isPlayingNotes = false;

	private final static int defaultChordOctave = 4;
	private final static int defaultNoteOctave = 4;
	
	private ArrayList<Ant> ants;
	private ArrayList<CellularAutomaton> cellularAutomata;
	private ArrayList<Boid> boids;
	private Boid playHeadBoid;
	private PVector previousPlayHeadBoidEMA;
	private PVector currentPlayHeadBoidEMA;
	public static boolean DRAW_CLUSTER_CENTERS = false;
	private ArrayList<Boid> playHeadBoids;
	private ArrayList<PVector> previousPlayHeadBoidsEMA;
	private ArrayList<PVector> currentPlayHeadBoidsEMA;
	private float smoothingFactor = 0.25f;
	
	private final static boolean RANDOM_INIT = false;
	private final static double percentageOn = 0.10;
	
	private Clock pulseClock;
	private Clock boidPulseClock;
	private int timeXIndex;
	private int timeYIndex;
	private final static int TEMPO = 120; //BPM
	private float pulsePeriod; // milliseconds
	private float boidPulsePeriod; // milliseconds
	
	private ArrayList<PImage> jellyfishImages;
	private ArrayList<PImage> fishImages;
	private ArrayList<PImage> seahorseImages;
	private ArrayList<PImage> coralImages;
	private ArrayList<PImage> backdropImages;
	
	private RealtimePlayer player;
	ArrayList<String> chords;
	
	private Random random;
	
	public ChordMatrix(int dimX, int dimY, PApplet parent)
	{
		this.parent = parent;
		
		pulseClock = new Clock();
		boidPulseClock = new Clock();
		timeXIndex = 0;
		timeYIndex = 0;
		pulsePeriod = 60f / TEMPO * 1000; // milliseconds
		boidPulsePeriod = 60f / 5 / TEMPO * 1000; // milliseconds
		
		width = parent.width;
		height = parent.height;
		
		durationIndex = 2;
		
		CHORD_SIGNATURES = new String[(selectedChords <= ALL_CHORD_SIGNATURES.length) ? selectedChords : ALL_CHORD_SIGNATURES.length];
		for(int index = ALL_CHORD_SIGNATURES.length - 1, chordCount = 0; index >= 0 && chordCount < selectedChords;index--, chordCount++)
		{
			CHORD_SIGNATURES[chordCount] = ALL_CHORD_SIGNATURES[index];
		}
		
		dimensionX = dimX;
		dimensionY = dimY;
		
		this.matrix = initChordCellMatrix(dimX, dimY);
		
		ants = new ArrayList<Ant>();
		cellularAutomata = new ArrayList<CellularAutomaton>();
		boids = new ArrayList<Boid>();
		
		playHeadBoids = new ArrayList<Boid>();
		previousPlayHeadBoidsEMA = new ArrayList<PVector>();
		currentPlayHeadBoidsEMA = new ArrayList<PVector>();
		
		try
		{
			player = new RealtimePlayer();
		}
		catch (MidiUnavailableException e)
		{
			e.printStackTrace();
		}
		
//		chords = computeCurrentTimeChords(timeXIndex);
//		startPlayingChords(chords);
		
		random = new Random();
		
		initializeImages();
		
		addBoids(150);
	}
	
	public void initializeImages()
	{
		if(backdropImages != null && !backdropImages.isEmpty())
		{
			return;
		}
		
		String path = "data/reef-rock/backdrop/";
		String imageFile;
		File imageFolder = new File(path);
		File[] imageFileList = imageFolder.listFiles();
		backdropImages = new ArrayList<PImage>();

		for (int i = 0; i < imageFileList.length; i++) 
		{
			if (imageFileList[i].isFile()) 
			{
				imageFile = imageFileList[i].getName();
				if (imageFile.toLowerCase().endsWith(".png"))
				{
					System.out.println("Imported Image File: " + path + imageFile);

					backdropImages.add(parent.loadImage(path + imageFile));
				}
			}
		}

		if(imageFileList.length == 0)
		{
			System.out.println("No Image Files In Folder: " + path);
		}
		
		path = "data/reef-rock/coral/";
		imageFolder = new File(path);
		imageFileList = imageFolder.listFiles();
		coralImages = new ArrayList<PImage>();

		for (int i = 0; i < imageFileList.length; i++) 
		{
			if (imageFileList[i].isFile()) 
			{
				imageFile = imageFileList[i].getName();
				if (imageFile.toLowerCase().endsWith(".png"))
				{
					System.out.println("Imported Image File: " + path + imageFile);

					coralImages.add(parent.loadImage(path + imageFile));
				}
			}
		}

		if(imageFileList.length == 0)
		{
			System.out.println("No Image Files In Folder: " + path);
		}
		
		path = "data/reef-rock/fish/";
		imageFolder = new File(path);
		imageFileList = imageFolder.listFiles();
		fishImages = new ArrayList<PImage>();

		for (int i = 0; i < imageFileList.length; i++) 
		{
			if (imageFileList[i].isFile()) 
			{
				imageFile = imageFileList[i].getName();
				if (imageFile.toLowerCase().endsWith(".png"))
				{
					System.out.println("Imported Image File: " + path + imageFile);

					fishImages.add(parent.loadImage(path + imageFile));
				}
			}
		}

		if(imageFileList.length == 0)
		{
			System.out.println("No Image Files In Folder: " + path);
		}
		
		path = "data/reef-rock/jellyfish/";
		imageFolder = new File(path);
		imageFileList = imageFolder.listFiles();
		jellyfishImages = new ArrayList<PImage>();

		for (int i = 0; i < imageFileList.length; i++) 
		{
			if (imageFileList[i].isFile()) 
			{
				imageFile = imageFileList[i].getName();
				if (imageFile.toLowerCase().endsWith(".png"))
				{
					System.out.println("Imported Image File: " + path + imageFile);

					jellyfishImages.add(parent.loadImage(path + imageFile));
				}
			}
		}

		if(imageFileList.length == 0)
		{
			System.out.println("No Image Files In Folder: " + path);
		}
		
		path = "data/reef-rock/seahorse/";
		imageFolder = new File(path);
		imageFileList = imageFolder.listFiles();
		seahorseImages = new ArrayList<PImage>();

		for (int i = 0; i < imageFileList.length; i++) 
		{
			if (imageFileList[i].isFile()) 
			{
				imageFile = imageFileList[i].getName();
				if (imageFile.toLowerCase().endsWith(".png"))
				{
					System.out.println("Imported Image File: " + path + imageFile);

					seahorseImages.add(parent.loadImage(path + imageFile));
				}
			}
		}

		if(imageFileList.length == 0)
		{
			System.out.println("No Image Files In Folder: " + path);
		}
	}
	
	@SuppressWarnings("unused")
	public ChordCell[][] initChordCellMatrix(int dimX, int dimY)
	{
		ChordCell[][] matrix = new ChordCell[dimX][dimY];
		
		float cellWidth = width / dimX;
		float cellHeight = height / dimY;
		
		float cellOriginX = 0f;
		float cellOriginY = 0f;
		
		for(int xIndex = 0; xIndex < dimX; xIndex++)
		{
			int chordIndex = CHORD_SIGNATURES.length - 1;
			int noteIndex = NOTES.length - 1;
			int currentOctaveRange = (!isPlayingNotes) ? octaveRange : octaveRange * CHORD_SIGNATURES.length;
			int octave = ((!isPlayingNotes) ? defaultChordOctave : defaultNoteOctave) + Math.round((currentOctaveRange - 1) / 2f);
			if(octave < 0)
			{
				octave = 0;
			}
			else if(octave > 9)
			{
				octave = 9;
			}
			int octaveCount = 0;
			cellOriginY = 0f;
			
			for (int yIndex = 0; yIndex < dimY; yIndex++)
			{
				String chord = "";
				if(!isPlayingNotes)
				{
					chord = NOTES[noteIndex--] + octave + CHORD_SIGNATURES[chordIndex] + DURATIONS[durationIndex];
				}
				else
				{
					chord = NOTES[noteIndex--] + octave + DURATIONS[durationIndex];
				}
				
				Point CellOrigin = new Point(cellOriginX, cellOriginY);
				
				ChordCell cell = new ChordCell(chord, CellOrigin, cellWidth, cellHeight);
				
				if(RANDOM_INIT && Math.random() < percentageOn)
				{
					cell.toggleIsOn();
				}
				
				System.out.println("CELL: " + cell.toString());
				
				matrix[xIndex][yIndex] = cell;
				
				if(!isPlayingNotes())
				{
					if(noteIndex == -1)
					{
						noteIndex = NOTES.length - 1;
						chordIndex--;
					}
					if(chordIndex == -1)
					{
						chordIndex = CHORD_SIGNATURES.length - 1;
						octave = (10 + octave - 1) % 10;
					}
				}
				else
				{
					if(noteIndex == -1)
					{
						noteIndex = NOTES.length - 1;
						octave = (10 + octave - 1) % 10;
					}
				}
				
				if(octaveCount > currentOctaveRange)
				{
					octave = ((!isPlayingNotes) ? defaultChordOctave : defaultNoteOctave) + Math.round((currentOctaveRange - 1) / 2f);
					if(octave < 0)
					{
						octave = 0;
					}
					else if(octave > 9)
					{
						octave = 9;
					}
					octaveCount = 0;
				}
				
				cellOriginY += cellHeight;
			}
			
			cellOriginX += cellWidth;
		}
		
		return matrix;
	}
	
	public void reInitChordMatrix(int dimX, int dimY)
	{
		dimensionX = dimX;
		dimensionY = dimY;
		
		this.matrix = initChordCellMatrix(dimX, dimY);
		
//		for(Ant a : ants)
//		{
//			a.setMatrix(this);
//		}
		
		ants.clear();
		
//		for(CellularAutomaton c : cellularAutomata)
//		{
//			c.setMatrix(this);
//		}
		
		cellularAutomata.clear();
		
		for(Boid b : boids)
		{
			b.setMatrix(this);
		}
	}

	public ChordMatrix(int cellCount, PApplet parent)
	{
		this(Math.round((float)Math.sqrt(cellCount)), Math.round((float)Math.sqrt(cellCount)), parent);
	}
	
	public void draw()
	{
		if(pulseClock.iniTimeMillis() > pulsePeriod)
		{
//			if(chords != null)
//			{
//				stopPlayingChords(chords);
//			}
//			timeXIndex = (timeXIndex + 1) % dimensionX;
			pulseClock.start();
//			chords = computeCurrentTimeChords(timeXIndex);
			if(playHeadBoid != null)
			{
				ChordCell cell = getCellFromPoint(new Point(playHeadBoid.getPosition()));
				
				if(cell.getIsOn())
				{
					chords = new ArrayList<String>();
					chords.add(cell.getChord());
				}
				else
				{
					chords = new ArrayList<String>();
				}
				
				if(DRAW_CLUSTER_CENTERS)
				{
					for(int index = 0; index < playHeadBoids.size(); index++)
					{
						cell = getCellFromPoint(new Point(playHeadBoids.get(index).getPosition()));
						
						if(cell != null && cell.getIsOn())
						{
							chords.add(cell.getChord());
						}
					}
				}
//				System.out.println("Chords: " + chords);
				startPlayingChords(chords);
			}
			
//			startPlayingChords(chords);
			
			moveCellularAutomata();
			moveAnts();
		}
		
		parent.tint(255);
		parent.lights();
		
		drawBackDrop();
		
		drawChordMatrix();
		
		drawCellularAutomata();	
		
		drawAnts();
		
		if(boidPulseClock.iniTimeMillis() > boidPulsePeriod)
		{
			moveBoids();
			if(playHeadBoid != null)
			{
				Point p = new Point(playHeadBoid.getPosition());
				for(int x = 0; x < dimensionX; x++)
				{
					for(int y = 0; y < dimensionY; y++)
					{
						if(matrix[x][y].pointInCell(p))
						{
							timeXIndex = x;
							timeYIndex = y;
						}
					}
				}
			}
			drawBoids();
			
			if(DRAW_CLUSTER_CENTERS)
			{
				drawPlayHeadBoids();
			}
		}
	}
	
	public void moveAnts()
	{
		for(Ant a : ants)
		{
			a.nextMove();
		}
	}
	
	public void moveCellularAutomata()
	{
		for(CellularAutomaton c : cellularAutomata)
		{
			c.nextMove();
		}
	}
	
	public void moveBoids()
	{
		for(Boid b : boids)
		{
			b.nextMove();
		}
	}
	
	public String[] computeCurrentTimeChords(int timeIndex)
	{
		String musicString = "";
		for(int time = timeIndex, yIndex = 0; yIndex < dimensionY; yIndex++)
		{
			ChordCell cell = matrix[time][yIndex];
			
			if(cell.getIsOn())
			{
				musicString = musicString + " " + cell.getChord();
			}
		}
		
		String[] chords = musicString.split(" ");
		
		return chords;
	}
	
	public void startPlayingChords(ArrayList<String> chords)
	{
		for(int index = 0; index < chords.size(); index++)
		{
			if(!isPlayingNotes() && !chords.isEmpty())
			{
				this.player.startChord(new Chord(chords.get(index)));
			}
			else if(isPlayingNotes() && !chords.isEmpty())
			{
				this.player.startNote(new Note(chords.get(index)));
			}
		}
	}
	
//	public void stopPlayingChords(ArrayList<String> chords)
//	{
//		for(int index = 0; index < chords.size(); index++)
//		{
//			if(!isPlayingNotes() && !chords.isEmpty())
//			{
//				this.player.stopChord(new Chord(chords.get(index)));
//			}
//			else if(isPlayingNotes() && !chords.isEmpty())
//			{
//				this.player.stopNote(new Note(chords.get(index)));
//			}
//		}
//	}
	
	public void drawBackDrop()
	{
//		parent.getGraphics().clear();
		parent.image(backdropImages.get(0), 0, 0, parent.displayWidth, parent.displayHeight);
	}
	
	public void drawChordMatrix()
	{
		int oldFill = parent.getGraphics().fillColor;
		
		for(int xIndex = 0; xIndex < dimensionX; xIndex++)
		{
			for(int yIndex = 0; yIndex < dimensionY; yIndex++)
			{
				ChordCell cell = matrix[xIndex][yIndex];
				if(cell.getIsOn() && (timeXIndex == xIndex || timeYIndex == yIndex))
				{
					parent.fill(parent.color(175, 20, 0, 100));
				}
				else if(cell.getIsOn() && !(timeXIndex == xIndex || timeYIndex == yIndex))
				{
					parent.fill(parent.color(154, 46, 79, 100));
				}
				else if(!cell.getIsOn() && (timeXIndex == xIndex || timeYIndex == yIndex))
				{
					parent.fill(parent.color(39, 36, 194, 100));
				}
				else if(!cell.getIsOn() && !(timeXIndex == xIndex || timeYIndex == yIndex))
				{
					parent.fill(parent.color(58, 92, 169, 100));
				}
				
				parent.rect(cell.getOrigin().x, cell.getOrigin().y, cell.getWidth(), cell.getHeight());
			}
		}
		
		parent.fill(oldFill);
	}
	
	public void drawAnts()
	{
		int oldFill = parent.getGraphics().fillColor;
		
		parent.fill(parent.color(87, 201, 87));
		
		for(Ant a : ants)
		{
			ChordCell cell = getCellFromIndexPoint(a.getPosition());
			float antWidth = (width / dimensionX < height / dimensionY) ? width / dimensionX : height / dimensionY;
			float antHeight = (width / dimensionX < height / dimensionY) ? width / dimensionX : height / dimensionY;
			
			if(isPlayingNotes())
			{
				antWidth /= ((float)CHORD_SIGNATURES.length);
				antHeight /= ((float)CHORD_SIGNATURES.length);
			}
			
			float originX = cell.getCenter().x - antWidth / 2;
			float originY = cell.getCenter().y - antHeight / 2;
//			parent.ellipse(originX, originY, antWidth, antHeight);
			
//			if(a.getAntImage().width != Math.round(antWidth) || a.getAntImage().height != Math.round(antHeight))
//			{
//				PImage displayImage = a.getAntImage();
//				displayImage.resize(Math.round(antWidth), Math.round(antHeight));
//				a.setAntImage(displayImage);
//			}
			
			parent.image(a.getAntImage(), originX - 1 * antWidth, originY - 0 * antHeight, antWidth, antHeight);
		}
		
		parent.fill(oldFill);
	}
	
	public void drawCellularAutomata()
	{
		int oldFill = parent.getGraphics().fillColor;
		
		parent.fill(parent.color(252, 249, 108));
		
		for(CellularAutomaton c : cellularAutomata)
		{
			ChordCell cell = getCellFromIndexPoint(c.getPosition());
			float cellularAutomatonWidth = width / dimensionX / 2;
			float cellularAutomatonHeight = height / dimensionY / 2;
			float originX = cell.getCenter().x - cellularAutomatonWidth / 2;
			float originY = cell.getCenter().y - cellularAutomatonHeight / 2;
//			parent.rect(originX, originY, cellularAutomatonWidth, cellularAutomatonHeight);
			parent.image(c.getCellularAutomatonImage(), originX, originY, cellularAutomatonWidth, cellularAutomatonHeight);
		}
		
		parent.fill(oldFill);
	}
	
	public void drawBoids()
	{
		int oldFill = parent.getGraphics().fillColor;
		
		parent.fill(parent.color(216, 1, 223));
		
		PVector sum = new PVector();
		
		for(Boid b : boids)
		{
			float originX = b.getPosition().x;
			float originY = b.getPosition().y;
			
			parent.pushMatrix();
			int oldTint = parent.getGraphics().tintColor;
			
			float boidWidth = (width / dimensionX / 2 < height / dimensionY / 2) ? width / dimensionX / 2 : height / dimensionY / 2;
			float boidHeight = (width / dimensionX / 2 < height / dimensionY / 2) ? width / dimensionX / 2 : height / dimensionY / 2;
			
			if(isPlayingNotes())
			{
				boidWidth /= ((float)CHORD_SIGNATURES.length);
				boidHeight /= ((float)CHORD_SIGNATURES.length);
			}
			
//			parent.ellipse(originX, originY, boidWidth, boidHeight);
			
			parent.translate(originX, originY);
			
			parent.rotate(b.getAngleOrientation());
			parent.tint(255, 200);
//			parent.image(b.getBoidImage(), originX - 1 * boidWidth, originY - 1 * boidHeight, 2 * boidWidth, 2 * boidHeight);
			parent.image(b.getBoidImage(), 0 - 1 * boidWidth, 0 - 1 * boidHeight, 2 * boidWidth, 2 * boidHeight);
			parent.rotate(-1 * b.getAngleOrientation());
			
			parent.translate(-1 * originX, -1 * originY);
			
			parent.getGraphics().tint(oldTint);
			parent.popMatrix();
			sum.add(b.getPosition());
		}
		
		if(!boids.isEmpty())
		{
			sum.div((float)boids.size());

			if (previousPlayHeadBoidEMA == null)
			{
				previousPlayHeadBoidEMA = sum;
				currentPlayHeadBoidEMA = sum;
				playHeadBoid = new Boid(sum.x, sum.y, width, height, this, jellyfishImages.get(0));
			}
			else
			{
				currentPlayHeadBoidEMA = previousPlayHeadBoidEMA;
				currentPlayHeadBoidEMA.mult(1 - smoothingFactor);
				PVector smoothingTimesX = sum;
				smoothingTimesX.mult(smoothingFactor);
				currentPlayHeadBoidEMA.add(smoothingTimesX);
			}
			playHeadBoid.setPosition(currentPlayHeadBoidEMA);

			float originX = playHeadBoid.getPosition().x;
			float originY = playHeadBoid.getPosition().y;
			
			float boidWidth = (width / dimensionX > height / dimensionY) ? width / dimensionX / 4 : height / dimensionY / 4;
			float boidHeight = (width / dimensionX > height / dimensionY) ? width / dimensionX / 4 : height / dimensionY / 4;
			
			int oldTint = parent.getGraphics().tintColor;
			parent.tint(255, 180);
//			parent.ellipse(originX, originY, boidWidth, boidHeight);
			parent.image(playHeadBoid.getBoidImage(), originX - 3 * boidWidth, originY - 3 * boidHeight, 6 * boidWidth, 6 * boidHeight);
			parent.getGraphics().tint(oldTint);
		}
		
		parent.fill(oldFill);
	}
	
	public void drawPlayHeadBoids()
	{
		int oldFill = parent.getGraphics().fillColor;
		
		parent.fill(parent.color(216, 1, 223));

		if(!boids.isEmpty())
		{
			currentPlayHeadBoidsEMA = Boid.getClusterCenters();
			
			if(currentPlayHeadBoidsEMA.size() != previousPlayHeadBoidsEMA.size())
			{
				previousPlayHeadBoidsEMA.clear();
				playHeadBoids.clear();
				
				previousPlayHeadBoidsEMA = new ArrayList<>(currentPlayHeadBoidsEMA);
				
				for(int index = 0; index < currentPlayHeadBoidsEMA.size(); index++)
				{
					playHeadBoids.add(new Boid(currentPlayHeadBoidsEMA.get(index).x, currentPlayHeadBoidsEMA.get(index).y, width, height, this, jellyfishImages.get(random.nextInt(jellyfishImages.size()))));
				}
			}
			else
			{
				for(int index = 0; index < currentPlayHeadBoidsEMA.size(); index++)
				{
					PVector tempCurrentBoidEMA = previousPlayHeadBoidsEMA.get(index);
					tempCurrentBoidEMA.mult(1 - smoothingFactor);
					PVector smoothingTimesX = currentPlayHeadBoidsEMA.get(index);
					smoothingTimesX.mult(smoothingFactor);
					tempCurrentBoidEMA.add(smoothingTimesX);
					currentPlayHeadBoidsEMA.set(index, tempCurrentBoidEMA);
				}
			}
			
			for(int index = 0; index < playHeadBoids.size(); index++)
			{
				playHeadBoids.get(index).setPosition(currentPlayHeadBoidsEMA.get(index));
				
				float originX = playHeadBoids.get(index).getPosition().x;
				float originY = playHeadBoids.get(index).getPosition().y;
				
				float boidWidth = (width / dimensionX > height / dimensionY) ? width / dimensionX / 3 : height / dimensionY / 3;
				float boidHeight = (width / dimensionX > height / dimensionY) ? width / dimensionX / 3 : height / dimensionY / 3;
				
				int oldTint = parent.getGraphics().tintColor;
				parent.tint(255, 180);
//				parent.ellipse(originX, originY, boidWidth, boidHeight);
				parent.image(playHeadBoids.get(index).getBoidImage(), originX, originY, 4 * boidWidth, 4 * boidHeight);
				parent.getGraphics().tint(oldTint);
			}
		}
		
		parent.fill(oldFill);
	}
	
	public ChordCell getCellFromPoint(Point p)
	{
		for(int x = 0; x < dimensionX; x++)
		{
			for(int y = 0; y < dimensionY; y++)
			{
				if(matrix[x][y].pointInCell(p))
				{
					return matrix[x][y];
				}
			}
		}
		
		return null;
	}
	
	public ChordCell getCellFromIndexPoint(Point p)
	{
		return matrix[(int) p.x][(int) p.y];
	}
	
	public boolean addAnt(Point p)
	{
		for(int x = 0; x < dimensionX; x++)
		{
			for(int y = 0; y < dimensionY; y++)
			{
				if(matrix[x][y].pointInCell(p))
				{
					ants.add(new Ant(x, y, dimensionX, dimensionY, this, seahorseImages.get(random.nextInt(seahorseImages.size()))));
					
					return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean removeAnt(Point p)
	{
		for(int x = 0; x < dimensionX; x++)
		{
			for(int y = 0; y < dimensionY; y++)
			{
				if(matrix[x][y].pointInCell(p))
				{
					for(int i = 0; i < ants.size(); i++)
					{
						Ant a = ants.get(i);
						if(a.getPosition().x == x && a.getPosition().y == y)
						{
							ants.remove(i);
						}
					}
					
					return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean addCellularAutomaton(Point p)
	{
		for(int x = 0; x < dimensionX; x++)
		{
			for(int y = 0; y < dimensionY; y++)
			{
				if(matrix[x][y].pointInCell(p))
				{
					for(CellularAutomaton c : cellularAutomata)
					{
						if(c.getPosition().x == x && c.getPosition().y == y)
						{
							return false;
						}
					}
					cellularAutomata.add(new CellularAutomaton(x, y, dimensionX, dimensionY, this, coralImages.get(random.nextInt(coralImages.size()))));
					
					return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean removeCellularAutomaton(Point p)
	{
		for(int x = 0; x < dimensionX; x++)
		{
			for(int y = 0; y < dimensionY; y++)
			{
				if(matrix[x][y].pointInCell(p))
				{
					for(int i = 0; i < cellularAutomata.size(); i++)
					{
						CellularAutomaton c = cellularAutomata.get(i);
						if(c.getPosition().x == x && c.getPosition().y == y)
						{
							cellularAutomata.remove(i);
						}
					}
					
					return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean addBoid(Point p)
	{
		boids.add(new Boid(p.x, p.y, width, height, this, fishImages.get(random.nextInt(fishImages.size()))));
		
		return true;
	}
	
	public boolean removeBoid(Point p)
	{
		for(int i = 0; i < boids.size(); i++)
		{
			Boid b = boids.get(i);
			if(Math.abs(b.getPosition().x - p.x) < 25 && Math.abs(b.getPosition().y - p.y) < 25)
			{
				boids.remove(i);
			}
		}
		
		return true;
	}
	
	public boolean addBoids(Point p, int count)
	{
		for(int i = 0; i < count; i++)
		{
			boids.add(new Boid(p.x, p.y, width, height, this, fishImages.get(random.nextInt(fishImages.size()))));
		}
		
		return true;
	}
	
	public boolean addBoids(int count)
	{
		return addBoids(new Point(width / 2f, height / 2f), count);
	}

	public ChordCell[][] getMatrix()
	{
		return matrix;
	}

	public void setMatrix(ChordCell[][] matrix)
	{
		this.matrix = matrix;
	}

	public PApplet getParent()
	{
		return parent;
	}

	public void setParent(PApplet parent)
	{
		this.parent = parent;
	}

	public int getDimensionX()
	{
		return dimensionX;
	}

	public void setDimensionX(int dimensionX)
	{
		this.dimensionX = dimensionX;
	}

	public int getDimensionY()
	{
		return dimensionY;
	}

	public void setDimensionY(int dimensionY)
	{
		this.dimensionY = dimensionY;
	}

	public int getWidth()
	{
		return width;
	}

	public void setWidth(int width)
	{
		this.width = width;
	}

	public int getHeight()
	{
		return height;
	}

	public void setHeight(int height)
	{
		this.height = height;
	}

	public int getDurationIndex()
	{
		return durationIndex;
	}

	public void setDurationIndex(int durationIndex)
	{
		this.durationIndex = durationIndex;
	}

	public ArrayList<Ant> getAnts()
	{
		return ants;
	}

	public void setAnts(ArrayList<Ant> ants)
	{
		this.ants = ants;
	}

	public ArrayList<CellularAutomaton> getCellularAutomata()
	{
		return cellularAutomata;
	}

	public void setCellularAutomata(ArrayList<CellularAutomaton> cellularAutomata)
	{
		this.cellularAutomata = cellularAutomata;
	}
	
	public ArrayList<Boid> getBoids()
	{
		return boids;
	}

	public void setBoids(ArrayList<Boid> boids)
	{
		this.boids = boids;
	}
	
	public static boolean isPlayingNotes()
	{
		return isPlayingNotes;
	}

	public static void setPlayingNotes(boolean isPlayingNotes)
	{
		ChordMatrix.isPlayingNotes = isPlayingNotes;
	}
	
	public static void togglePlayingNotes()
	{
		isPlayingNotes = !isPlayingNotes;
	}
}