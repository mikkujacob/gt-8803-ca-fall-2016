/**
 * 
 */
package cs8803ca.project4.organisms.alife;

import java.util.ArrayList;

import cs8803ca.project4.music.ChordMatrix;
import processing.core.PImage;
import processing.core.PVector;

/**
 * @author mikhailjacob
 *
 */
public class Boid
{
	private PVector position;
	private float width;
	private float height;
	private PVector velocity;
	private PVector acceleration;
	private static final float maxVelocity = 5f; // 3 or 10
	private static final float maxAcceleration = 0.9f; //0.4
	private static final float separationWeight = 1.9f; //1.5
	private static final float alignmentWeight = 2.0f; //1.0
	private static final float cohesionWeight = 1.4f; //1.4
	private static final float maxSeparationNeighborDistance = 50f;
	private static final float maxAlignmentNeighborDistance = 100f;
	private static final float maxCohesionNeighborDistance = 100f;
	private static final float maxClusterNeighborDistance = 100f;
	private static final float offset = 2f;
	private static ChordMatrix matrix;
	private PImage boidImage;
	private float angleOrientation;
	
	public Boid(float x, float y, PVector velocity, float width, float height, ChordMatrix matrix, PImage boidImage)
	{
		this.position = new PVector(x, y, 0);
		this.width = width;
		this.height = height;
		this.velocity = velocity;
		this.acceleration = new PVector(0, 0, 0);
		Boid.matrix = matrix;
		this.boidImage = boidImage;
	}
	
	public Boid(float x, float y, float width, float height, ChordMatrix matrix, PImage boidImage)
	{
		this(x, y, PVector.random2D(), width, height, matrix, boidImage);
	}
	
	public void nextMove()
	{
		computeNextPosition();
	}
	
	public void computeNextPosition()
	{
		this.acceleration.set(0, 0, 0);
		PVector separationAcceleration = computeSeparationAcceleration();
		separationAcceleration.mult(separationWeight);
		PVector alignmentAcceleration = computeAlignmentAcceleration();
		alignmentAcceleration.mult(alignmentWeight);
		PVector cohesionAcceleration = computeCohesionAcceleration();
		cohesionAcceleration.mult(cohesionWeight);
		
		this.acceleration.add(separationAcceleration);
		this.acceleration.add(alignmentAcceleration);
		this.acceleration.add(cohesionAcceleration);
		this.velocity.add(this.acceleration);
		this.velocity.limit(Boid.maxVelocity);
		this.position.add(this.velocity);
		this.acceleration.set(0, 0, 0);
		
		this.angleOrientation = PVector.angleBetween(velocity, new PVector(1, 0, 0));
		
		if(this.position.x < -1 * offset)
		{
			this.position.x = this.width + offset;
		}
		if(this.position.x > this.width + offset)
		{
			this.position.x = -1 * offset;
		}
		if(this.position.y < -1 * offset)
		{
			this.position.y = this.height + offset;
		}
		if(this.position.y > this.height + offset)
		{
			this.position.y = -1 * offset;
		}
	}
	
	public PVector computeSeparationAcceleration()
	{
		PVector steering = new PVector(0, 0, 0);
		float boidCount = 0;
		
		for(Boid b : Boid.matrix.getBoids())
		{
			float distance = PVector.dist(this.position, b.position);
			if(distance > 0f && distance < Boid.maxSeparationNeighborDistance)
			{
				PVector difference = PVector.sub(this.position, b.position);
				difference.normalize();
				difference.div(distance);
				steering.add(difference);
				boidCount += 1;
			}
		}
		
		if(boidCount > 0)
		{
			steering.div(boidCount);
		}
		
		if(steering.mag() > 0)
		{
			steering.setMag(Boid.maxVelocity);
			steering.sub(this.velocity);
			steering.limit(Boid.maxAcceleration);
		}
		
		return steering;
	}

	public PVector computeAlignmentAcceleration()
	{
		PVector sum = new PVector(0, 0, 0);
		float boidCount = 0;
		
		for(Boid b : Boid.matrix.getBoids())
		{
			float distance = PVector.dist(this.position, b.position);
			if(distance > 0f && distance < Boid.maxAlignmentNeighborDistance)
			{
				sum.add(b.velocity);
				boidCount += 1;
			}
		}
		
		if(boidCount > 0)
		{
			sum.div(boidCount);
			sum.setMag(Boid.maxVelocity);
			PVector steering = PVector.sub(sum, this.velocity);
			steering.limit(Boid.maxAcceleration);
			return steering;
		}
		
		return new PVector(0, 0, 0);
	}
	
	public PVector computeCohesionAcceleration()
	{
		PVector sum = new PVector(0, 0, 0);
		float boidCount = 0;
		
		for(Boid b : Boid.matrix.getBoids())
		{
			float distance = PVector.dist(this.position, b.position);
			if(distance > 0f && distance < Boid.maxCohesionNeighborDistance)
			{
				sum.add(b.position);
				boidCount += 1;
			}
		}
		
		if(boidCount > 0)
		{
			sum.div(boidCount);
			return computeSeeking(sum);
		}
		
		return new PVector(0, 0, 0);
	}
	
	public PVector computeSeeking(PVector goal)
	{
		PVector difference = PVector.sub(goal, this.position);
		difference.setMag(Boid.maxVelocity);
		PVector steering = PVector.sub(difference, this.velocity);
		steering.limit(Boid.maxAcceleration);
		return steering;
	}
	
	public static ArrayList<PVector> getClusterCenters()
	{
		return getClusterCenters(maxClusterNeighborDistance);
	}
	
	public static ArrayList<PVector> getClusterCenters(float thresholdDistance)
	{
		//Get clusters
		ArrayList<Boid> unclusteredBoids = new ArrayList<Boid>(Boid.matrix.getBoids());
		ArrayList<ArrayList<Boid>> boidClusters = new ArrayList<ArrayList<Boid>>();
		ArrayList<PVector> clusteredBoidCenters = new ArrayList<PVector>();
		
		while(!unclusteredBoids.isEmpty())
		{
			//Make first unclustered boid part of new cluster.
			ArrayList<Boid> boidCluster = new ArrayList<Boid>();
			boidCluster.add(unclusteredBoids.get(0));
			
			//For every element in boid cluster
			for(int clusteredIndex = 0; clusteredIndex < boidCluster.size(); clusteredIndex++)
			{
				//For every element still in unclustered boids
				for(int unclusteredIndex = 0; unclusteredIndex < unclusteredBoids.size(); unclusteredIndex++)
				{
					//If element in boid cluster is close to element in unclustered boids within some threshold
					if(boidCluster.get(clusteredIndex).getPosition().dist(unclusteredBoids.get(unclusteredIndex).getPosition()) < thresholdDistance)
					{
						//Add unclustered boid to boid cluster and remove it from unclustered boids
						boidCluster.add(unclusteredBoids.remove(unclusteredIndex));
					}
				}
				
			}
			
			//Add cluster to boid clusters
			boidClusters.add(boidCluster);
		}
		
		//Get cluster centers
		for(int clusterIndex = 0; clusterIndex < boidClusters.size(); clusterIndex++)
		{
			//Compute sum of all boid positions in a cluster
			PVector averagePosition = new PVector();
			int count = 0;
			for(Boid b : boidClusters.get(clusterIndex))
			{
				averagePosition.add(b.getPosition());
				count++;
			}
			if(count > 0)
			{
				//Compute cluster center 
				averagePosition.div(count);
				//Add cluster center to clustered boid center list
				clusteredBoidCenters.add(averagePosition);
			}
		}
		
		return clusteredBoidCenters;
	}
	
	public PVector getPosition()
	{
		return position;
	}

	public void setPosition(PVector position)
	{
		this.position = position;
	}

	public PVector getVelocity()
	{
		return velocity;
	}

	public void setVelocity(PVector velocity)
	{
		this.velocity = velocity;
	}

	public PVector getAcceleration()
	{
		return acceleration;
	}

	public void setAcceleration(PVector acceleration)
	{
		this.acceleration = acceleration;
	}

	public ChordMatrix getMatrix()
	{
		return matrix;
	}

	public void setMatrix(ChordMatrix matrix)
	{
		Boid.matrix = matrix;
	}

	public PImage getBoidImage()
	{
		return boidImage;
	}

	public void setBoidImage(PImage boidImage)
	{
		this.boidImage = boidImage;
	}

	public float getAngleOrientation()
	{
		return angleOrientation;
	}

	public void setAngleOrientation(float angleOrientation)
	{
		this.angleOrientation = angleOrientation;
	}
}
