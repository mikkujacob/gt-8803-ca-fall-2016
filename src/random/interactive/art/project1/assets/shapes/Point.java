package random.interactive.art.project1.assets.shapes;

public class Point
{
	public float x;
	public float y;
	public float z;
	
	public Point(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Point(float x, float y)
	{
		this(x, y, 0);
	}
	
	public Point(Point point)
	{
		this(point.x, point.y, point.z);
	}
}
