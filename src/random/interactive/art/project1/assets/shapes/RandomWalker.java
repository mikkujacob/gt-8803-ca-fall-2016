package random.interactive.art.project1.assets.shapes;

import cs8803ca.project3.ColorSpaces;
import processing.core.PApplet;
import random.interactive.art.project1.application.Project1.RandomWalkDrawer;

public class RandomWalker
{
	PApplet parent;
	float radius;
	int color;
	Point origin;
	Point lastOrigin;
	
	public RandomWalker(float radius, int color, Point origin, PApplet parent)
	{
		this.parent = parent;
		this.radius = radius;
		this.color = color;
		this.color = parent.color(parent.red(color), parent.green(color), parent.blue(color), parent.random(225, 255));
		this.origin = new Point(origin);
		this.lastOrigin = origin;
	}
	
	public RandomWalker(float radius, int color, PApplet parent)
	{
		this(radius, color, new Point(parent.random(0, parent.displayWidth), parent.random(0, parent.displayHeight), parent.random(0, 1000)), parent);
	}
	
	public RandomWalker(int color, Point origin, PApplet parent)
	{
		this(parent.height / parent.random(40, 100), color, origin, parent);
	}
	
	public RandomWalker(float radius, Point origin, PApplet parent)
	{
		this(radius, new ColorSpaces(parent).LCHtoColor(parent.random(0, 100), parent.random(0, 100), parent.random(0, 360)), origin, parent);
	}
	
	public RandomWalker(float radius, PApplet parent)
	{
		this(radius, new ColorSpaces(parent).LCHtoColor(parent.random(0, 100), parent.random(0, 100), parent.random(0, 360)), new Point(parent.random(0, parent.displayWidth), parent.random(0, parent.displayHeight), parent.random(0, 1000)), parent);
	}
	
	public RandomWalker(int color, PApplet parent)
	{
		this(parent.height / parent.random(40, 100), color, new Point(parent.random(0, parent.displayWidth), parent.random(0, parent.displayHeight), parent.random(0, 1000)), parent);
	}
	
	public RandomWalker(Point origin, PApplet parent)
	{
		this(parent.height / parent.random(40, 100), new ColorSpaces(parent).LCHtoColor(parent.random(0, 100), parent.random(0, 100), parent.random(0, 360)), origin, parent);
	}
	
	public RandomWalker(PApplet parent)
	{
		this(parent.height / parent.random(40, 100), new ColorSpaces(parent).LCHtoColor(parent.random(0, 100), parent.random(0, 100), parent.random(0, 360)), new Point(parent.random(0, parent.displayWidth), parent.random(0, parent.displayHeight), parent.random(0, 1000)), parent);
	}
	
	public void move(float jiggleConstant)
	{
		lastOrigin = new Point(origin);
		origin.x += parent.randomGaussian() * jiggleConstant;
		origin.y += parent.randomGaussian() * jiggleConstant;
		origin.z += parent.randomGaussian() * jiggleConstant;
		
		if(origin.x < -1 * parent.displayWidth * 0.1 || origin.x > parent.displayWidth * 1.1 || 
				origin.y < -1 * parent.displayHeight * 0.1 || origin.y > parent.displayHeight * 1.1 || 
				origin.z < -1 * 9 * 0.1 || origin.z > 1000 * 1.1)
		{
			origin = new Point(parent.random(0, parent.displayWidth), parent.random(0, parent.displayHeight), parent.random(0, 1000));
			lastOrigin = origin;
		}
	}
	
	public void draw(RandomWalkDrawer drawer)
	{
		if(drawer == RandomWalkDrawer.LINES)
		{
			float oldStrokeWeight = parent.g.strokeWeight;
			int oldStroke = parent.g.strokeColor;
			parent.strokeWeight(PApplet.map(radius, parent.height/100, parent.height/40, 1, 10));
			parent.stroke(color);
			parent.line(origin.x, origin.y, lastOrigin.x, lastOrigin.y);
			parent.stroke(oldStroke);
			parent.strokeWeight(oldStrokeWeight);
		}
		else if(drawer == RandomWalkDrawer.CIRCLES)
		{
			int oldStroke = parent.g.strokeColor;
			int oldFill = parent.g.fillColor;
			parent.noStroke();
			parent.fill(color);
			parent.ellipse(origin.x, origin.y, radius, radius);
			parent.fill(oldFill);
			parent.stroke(oldStroke);
		}
	}
}
