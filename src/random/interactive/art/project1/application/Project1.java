package random.interactive.art.project1.application;

import processing.core.PApplet;
import random.interactive.art.project1.assets.shapes.RandomWalker;

public class Project1 extends PApplet
{
	final int n = 1000; 
	float jiggleConstant = 0.5f;
	RandomWalker[] walkers = new RandomWalker[n];
	RandomWalkDrawer drawer = RandomWalkDrawer.CIRCLES;
	
	public enum RandomWalkDrawer
	{
		CIRCLES,
		LINES
	}
	
	public void setup()
	{
		init();
	}
	
	public void init()
	{
		for(int i = 0; i < n; i++)
		{
			walkers[i] = new RandomWalker(this);
		}
		
		background(0);
	}
	
	public void settings()
	{
		fullScreen(P3D);
	}
	
	public void draw()
	{
		lights();
		
		for(int i = 0; i < n; i++)
		{
			walkers[i].draw(this.drawer);
			walkers[i].move(jiggleConstant);
		}
	}
	
	public static void main(String[] args)
	{
		PApplet.main(new String[] { "random.interactive.art.project1.application.Project1" });
	}
	
	public void keyPressed()
	{
		if(key == ' ')
		{
			init();
		}
		else if(key == '=')
		{
			jiggleConstant += 0.5f;
		}
		else if(key == '-')
		{
			jiggleConstant -= 0.5f;
		}
		else if(key == 'd')
		{
			this.drawer = RandomWalkDrawer.values()[(this.drawer.ordinal() + 1) % RandomWalkDrawer.values().length];
		}
	}
}
